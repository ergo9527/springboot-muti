package com.ergo.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Future;

/**
 * 配置kafka生产者
 */
@Slf4j
//@Component
public class KafkaSendMsgUtil {

    private static Properties getProps() {
        //加载配置文件
//        System.setProperty("java.security.auth.login.config", "/kafka-ca/kafka_jaas_wxming.conf");
//        System.setProperty("java.security.krb5.conf", "/kafka-ca/krb5.conf");

        Properties props = new Properties();
        //kafka相应信息
        props.put("bootstrap.servers", "http://192.168.4.229:9092");
//        props.put("security.protocol", "SASL_PLAINTEXT");
//        props.put("sasl.kerberos.service.name", "kafka");
//        props.put("sasl.mechanism", "GSSAPI");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    public static void sendMsg(String topic, JSONObject jsonObject) {
        try {
            Producer<String, String> producer = new KafkaProducer<>(getProps());
            String key = UUID.randomUUID().toString().replaceAll("-", "");
            Future sent = producer.send(new ProducerRecord<>(topic, key, jsonObject.toJSONString()));
            System.out.println(sent.isDone());
            producer.close();
            //刷新流
//            server.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void sendJsonArrayMsg(String topic, JSONArray jsonArray) {
//        try {
//            //加载配置文件
//            System.setProperty("java.security.auth.login.config", "/kafka-ca/kafka_jaas_wxming.conf");
//            System.setProperty("java.security.krb5.conf", "/kafka-ca/krb5.conf");
//
//            Properties props = new Properties();
//            //kafka相应信息
//            props.put("bootstrap.servers", "glzt-bigdata-hdp003:6667,glzt-bigdata-hdp004:6667,glzt-bigdata-hdp005:6667");
//            props.put("security.protocol", "SASL_PLAINTEXT");
//            props.put("sasl.kerberos.service.name", "kafka");
//            props.put("sasl.mechanism", "GSSAPI");
//            props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//            props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//
//            Producer<String, String> server = new KafkaProducer<>(props);
//            //生成key
//            String key = UUID.randomUUID().toString().replaceAll("-", "");
//
//            Future sent = server.send(new ProducerRecord<>(topic, key, jsonArray.toJSONString()));
////            log.info("消息发送状态：{}",sent.isDone());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
