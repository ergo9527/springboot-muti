package com.ergo;

import com.alibaba.fastjson.JSONObject;
import com.ergo.utils.KafkaSendMsgUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.util.Random;

/**
 * Hello world!
 */
@Slf4j
public class App {
    public static void main(String[] args) {
        log.debug("Hello World!");
        JSONObject jsonObject = new JSONObject();
        while (true) {
            Random random = new Random();
            jsonObject.put("msg", random.nextInt(100000));
            KafkaSendMsgUtil.sendMsg("mobile_station_dev", jsonObject);
        }

    }
}
