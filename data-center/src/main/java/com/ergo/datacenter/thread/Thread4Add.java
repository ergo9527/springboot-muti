package com.ergo.datacenter.thread;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeyEnterprise;
import com.ergo.datacenter.mapper.KeyEnterpriseMapper;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 11:52
 * @Description :
 */
public class Thread4Add implements Runnable {

    private List<JSONObject> list;

    private KeyEnterpriseMapper mapper;

    public void setList(List<JSONObject> list) {
        this.list = list;
    }

    public void setMapper(KeyEnterpriseMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void run() {
        list.stream().forEach(k -> {

            //构建KeyEnterprise
            KeyEnterprise keyEnterprise = KeyEnterprise.builder()
                    .name(k.getString("name"))
                    .industry_name(k.getString("industry_name"))
                    .industry_category(k.getString("industry_category"))
                    .regist_number(k.getString("regist_number"))
                    .credit_code(k.getString("credit_code"))
                    .industry_type(k.getString("industry_type"))
                    .phone_legal_person(k.getString("phone_legal_person"))
                    .county(k.getString("county"))
                    .industry_category_detail(k.getString("industry_category_detail"))
                    .addr_prod_business(k.getString("addr_prod_business"))
                    .recent_station_name(k.getString("recent_station_name"))
                    .lat(k.getBigDecimal("lat"))
                    .lon(k.getBigDecimal("lon"))
                    .business_status(k.getString("business_status"))
                    .build();
            //插入数据库
            mapper.insert(keyEnterprise);
        });
    }
}
