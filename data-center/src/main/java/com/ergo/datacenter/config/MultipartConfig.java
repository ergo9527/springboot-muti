package com.ergo.datacenter.config;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 10:20
 * @Description : 配置上传MultipartFile大小限制
 */
@Configuration
public class MultipartConfig {

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //单个文件最大字节数
        factory.setMaxFileSize(DataSize.ofBytes(13764043L));
        /// 总上传文件最大字节数
        factory.setMaxRequestSize(DataSize.ofBytes(137640430L));
        return factory.createMultipartConfig();
    }
}
