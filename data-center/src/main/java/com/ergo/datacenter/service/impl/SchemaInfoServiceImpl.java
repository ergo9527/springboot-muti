package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ergo.datacenter.entity.ColumnAssetInfo;
import com.ergo.datacenter.entity.TableAssetInfo;
import com.ergo.datacenter.mapper.ColumnAssetInfoMapper;
import com.ergo.datacenter.mapper.SchemaInfoMapper;
import com.ergo.datacenter.mapper.TableAssetInfoMapper;
import com.ergo.datacenter.service.SchemaInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/6/26 11:01
 * @Description :
 */
@Slf4j
@Service
public class SchemaInfoServiceImpl implements SchemaInfoService {

    @Autowired
    private SchemaInfoMapper schemaInfoMapper;

    @Autowired
    private TableAssetInfoMapper tableAssetInfoMapper;

    @Autowired
    private ColumnAssetInfoMapper columnAssetInfoMapper;

    /**
     * 同步数据中台资产: 所有表信息、字段信息等
     */
    @Override
    public void syncDataCenterAsset(String code) {

        QueryWrapper<TableAssetInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("code", code);
        TableAssetInfo t = tableAssetInfoMapper.selectOne(wrapper);
        System.out.println("表名：" + t.getTableName());

        //根据表名获取表的字段信息
        List<ColumnAssetInfo> assertColumnList = schemaInfoMapper.findTableColumns(t.getTableName());

        //获取字段注释
        List<Map<String, Object>> list = schemaInfoMapper.findColumnCommentMap(code);
        Map<String, String> commentMap = new HashMap<>();
        list.stream().forEach(m -> {
            commentMap.put(m.get("column_name").toString(), m.get("column_comment").toString());
        });

        int columnWeight = 1;
        for (ColumnAssetInfo c : assertColumnList) {
            //设置字段所属表id
            c.setTableId(t.getId());
            //设置字段所属表名
            c.setTableName(t.getTableName());
            //设置字段展示权重
            c.setWeight(columnWeight++);
            //插入数据资产-字段表
            columnAssetInfoMapper.insert(c);
        }
    }
}
