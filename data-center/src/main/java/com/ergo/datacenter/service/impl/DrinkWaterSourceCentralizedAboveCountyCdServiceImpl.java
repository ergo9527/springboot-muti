package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkWaterSourceCentralizedAboveCountyCd;
import com.ergo.datacenter.entity.DrinkingWaterSource;
import com.ergo.datacenter.mapper.DrinkWaterSourceCentralizedAboveCountyCdMapper;
import com.ergo.datacenter.service.DrinkWaterSourceCentralizedAboveCountyCdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-11
 */
@Service
public class DrinkWaterSourceCentralizedAboveCountyCdServiceImpl extends ServiceImpl<DrinkWaterSourceCentralizedAboveCountyCdMapper, DrinkWaterSourceCentralizedAboveCountyCd> implements DrinkWaterSourceCentralizedAboveCountyCdService {

    @Override
    public void add(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");
        System.out.println(features.size());
        features.stream().forEach(a -> {
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) a;
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(map);
            JSONObject f = JSONObject.parseObject(jsonObject1.toString());

            JSONObject properties = f.getJSONObject("attributes");
            JSONObject geometry = f.getJSONObject("geometry");
//            JSONArray coordinates = geometry.getJSONArray("coordinates");

            DrinkWaterSourceCentralizedAboveCountyCd source = DrinkWaterSourceCentralizedAboveCountyCd.builder()
                    .sydName(properties.getString("SYDMC"))
                    .sydLevel(properties.getString("SYDJB"))
                    .sensitiveReceptorType(properties.getString("HJMGSTLX"))
                    .areaBelong(properties.getString("SZQX"))
                    .areaCode(properties.getString("QXDM"))
                    .basinName(properties.getString("所属流"))
                    .riverName(properties.getString("所属河"))
                    .sydType(properties.getString("水源地"))
                    .lat(geometry.getBigDecimal("y"))
                    .lon(geometry.getBigDecimal("x"))
                    .build();
            baseMapper.insert(source);
        });
    }
}
