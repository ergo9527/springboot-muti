package com.ergo.datacenter.service;


import com.ergo.datacenter.entity.DataAnalysisCategory;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 14:55
 * @Description :
 */
public interface DataAnalysisCategoryService {

    void addAnalysisCategoryNode(DataAnalysisCategory node);

    List<DataAnalysisCategory> recursivelyFindChilds(String pid);

    List<DataAnalysisCategory> recursivelyFindDataMartTree();

    void addOne(DataAnalysisCategory dataAnalysisCategory);
}
