package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeySupervisionEnterprise;
import com.ergo.datacenter.entity.NationalNatureReserveCd;
import com.ergo.datacenter.mapper.NationalNatureReserveCdMapper;
import com.ergo.datacenter.service.NationalNatureReserveCdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Service
public class NationalNatureReserveCdServiceImpl extends ServiceImpl<NationalNatureReserveCdMapper, NationalNatureReserveCd> implements NationalNatureReserveCdService {

    @Override
    public void add(JSONObject jsonObject) {
        List<NationalNatureReserveCd> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("attributes");
            JSONObject metry = feature.getJSONObject("geometry");

            NationalNatureReserveCd reserveCd = NationalNatureReserveCd.builder()
                    .name(properties.getString("名称"))
                    .areaBelong(properties.getString("所属县"))
                    .level(properties.getString("级别"))
                    .protectArea(properties.getString("保护区"))
                    .lon(metry.getBigDecimal("x"))
                    .lat(metry.getBigDecimal("y"))
                    .build();
            try {
                baseMapper.insert(reserveCd);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(reserveCd);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
        err.stream().forEach(e -> {
            System.out.println(e);
        });
    }
}
