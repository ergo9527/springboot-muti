package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkWaterSourceIntakeReserveCd;
import com.ergo.datacenter.mapper.DrinkWaterSourceIntakeReserveCdMapper;
import com.ergo.datacenter.service.DrinkWaterSourceIntakeReserveCdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Service
public class DrinkWaterSourceIntakeReserveCdServiceImpl extends ServiceImpl<DrinkWaterSourceIntakeReserveCdMapper, DrinkWaterSourceIntakeReserveCd> implements DrinkWaterSourceIntakeReserveCdService {

    @Override
    public void add(JSONObject jsonObject) {
        List<DrinkWaterSourceIntakeReserveCd> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("attributes");
            JSONObject metry = feature.getJSONObject("geometry");

            DrinkWaterSourceIntakeReserveCd reserveCd = DrinkWaterSourceIntakeReserveCd.builder()
                    .areaBelong(properties.getString("县市区"))
                    .waterSource(properties.getString("水源地"))
                    .waterIntake(properties.getString("取水口"))
                    .serviceCity(properties.getString("服务城"))
                    .waterIntakePlace(properties.getString("取水_1"))
                    .lon(metry.getBigDecimal("x"))
                    .lat(metry.getBigDecimal("y"))
                    .build();
            System.out.println(reserveCd);
            try {
                baseMapper.insert(reserveCd);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(reserveCd);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
        err.stream().forEach(e -> {
            System.out.println(e);
        });
    }
}
