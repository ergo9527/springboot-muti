package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeySupervisionEnterprise;
import com.ergo.datacenter.entity.RiverPolicyPoint;
import com.ergo.datacenter.mapper.RiverPolicyPointMapper;
import com.ergo.datacenter.service.RiverPolicyPointService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
@Service
public class RiverPolicyPointServiceImpl extends ServiceImpl<RiverPolicyPointMapper, RiverPolicyPoint> implements RiverPolicyPointService {

    @Override
    public void add(JSONObject jsonObject) {
        List<RiverPolicyPoint> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");

            String desc = properties.getString("desc");
            if (StringUtils.isEmpty(desc)) {
                desc = "";
            }

            RiverPolicyPoint policyPoint = RiverPolicyPoint.builder()
                    .name(properties.getString("Name"))
                    .type(properties.getString("Layer"))
                    .description(desc)
                    .lon((BigDecimal) geometry.getBigDecimal("x"))
                    .lat((BigDecimal) geometry.getBigDecimal("y"))
                    .build();
            try {
                baseMapper.insert(policyPoint);
//                System.out.println(policyPoint);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(policyPoint);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
        err.stream().forEach(e -> {
            System.out.println(e);
        });
    }
}
