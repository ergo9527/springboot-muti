package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.ObdDeviceCarInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-11-19
 */
public interface ObdDeviceCarInfoService extends IService<ObdDeviceCarInfo> {

    void add(JSONObject jsonObject);
}
