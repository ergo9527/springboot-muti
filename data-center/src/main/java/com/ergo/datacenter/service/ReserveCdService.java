package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.ReserveCd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-26
 */
public interface ReserveCdService extends IService<ReserveCd> {

    void addBatch(JSONObject jsonObject);

}
