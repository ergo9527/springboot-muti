package com.ergo.datacenter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ergo.datacenter.result.ResultMsg;


/**
 * @Author : liuxin
 * @Date: 2021/6/15 17:35
 * @Description :
 */
public interface TidbBaseService {

    String findByPage(Page page, String sql);

    ResultMsg findList(String sql);

    String findMap(String sql);
}
