package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ergo.datacenter.entity.SlagTruck;
import com.ergo.datacenter.mapper.SlagTruckMapper;
import com.ergo.datacenter.service.SlagTruckService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 16:11
 * @Description :
 */
@Service
public class SlagTruckServiceImpl extends ServiceImpl<SlagTruckMapper, SlagTruck> implements SlagTruckService {

    @Override
    public void add(JSONObject jsonObject) {

        JSONArray data = jsonObject.getJSONArray("data");
        data.stream().forEach(d -> {
            List item = (List) d;

//            item.stream().forEach(i->{
//                System.out.println(i.getClass());
//            });

            SlagTruck slagTruck = SlagTruck.builder()
                    .score(item.get(0))
                    .lat(BigDecimal.valueOf((Double) item.get(1)))
                    .lon(BigDecimal.valueOf((Double) item.get(2)))
                    .pollutant(jsonObject.getString("pollutant"))
                    .year(jsonObject.getInteger("year"))
                    .month(jsonObject.getInteger("month"))
                    .build();
            baseMapper.insert(slagTruck);
        });
    }
}
