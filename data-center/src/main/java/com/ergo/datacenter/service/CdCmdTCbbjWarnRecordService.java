package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.CdCmdTCbbjWarnRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-10-21
 */
public interface CdCmdTCbbjWarnRecordService extends IService<CdCmdTCbbjWarnRecord> {

}
