package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.IndustryParkSewageFacilities;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
public interface IndustryParkSewageFacilitiesService extends IService<IndustryParkSewageFacilities> {

    void insertBatch(JSONObject jsonObject);
}
