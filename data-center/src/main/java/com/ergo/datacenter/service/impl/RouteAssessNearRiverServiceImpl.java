package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RouteAssess;
import com.ergo.datacenter.entity.RouteAssessNearRiver;
import com.ergo.datacenter.mapper.RouteAssessNearRiverMapper;
import com.ergo.datacenter.service.RouteAssessNearRiverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-18
 */
@Service
public class RouteAssessNearRiverServiceImpl extends ServiceImpl<RouteAssessNearRiverMapper, RouteAssessNearRiver> implements RouteAssessNearRiverService {

    @Override
    public void addBatch(JSONObject jsonObject) {

        List<RouteAssessNearRiver> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("properties");

            RouteAssessNearRiver routeAssess = RouteAssessNearRiver.builder()
                    .name(properties.getString("Name"))
                    .kind(properties.getString("Kind"))
                    .distance(properties.getDouble("distance"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                System.out.println(routeAssess);
                baseMapper.insert(routeAssess);
            } catch (Exception e) {
                err.add(routeAssess);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
        err.stream().forEach(e -> System.out.println(e.toString()));
    }
}
