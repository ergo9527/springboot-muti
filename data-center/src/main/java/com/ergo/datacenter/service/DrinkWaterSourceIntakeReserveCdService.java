package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkWaterSourceIntakeReserveCd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
public interface DrinkWaterSourceIntakeReserveCdService extends IService<DrinkWaterSourceIntakeReserveCd> {
    void add(JSONObject jsonObject);
}
