package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.MapConfigPolygon;
import com.ergo.datacenter.entity.RouteAssess;
import com.ergo.datacenter.entity.WasteSolid;
import com.ergo.datacenter.mapper.MapConfigPolygonMapper;
import com.ergo.datacenter.mapper.RouteAssessMapper;
import com.ergo.datacenter.service.RouteAssessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-18
 */
@Service
public class RouteAssessServiceImpl extends ServiceImpl<RouteAssessMapper, RouteAssess> implements RouteAssessService {

    @Autowired
    private MapConfigPolygonMapper configPolygonMapper;

    @Override
    public void addBatch(JSONObject jsonObject) {
        List<RouteAssess> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("properties");

            RouteAssess routeAssess = RouteAssess.builder()
                    .name(properties.getString("Name"))
                    .kind(properties.getString("Kind"))
                    .fid(properties.getLong("FID_评估路线"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(routeAssess);
            } catch (Exception e) {
                err.add(routeAssess);
                return;
            }
            System.out.println("插入失败条数: " + err.size());
            err.stream().forEach(e -> System.out.println(e.toString()));
        });
    }
}
