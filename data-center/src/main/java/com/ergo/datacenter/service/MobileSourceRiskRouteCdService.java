package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.MobileSourceRiskRouteCd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
public interface MobileSourceRiskRouteCdService extends IService<MobileSourceRiskRouteCd> {
    void add(JSONObject jsonObject);
}
