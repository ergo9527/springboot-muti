package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeySupervisionEnterprise;
import com.ergo.datacenter.entity.RouteAssessRiskExclusion;
import com.ergo.datacenter.mapper.KeySupervisionEnterpriseMapper;
import com.ergo.datacenter.service.KeySupervisionEnterpriseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-09-27
 */
@Service
public class KeySupervisionEnterpriseServiceImpl extends ServiceImpl<KeySupervisionEnterpriseMapper, KeySupervisionEnterprise> implements KeySupervisionEnterpriseService {

    @Override
    public void addBatch(JSONObject jsonObject) {
        List<KeySupervisionEnterprise> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("attributes");
            JSONObject metry = feature.getJSONObject("geometry");

            KeySupervisionEnterprise key = KeySupervisionEnterprise.builder()
                    .index(properties.getLong("XH"))
                    .entName(properties.getString("QYMC"))
                    .areaBelong(properties.getString("SZQX"))
                    .lon(metry.getBigDecimal("x"))
                    .lat(metry.getBigDecimal("y"))
                    .dangerGoodsType(properties.getString("WXPZL"))
                    .relativeRiver(properties.getString("GLHL"))
                    .relativeEnvirRiskAcceptor(properties.getString("SHJMGST"))
                    .majorEnvirRiskType(properties.getString("ZYHJFXLX"))
                    .waterRiskLevel(properties.getString("LYHJFXDJ"))
                    .reservePlanAssessLevel(properties.getString("YAPGDJ"))
                    .valueQ(properties.getDoubleValue("Q值"))
                    .remark(properties.getString("BZ"))
                    .build();
            try {
                baseMapper.insert(key);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(key);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
        err.stream().forEach(e -> {
            System.out.println(e);
        });
    }
}
