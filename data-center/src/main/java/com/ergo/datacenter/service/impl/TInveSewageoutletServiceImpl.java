package com.ergo.datacenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ergo.datacenter.entity.TInveSewageoutlet;
import com.ergo.datacenter.mapper.TInveSewageoutletMapper;
import com.ergo.datacenter.service.TInveSewageoutletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-09-09
 */
@Service
public class TInveSewageoutletServiceImpl extends ServiceImpl<TInveSewageoutletMapper, TInveSewageoutlet> implements TInveSewageoutletService {

    @Override
    public void CleanData() {
        List<TInveSewageoutlet> list = baseMapper.selectList(null);
        list.stream().forEach(o -> {
            if (o.getFLongitude() == null && o.getFLatitude() == null) {  //经纬度为NULL,不做处理,跳到下一次循环
                return;
            } else if (o.getFLongitude() == null || o.getFLatitude() == null) {     //有一个为null,则都设为null
                o.setFLongitude(null);
                o.setFLatitude(null);
            } else {    //经纬度都不为空
                BigDecimal longitude = new BigDecimal(o.getFLongitude());
                BigDecimal latitude = new BigDecimal(o.getFLatitude());
                if ("0".equals(o.getFLongitude()) || "0".equals(o.getFLatitude())) {  //经纬度为0,设为NULL
                    o.setFLongitude(null);
                    o.setFLatitude(null);
                } else if (longitude.compareTo(latitude) == -1) {   //经度比纬度数值小,数据需要修正,交换经纬度的值
                    String temp = o.getFLongitude();
                    o.setFLongitude(o.getFLatitude());
                    o.setFLatitude(temp);
                }
            }
            baseMapper.update(o, null);
        });

    }

    @Override
    public List<TInveSewageoutlet> findAll() {
        return baseMapper.selectList(null);
    }
}
