package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.ScenarioDynamicMiddle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-10-09
 */
public interface ScenarioDynamicMiddleService extends IService<ScenarioDynamicMiddle> {

}
