package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.StApi;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-09-07
 */
public interface StApiService extends IService<StApi> {

}
