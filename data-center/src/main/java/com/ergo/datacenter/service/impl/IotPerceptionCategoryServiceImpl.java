package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.IotPerceptionCategory;
import com.ergo.datacenter.mapper.IotPerceptionCategoryMapper;
import com.ergo.datacenter.service.IotPerceptionCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
@Service
public class IotPerceptionCategoryServiceImpl extends ServiceImpl<IotPerceptionCategoryMapper, IotPerceptionCategory> implements IotPerceptionCategoryService {

}
