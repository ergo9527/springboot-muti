package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.DataAssetColumns;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
public interface DataAssetColumnsService extends IService<DataAssetColumns> {

}
