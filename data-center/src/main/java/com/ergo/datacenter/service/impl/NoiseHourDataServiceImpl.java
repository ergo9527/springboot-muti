package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.NoiseHourData;
import com.ergo.datacenter.mapper.NoiseHourDataMapper;
import com.ergo.datacenter.service.NoiseHourDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2022-03-11
 */
@Service
public class NoiseHourDataServiceImpl extends ServiceImpl<NoiseHourDataMapper, NoiseHourData> implements NoiseHourDataService {

}
