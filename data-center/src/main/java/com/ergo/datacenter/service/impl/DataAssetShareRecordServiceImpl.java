package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.DataAssetShareRecord;
import com.ergo.datacenter.mapper.DataAssetShareRecordMapper;
import com.ergo.datacenter.service.DataAssetShareRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2022-03-24
 */
@Service
public class DataAssetShareRecordServiceImpl extends ServiceImpl<DataAssetShareRecordMapper, DataAssetShareRecord> implements DataAssetShareRecordService {

}
