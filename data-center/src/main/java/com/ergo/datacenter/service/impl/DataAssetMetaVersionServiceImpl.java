package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ergo.datacenter.entity.DataAssetColumns;
import com.ergo.datacenter.entity.DataAssetMetaVersion;
import com.ergo.datacenter.entity.DataAssetTables;
import com.ergo.datacenter.mapper.DataAssetColumnsMapper;
import com.ergo.datacenter.mapper.DataAssetMetaVersionMapper;
import com.ergo.datacenter.mapper.DataAssetTablesMapper;
import com.ergo.datacenter.service.DataAssetMetaVersionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
@Service
public class DataAssetMetaVersionServiceImpl extends ServiceImpl<DataAssetMetaVersionMapper, DataAssetMetaVersion> implements DataAssetMetaVersionService {

    @Autowired
    private DataAssetTablesMapper assetTablesMapper;

    @Autowired
    private DataAssetColumnsMapper columnsMapper;

    @Override
    public void doSync() {
        //查询所有数据资产
        List<DataAssetTables> assetTablesList = assetTablesMapper.selectList(null);
        //遍历数据资产
        assetTablesList.stream().forEach(asset -> {
            //查询该资产表字段集合
            QueryWrapper<DataAssetColumns> columnsQueryWrapper = new QueryWrapper<>();
            columnsQueryWrapper.eq("tableId", asset.getId());
            List<DataAssetColumns> columnsList = columnsMapper.selectList(columnsQueryWrapper);
            //遍历字段集合,构建字段元数据json
            JSONObject columnMeta = new JSONObject();
            columnsList.stream().forEach(column -> {
                columnMeta.put(column.getColumnName(), column.getDataType());
            });
            //构建资产版本记录
            DataAssetMetaVersion metaVersion = DataAssetMetaVersion.builder()
                    .version("V1.0.0")
                    .columnMetaData(columnMeta.toJSONString())
                    .assetId(asset.getId())
                    .publishedAt(new Date()).build();
            //插入版本记录
            baseMapper.insert(metaVersion);
        });
    }
}
