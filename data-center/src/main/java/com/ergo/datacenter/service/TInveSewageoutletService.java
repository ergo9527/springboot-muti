package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.TInveSewageoutlet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-09-09
 */
public interface TInveSewageoutletService extends IService<TInveSewageoutlet> {

    void CleanData();

    List<TInveSewageoutlet> findAll();

}
