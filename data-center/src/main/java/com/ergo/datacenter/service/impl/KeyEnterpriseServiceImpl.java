package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ergo.datacenter.entity.KeyEnterprise;
import com.ergo.datacenter.mapper.KeyEnterpriseMapper;
import com.ergo.datacenter.service.KeyEnterpriseService;
import com.ergo.datacenter.thread.Thread4Add;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 9:27
 * @Description :
 */
@Service
public class KeyEnterpriseServiceImpl extends ServiceImpl<KeyEnterpriseMapper, KeyEnterprise> implements KeyEnterpriseService {

    @Autowired
    private KeyEnterpriseMapper mapper;

    @Override
    public void analysis(List<JSONObject> list) {
        for (int i = 1; i <= 200; i++) {
            List<JSONObject> subList = list.subList((i - 1) * 453, 453 * i - 1);
            Thread4Add t = new Thread4Add();
            t.setMapper(mapper);
            t.setList(subList);
            t.run();
        }
    }
}
