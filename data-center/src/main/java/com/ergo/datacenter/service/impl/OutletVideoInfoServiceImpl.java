package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.OutletVideoInfo;
import com.ergo.datacenter.mapper.OutletVideoInfoMapper;

import com.ergo.datacenter.service.OutletVideoInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-05
 */
@Service
public class OutletVideoInfoServiceImpl extends ServiceImpl<OutletVideoInfoMapper, OutletVideoInfo> implements OutletVideoInfoService {

    @Override
    public String token() {
        //用户key
        String appKey = "87876567";
        //用户秘钥
        String appSecret = "slWw233Lk34es343ksd1";
        //请求地址
        String url = "http://125.70.9.131:8000/token/get-token";
        //请求参数
        JSONObject map = new JSONObject();
        map.put("appKey", appKey);
        map.put("appSecret", appSecret);

        //设置headerMap参数，对应httpHeader参数，用于签名计算
        HttpHeaders headers = new HttpHeaders();

        //发送http请求
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(map.toJSONString(), headers);
        JSONObject response = restTemplate.postForObject(url, httpEntity, JSONObject.class);
        return response.getString("accessToken");
    }

    @Override
    public void add() {

        //请求地址
        String url = "http://125.70.9.131:8000/drain-hole/resource";
        //请求参数
        JSONObject map = new JSONObject();
        map.put("page", 2);
        map.put("limit", 1000);

        //设置headerMap参数，对应httpHeader参数，用于签名计算
        HttpHeaders headers = new HttpHeaders();
        headers.add("accessToken", token());

        //发送http请求
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(map.toJSONString(), headers);
        JSONObject response = restTemplate.postForObject(url, httpEntity, JSONObject.class);
        System.out.println(response);

        //数据处理
        System.out.println(response.get("data").getClass());
        JSONArray data = response.getJSONArray("data");
        System.out.println("排口数量：" + data.size());
        data.stream().forEach(d -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(d);
            JSONObject jsonObject = JSONObject.parseObject(jsonObject1.toString());
            OutletVideoInfo outletVideoInfo = OutletVideoInfo.builder()
                    .warn(jsonObject.getInteger("warn"))
                    .name(jsonObject.getString("name"))
                    .code(jsonObject.getString("code"))
                    .lon(jsonObject.getDouble("lng"))
                    .lat(jsonObject.getDouble("lat"))
                    .phone(jsonObject.getString("phone"))
                    .duty(jsonObject.getString("duty"))
                    .supervise(jsonObject.getString("supervise"))
                    .riverlakeid(jsonObject.getInteger("riverLakeId"))
                    .status(jsonObject.getInteger("status"))
                    .type(jsonObject.getInteger("type"))
                    .position(jsonObject.getInteger("position"))
                    .build();

            baseMapper.insert(outletVideoInfo);
        });
    }


}
