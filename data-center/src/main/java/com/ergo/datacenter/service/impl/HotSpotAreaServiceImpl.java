package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.HotSpotArea;
import com.ergo.datacenter.entity.WasteSolid;
import com.ergo.datacenter.mapper.HotSpotAreaMapper;
import com.ergo.datacenter.service.HotSpotAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@Service
public class HotSpotAreaServiceImpl extends ServiceImpl<HotSpotAreaMapper, HotSpotArea> implements HotSpotAreaService {

    @Override
    public void add(JSONObject jsonObject) {
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("properties");
//            feature.put("properties", attributes);
//            feature.put("type", "Feature");
//            feature.remove("attributes");
//            JSONObject geometry = feature.getJSONObject("geometry");
//            JSONArray ring = geometry.getJSONArray("rings");
//            JSONArray coordinates = new JSONArray();
//            JSONArray hhh = new JSONArray();
//            for (int i = 0; i < ring.size(); i++) {
//                JSONArray array = (JSONArray) ring.get(i);
//                for (int k = 0; k < array.size(); k++) {
//                    hhh.add(array.get(k));
//                }
//            }
//            coordinates.add(hhh);
//            geometry.put("coordinates", coordinates);
//            geometry.put("type", "MultiPolygon");
//            geometry.remove("rings");
//            feature.put("geometry", geometry);
            HotSpotArea hotSpotArea = HotSpotArea.builder()
                    .county(properties.getString("County"))
                    .town(properties.getString("TOWN"))
                    .gridId(properties.getString("GRID_ID"))
                    .wRxy(properties.get("wRxy").toString())
                    .aRxy(properties.get("aRxy").toString())
                    .rxy(properties.get("Rxy").toString())
                    .geometryObj(feature.toJSONString())
                    .build();

            baseMapper.insert(hotSpotArea);

        });
    }
}
