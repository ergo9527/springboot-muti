package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.DataAssetShareRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2022-03-24
 */
public interface DataAssetShareRecordService extends IService<DataAssetShareRecord> {

}
