package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RiverPolicyWater;
import com.ergo.datacenter.entity.WaterSystem;
import com.ergo.datacenter.mapper.RiverPolicyWaterMapper;
import com.ergo.datacenter.service.RiverPolicyWaterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@Service
public class RiverPolicyWaterServiceImpl extends ServiceImpl<RiverPolicyWaterMapper, RiverPolicyWater> implements RiverPolicyWaterService {

    @Override
    public void addBatch(JSONObject jsonObject) {
        JSONObject geometryObj = new JSONObject();

        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            JSONArray array = new JSONArray();
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("properties");
            feature.put("properties", properties);
            feature.put("type", "Feature");
            properties.remove("Style");
            properties.remove("FID");
            properties.put("name", properties.getString("Name"));
            properties.put("layer", properties.getString("Layer"));
            properties.remove("Name");
            properties.remove("Layer");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray coordinates = geometry.getJSONArray("coordinates");
            geometry.put("coordinates", coordinates);
            geometry.put("type", "LineString");
            feature.put("geometry",geometry);
//            array.add(feature);
//            geometryObj.put("features", array);
//            geometryObj.put("type", "FeatureCollection");
//            geometryObj.put("properties", properties);
            RiverPolicyWater riverPolicyWater = RiverPolicyWater.builder()
                    .name(properties.getString("name"))
                    .layer(properties.getString("name"))
                    .geometryObj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(riverPolicyWater);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(riverPolicyWater + "插入失败");
                return;
            }
        });
    }
}
