package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.DataAssetTables;
import com.ergo.datacenter.mapper.DataAssetTablesMapper;
import com.ergo.datacenter.service.DataAssetTablesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2022-03-15
 */
@Service
public class DataAssetTablesServiceImpl extends ServiceImpl<DataAssetTablesMapper, DataAssetTables> implements DataAssetTablesService {

}
