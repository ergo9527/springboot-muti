package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 9:27
 * @Description :
 */
public interface KeyEnterpriseService {

    void analysis(List<JSONObject> list);
}
