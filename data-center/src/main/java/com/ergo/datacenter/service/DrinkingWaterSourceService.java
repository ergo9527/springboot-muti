package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkingWaterSource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
public interface DrinkingWaterSourceService extends IService<DrinkingWaterSource> {

    void insertBatch(JSONObject jsonObject);
}
