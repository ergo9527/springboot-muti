package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.EmissionEntInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-11-23
 */
public interface EmissionEntInfoService extends IService<EmissionEntInfo> {

    void add(JSONObject jsonObject);
}
