package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.HotSpotArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
public interface HotSpotAreaService extends IService<HotSpotArea> {

    void add(JSONObject jsonObject);

}
