package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.MapConfigPolygon;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
public interface MapConfigPolygonService extends IService<MapConfigPolygon> {

}
