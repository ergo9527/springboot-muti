package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.ScenarioAnalysisCategory;
import com.ergo.datacenter.mapper.ScenarioAnalysisCategoryMapper;
import com.ergo.datacenter.service.ScenarioAnalysisCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-08
 */
@Service
public class ScenarioAnalysisCategoryServiceImpl extends ServiceImpl<ScenarioAnalysisCategoryMapper, ScenarioAnalysisCategory> implements ScenarioAnalysisCategoryService {

}
