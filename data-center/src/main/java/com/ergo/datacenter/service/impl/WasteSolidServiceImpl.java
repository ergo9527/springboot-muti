package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.MapConfigPolygon;
import com.ergo.datacenter.entity.WasteSolid;
import com.ergo.datacenter.mapper.MapConfigPolygonMapper;
import com.ergo.datacenter.mapper.WasteSolidMapper;
import com.ergo.datacenter.service.MapConfigPolygonService;
import com.ergo.datacenter.service.WasteSolidService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-16
 */
@Service
public class WasteSolidServiceImpl extends ServiceImpl<WasteSolidMapper, WasteSolid> implements WasteSolidService {

    @Autowired
    private MapConfigPolygonMapper configPolygonMapper;

    @Override
    public void addBatch(JSONObject jsonObject) {
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray ring = geometry.getJSONArray("rings");
            JSONArray coordinates = new JSONArray();
            for (int i = 0; i < ring.size(); i++) {
                coordinates.add(ring.get(i));
            }
            geometry.put("coordinates", coordinates);
            geometry.put("type", "Polygon");
            geometry.remove("rings");
            feature.put("geometry",geometry);
            WasteSolid wasteSolid = WasteSolid.builder()
                    .wasteType(attributes.getString("固废类"))
                    .referDate("2020")
                    .areaBelong(attributes.getString("分属区"))
                    .areaCovered(attributes.getBigDecimal("面积"))
//                    .wasteCode(attributes.getString("编码"))
//                    .isDemolitionSlag(attributes.getString("拆迁渣"))
                    .lat(attributes.getBigDecimal("中心纬"))
                    .lon(attributes.getBigDecimal("中心经"))
                    .polygonObj(feature.toJSONString())
                    .build();

             baseMapper.insert(wasteSolid);

        });
    }

    @Override
    public void addConfig() {
        List<WasteSolid> list = baseMapper.selectList(null);
        list.stream().forEach(w -> {
            System.out.println(w.getId());
            MapConfigPolygon mapConfigPolygon = MapConfigPolygon.builder()
                    .polygonId(w.getId())
                    .pid(18L)
                    .color("#ffcc66")
                    .fillOpacity(0.3)
                    .createBy(2L)
                    .updateBy(2L)
                    .build();
            configPolygonMapper.insert(mapConfigPolygon);
        });
    }

    @Override
    public void changePolygonToLine() {
        List<WasteSolid> list = baseMapper.selectList(null);
        list.stream().forEach(w -> {
            JSONObject polygonObj = JSONObject.parseObject(w.getPolygonObj());
            System.out.println(polygonObj.getJSONArray("features"));
            JSONArray features = polygonObj.getJSONArray("features");
            JSONObject feature = (JSONObject) features.get(0);
            w.setPolygonObj(feature.toJSONString());
            baseMapper.updateById(w);
        });
    }
}
