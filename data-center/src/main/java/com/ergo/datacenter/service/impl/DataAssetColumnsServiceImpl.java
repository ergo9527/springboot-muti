package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.DataAssetColumns;
import com.ergo.datacenter.mapper.DataAssetColumnsMapper;
import com.ergo.datacenter.service.DataAssetColumnsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
@Service
public class DataAssetColumnsServiceImpl extends ServiceImpl<DataAssetColumnsMapper, DataAssetColumns> implements DataAssetColumnsService {

}
