package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DataAnalysisCategory;
import com.ergo.datacenter.entity.SkynetCamera;
import com.ergo.datacenter.mapper.SkynetCameraMapper;
import com.ergo.datacenter.service.SkynetCameraService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-12
 */
@Service
public class SkynetCameraServiceImpl extends ServiceImpl<SkynetCameraMapper, SkynetCamera> implements SkynetCameraService {

    @Override
    public void addCamera(String sessionId) {

        //请求地址
        String url = "http://10.1.231.87:8080/mobile/organList";

        //请求参数
        JSONObject map = new JSONObject();
        map.put("sessionId", sessionId);

        //设置HttpHeader参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //发送http请求
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(map.toJSONString(), headers);
        JSONObject response = restTemplate.postForObject(url, httpEntity, JSONObject.class);
        JSONObject data = response.getJSONObject("data");
        JSONArray organs = data.getJSONArray("childOrgan");
        JSONObject SkyNet = organs.getJSONObject(2);
        JSONArray skyNetChilds = SkyNet.getJSONArray("childOrgan");
        JSONObject cd = skyNetChilds.getJSONObject(0);
        JSONArray cdChilds = cd.getJSONArray("childOrgan");
        cdChilds.stream().forEach(c -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(c);
            JSONObject county = JSONObject.parseObject(jsonObject1.toString());
            JSONArray countyChilds = county.getJSONArray("childOrgan");
            countyChilds.stream().forEach(p -> {
                cn.hutool.json.JSONObject jsonObject2 = JSONUtil.parseObj(p);
                JSONObject pcs = JSONObject.parseObject(jsonObject2.toString());
                //获取机构下设备
                //请求地址
                String url2 = "http://10.1.231.87:8080/mobile/cameraList";
                //请求参数
                JSONObject map2 = new JSONObject();
                map2.put("sessionId", sessionId);
                map2.put("organId", pcs.getString("id"));
                map2.put("outPlatform", true);

                //设置HttpHeader参数
                HttpHeaders headers2 = new HttpHeaders();
                headers2.setContentType(MediaType.APPLICATION_JSON);

                //发送http请求
                RestTemplate restTemplate2 = new RestTemplate();
                HttpEntity<String> httpEntity2 = new HttpEntity<>(map2.toJSONString(), headers2);
                JSONObject response2 = restTemplate2.postForObject(url2, httpEntity2, JSONObject.class);
                JSONArray cameraArr = response2.getJSONArray("data");
                if(cameraArr!=null){
                    cameraArr.stream().forEach(cam -> {
                        cn.hutool.json.JSONObject jsonObject3 = JSONUtil.parseObj(cam);
                        JSONObject camera = JSONObject.parseObject(jsonObject3.toString());
                        String camId = camera.getString("id");

                        SkynetCamera skynetCamera = SkynetCamera.builder()
                                .id(camId.substring(11))
                                .name(camera.getString("camName"))
                                .orgnName(camera.getString("organName"))
                                .lat(camera.getDouble("y"))
                                .lon(camera.getDouble("x"))
                                .build();
                        baseMapper.insert(skynetCamera);
                    });
                }
            });


        });
    }
}
