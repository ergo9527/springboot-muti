package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ergo.datacenter.entity.RiskSource;
import com.ergo.datacenter.mapper.RiskSourceMapper;
import com.ergo.datacenter.service.RiskSourceService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedHashMap;

/**
 * @Author : liuxin
 * @Date: 2021/7/30 16:27
 * @Description :
 */
@Service
public class RiskSourceServiceImpl extends ServiceImpl<RiskSourceMapper, RiskSource> implements RiskSourceService {

    @Override
    public void insert(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            System.out.println(f.toString());
            LinkedHashMap feature = (LinkedHashMap) f;
            LinkedHashMap attributes = (LinkedHashMap) feature.get("attributes");
            RiskSource riskSource = RiskSource.builder()
                    .objectId(Long.valueOf(attributes.get("OBJECTID").toString()))
                    .name((String) attributes.get("Name"))
                    .county((String) attributes.get("County"))
                    .city((String) attributes.get("City"))
                    .lat(BigDecimal.valueOf((Double) attributes.get("LAT")))
                    .lon(BigDecimal.valueOf((Double) attributes.get("LON")))
                    .build();
//            baseMapper.insert(riskSource);
        });
    }
}
