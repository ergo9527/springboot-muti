package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.StApi;
import com.ergo.datacenter.mapper.StApiMapper;
import com.ergo.datacenter.service.StApiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-09-07
 */
@Service
public class StApiServiceImpl extends ServiceImpl<StApiMapper, StApi> implements StApiService {

}
