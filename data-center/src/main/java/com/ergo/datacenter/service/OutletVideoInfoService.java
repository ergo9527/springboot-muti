package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.OutletVideoInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-11-05
 */
public interface OutletVideoInfoService extends IService<OutletVideoInfo> {

    String token();

    void add();
}
