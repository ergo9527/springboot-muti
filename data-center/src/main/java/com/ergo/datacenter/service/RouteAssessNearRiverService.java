package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RouteAssessNearRiver;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-18
 */
public interface RouteAssessNearRiverService extends IService<RouteAssessNearRiver> {

    void addBatch(JSONObject jsonObject);

}
