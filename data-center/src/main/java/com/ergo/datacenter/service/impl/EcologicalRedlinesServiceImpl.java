package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.EcologicalRedlines;
import com.ergo.datacenter.entity.ReserveCd;
import com.ergo.datacenter.mapper.EcologicalRedlinesMapper;
import com.ergo.datacenter.service.EcologicalRedlinesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@Service
public class EcologicalRedlinesServiceImpl extends ServiceImpl<EcologicalRedlinesMapper, EcologicalRedlines> implements EcologicalRedlinesService {

    @Override
    public void addBatch(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray ring = geometry.getJSONArray("rings");
            geometry.put("coordinates", ring);
            geometry.put("type", "Polygon");
            geometry.remove("rings");
            JSONObject properties = feature.getJSONObject("properties");
            EcologicalRedlines redlines = EcologicalRedlines.builder()
                    .province(attributes.getString("四川省"))
                    .city(attributes.getString("市"))
                    .district(attributes.getString("县（区）"))
                    .protectedLevel(attributes.getString("保护地级别"))
                    .redlineName(attributes.getString("红线命名"))
                    .ecosystem(attributes.getString("生态系统"))
                    .leadingFunction(attributes.getString("主导功能"))
                    .protectedName(attributes.getString("保护地名称"))
                    .typeCode(attributes.getString("类型编码"))
                    .redlineCode(attributes.getString("红线编码"))
                    .areaCode(attributes.getString("区划代码"))
                    .controlMeasures(attributes.getString("管控措施"))
                    .redlineType(attributes.getString("红线类型"))
                    .area(attributes.getBigDecimal("面积（km2"))
                    .obj(feature.toJSONString())
                    .build();
            System.out.println(attributes.get("面积（km2").getClass()+": "+attributes.get("面积（km2"));
            try {
                baseMapper.insert(redlines);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(redlines + "插入失败");
                return;
            }
        });
    }
}
