package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeyAreas;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
public interface KeyAreasService extends IService<KeyAreas> {

    void add(JSONObject jsonObject);

}
