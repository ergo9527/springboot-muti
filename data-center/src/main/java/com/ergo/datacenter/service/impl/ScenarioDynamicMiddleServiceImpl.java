package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.ScenarioDynamicMiddle;
import com.ergo.datacenter.mapper.ScenarioDynamicMiddleMapper;
import com.ergo.datacenter.service.ScenarioDynamicMiddleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-09
 */
@Service
public class ScenarioDynamicMiddleServiceImpl extends ServiceImpl<ScenarioDynamicMiddleMapper, ScenarioDynamicMiddle> implements ScenarioDynamicMiddleService {

}
