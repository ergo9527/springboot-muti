package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkWaterSourceCentralizedAboveCountyCd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-10-11
 */
public interface DrinkWaterSourceCentralizedAboveCountyCdService extends IService<DrinkWaterSourceCentralizedAboveCountyCd> {
    void add(JSONObject jsonObject);
}
