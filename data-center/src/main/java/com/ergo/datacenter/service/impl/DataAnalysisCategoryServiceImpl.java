package com.ergo.datacenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ergo.datacenter.entity.DataAnalysisCategory;
import com.ergo.datacenter.mapper.DataAnalysisCategoryMapper;
import com.ergo.datacenter.service.DataAnalysisCategoryService;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 14:56
 * @Description :
 */
@Service
public class DataAnalysisCategoryServiceImpl extends ServiceImpl<DataAnalysisCategoryMapper, DataAnalysisCategory> implements DataAnalysisCategoryService {

    @Override
    public void addAnalysisCategoryNode(DataAnalysisCategory node) {
        System.out.println(node);
        baseMapper.insert(node);
    }

    @Override
    public List<DataAnalysisCategory> recursivelyFindChilds(String pid) {
        QueryWrapper<DataAnalysisCategory> wrapper = new QueryWrapper();
        wrapper.eq("parent_id", pid);
        wrapper.eq("status", 1);
        List<DataAnalysisCategory> categories = baseMapper.selectList(wrapper);
        List<DataAnalysisCategory> sortedChildList = categories.stream().sorted(Comparator.comparing(DataAnalysisCategory::getNodeName).reversed()).collect(Collectors.toList());
        sortedChildList.stream().forEach(c -> {
            c.setChildren(recursivelyFindChilds(c.getId()));
        });
        return sortedChildList;
    }

    @Override
    public List<DataAnalysisCategory> recursivelyFindDataMartTree() {
        return recursivelyFindChilds("0");
    }

    @Override
    public void addOne(DataAnalysisCategory dataAnalysisCategory) {
        baseMapper.insert(dataAnalysisCategory);
    }
}
