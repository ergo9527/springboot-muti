package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeySupervisionEnterprise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-09-27
 */
public interface KeySupervisionEnterpriseService extends IService<KeySupervisionEnterprise> {

    void addBatch(JSONObject jsonObject);

}
