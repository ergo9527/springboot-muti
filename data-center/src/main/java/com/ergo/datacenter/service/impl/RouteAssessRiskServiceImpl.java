package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RouteAssessNearRiver;
import com.ergo.datacenter.entity.RouteAssessRisk;
import com.ergo.datacenter.mapper.RouteAssessRiskMapper;
import com.ergo.datacenter.service.RouteAssessRiskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
@Service
public class RouteAssessRiskServiceImpl extends ServiceImpl<RouteAssessRiskMapper, RouteAssessRisk> implements RouteAssessRiskService {
    @Override
    public void addBatch(JSONObject jsonObject) {
        List<RouteAssessRisk> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("properties");

            RouteAssessRisk routeAssessRisk = RouteAssessRisk.builder()
                    .name(properties.getString("LDMC"))
                    .riskLevel(properties.getString("HJFXDJ"))
                    .routeLength(properties.getInteger("LDCD"))
                    .relativeRiver(properties.getString("GLST"))
                    .shortestDistanceFromRiver(properties.getInteger("ZDLHJL"))
                    .latStart(properties.getBigDecimal("QDWD"))
                    .lonStart(properties.getBigDecimal("QDJD"))
                    .latEnd(properties.getBigDecimal("ZDWD"))
                    .lonEnd(properties.getBigDecimal("ZDJD"))
                    .oldName(properties.getString("OldName"))
                    .shapeLength(properties.getBigDecimal("Shape_Length"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(routeAssessRisk);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(routeAssessRisk);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
    }
}
