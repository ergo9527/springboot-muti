package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.CrossBorderSection;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author ergo
 * @since 2021-08-12
 */
public interface CrossBorderSectionService extends IService<CrossBorderSection> {

    void insertBatch(JSONObject jsonObject);
}
