package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.DataAssetMetaVersion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
public interface DataAssetMetaVersionService extends IService<DataAssetMetaVersion> {

    void doSync();

}
