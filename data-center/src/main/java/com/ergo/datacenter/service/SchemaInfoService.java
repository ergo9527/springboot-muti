package com.ergo.datacenter.service;

/**
 * @Author : liuxin
 * @Date: 2021/6/26 10:24
 * @Description :
 */
public interface SchemaInfoService {

    void syncDataCenterAsset(String code);
}
