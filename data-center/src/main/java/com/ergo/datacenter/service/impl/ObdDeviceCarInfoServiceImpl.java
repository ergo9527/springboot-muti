package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.ObdDeviceCarInfo;
import com.ergo.datacenter.mapper.ObdDeviceCarInfoMapper;
import com.ergo.datacenter.service.ObdDeviceCarInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-19
 */
@Service
public class ObdDeviceCarInfoServiceImpl extends ServiceImpl<ObdDeviceCarInfoMapper, ObdDeviceCarInfo> implements ObdDeviceCarInfoService {

    @Override
    public void add(JSONObject jsonObject) {

    }
}
