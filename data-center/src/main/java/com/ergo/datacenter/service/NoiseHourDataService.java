package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.NoiseHourData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2022-03-11
 */
public interface NoiseHourDataService extends IService<NoiseHourData> {

}
