package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.WasteSolid;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-16
 */
public interface WasteSolidService extends IService<WasteSolid> {

    void addBatch(JSONObject jsonObject);

    void addConfig();

    void changePolygonToLine();

}
