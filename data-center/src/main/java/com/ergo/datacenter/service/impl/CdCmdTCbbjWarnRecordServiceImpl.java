package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.CdCmdTCbbjWarnRecord;
import com.ergo.datacenter.mapper.CdCmdTCbbjWarnRecordMapper;
import com.ergo.datacenter.service.CdCmdTCbbjWarnRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-21
 */
@Service
public class CdCmdTCbbjWarnRecordServiceImpl extends ServiceImpl<CdCmdTCbbjWarnRecordMapper, CdCmdTCbbjWarnRecord> implements CdCmdTCbbjWarnRecordService {

}
