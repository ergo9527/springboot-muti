package com.ergo.datacenter.service.impl;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.TidbBaseService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import cn.hutool.http.HttpRequest;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import cn.hutool.crypto.asymmetric.RSA;

/**
 * @Author : liuxin
 * @Date: 2021/6/15 11:03
 * @Description: 使用Http请求调用三方接口
 */
@Slf4j
@Service
@AllArgsConstructor
@NoArgsConstructor
public class TidbBaseServiceImpl implements TidbBaseService {

    @Value("${rsa.key.private}")
    private String privateKey;

    @Value("${rsa.key.public}")
    private String sourceDataPublicKey;

    //    @Value("${spring.application.name}")
    private String applicationName = "basic-resource-service";

    /**
     * 分页查询
     *
     * @param page 分页条件
     * @param sql
     * @return
     */
    @Override
    public String findByPage(Page page, String sql) {

        /**
         * 使用公钥对请求参数JSON字符串进行加密，并转为十六进制字符串
         */
        //构造请求参数JSONObject
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("current", page.getCurrent());
        jsonObject.put("size", page.getSize());
        jsonObject.put("sql", sql);

        //发送http请求(GET)
        String body = HttpRequest.get("127.0.0.1:53033/tidb/base/page?encryptionParameters=" + getEncryptionParameters(jsonObject))
                .header("client_name", applicationName)
                .header("sign", getEncryptedSign())
                .timeout(20000)//超时，毫秒
                .execute().body();
        return JSONObject.parseObject(JSONObject.parseObject(body).get("data").toString()).get("records").toString();
    }

    /**
     * 查询List
     *
     * @param sql
     * @return
     */
    @Override
    public ResultMsg findList(String sql) {

        //构造请求参数JSONObject
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sql", sql);

        //发送http请求(GET)
        String body = HttpRequest.get("127.0.0.1:53033/tidb/base/list?encryptionParameters=" + getEncryptionParameters(jsonObject))
                .header("client_name", "basic-resource-service")
                .header("sign", getEncryptedSign())
                .timeout(20000)//超时，毫秒
                .execute().body();
        return JSONObject.toJavaObject(JSONObject.parseObject(body), ResultMsg.class);
    }

    /**
     * 查询Map
     *
     * @param sql
     * @return
     */
    @Override
    public String findMap(String sql) {

        //构造请求参数JSONObject
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sql", sql);

        //发送http请求(GET)
        String body = HttpRequest.get("127.0.0.1:53033/tidb/base/map?encryptionParameters=" + getEncryptionParameters(jsonObject))
                .header("client_name", "basic-resource-service")
                .header("sign", getEncryptedSign())
                .timeout(20000)//超时，毫秒
                .execute().body();
        return JSONObject.parseObject(body).get("data").toString();
    }

    /**
     * 生成签名并使用私钥对进行加密
     *
     * @return String 十六进制加密签名字符串
     */
    private String getEncryptedSign() {

        //生成签名(服务名+当前时间戳)
        Long nowLongtime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder(applicationName);
        String signStr = sb.append("&&").append(nowLongtime).toString();
        //私钥加密签名
        RSA rsaForPrivate = new RSA(this.privateKey, null);
        System.out.println(privateKey);
        byte[] byteSign = rsaForPrivate.encrypt(StrUtil.bytes(signStr, CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
        //将加密后的签名转为十六进制字符串
        return HexUtils.toHexString(byteSign);
    }

    /**
     * 对请求参数的jsonObject进行公钥加密
     *
     * @return String 加密后的请求参数-十六进制
     */
    private String getEncryptionParameters(JSONObject jsonObject) {
        String jsonStr = jsonObject.toJSONString();
        RSA rsaForPublic = new RSA(null, this.sourceDataPublicKey);
        System.out.println(sourceDataPublicKey);
        byte[] encrypt = rsaForPublic.encrypt(StrUtil.bytes(jsonStr, CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
        return HexUtils.toHexString(encrypt);
    }
}
