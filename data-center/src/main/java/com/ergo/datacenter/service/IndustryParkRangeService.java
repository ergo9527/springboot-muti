package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.IndustryParkRange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
public interface IndustryParkRangeService extends IService<IndustryParkRange> {
    void add(JSONObject jsonObject);
}
