package com.ergo.datacenter.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.KeyAreas;
import com.ergo.datacenter.entity.MapConfigPolygon;
import com.ergo.datacenter.mapper.KeyAreasMapper;
import com.ergo.datacenter.mapper.MapConfigPolygonMapper;
import com.ergo.datacenter.service.KeyAreasService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Service
public class KeyAreasServiceImpl extends ServiceImpl<KeyAreasMapper, KeyAreas> implements KeyAreasService {

    @Autowired
    private MapConfigPolygonMapper configPolygonMapper;

    @Override
    public void add(JSONObject jsonObject) {

        KeyAreas keyAreas = KeyAreas.builder()
                .name(jsonObject.getString("name"))
                .polygonObj(jsonObject.getJSONObject("obj").toJSONString())
                .build();
        baseMapper.insert(keyAreas);


        MapConfigPolygon mapConfigPolygon = MapConfigPolygon.builder()
                .polygonId(keyAreas.getId())
                .color(jsonObject.getString("color"))
                .fillOpacity(jsonObject.getDouble("fillOpacity"))
                .createBy(2L)
                .updateBy(2L)
                .build();
        configPolygonMapper.insert(mapConfigPolygon);
    }
}
