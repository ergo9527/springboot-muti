package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.WaterSystem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
public interface WaterSystemService extends IService<WaterSystem> {
    void addBatch(JSONObject jsonObject);
}
