package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.SkynetCamera;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-11-12
 */
public interface SkynetCameraService extends IService<SkynetCamera> {

    void addCamera(String sessionId);

}
