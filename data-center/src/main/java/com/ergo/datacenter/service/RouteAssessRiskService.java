package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RouteAssessRisk;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
public interface RouteAssessRiskService extends IService<RouteAssessRisk> {

    void addBatch(JSONObject jsonObject);
}
