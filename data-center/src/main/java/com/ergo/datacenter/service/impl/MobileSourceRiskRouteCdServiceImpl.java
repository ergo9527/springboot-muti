package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.MobileSourceRiskRouteCd;
import com.ergo.datacenter.entity.RouteAssessRisk;
import com.ergo.datacenter.mapper.MobileSourceRiskRouteCdMapper;
import com.ergo.datacenter.service.MobileSourceRiskRouteCdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
@Service
public class MobileSourceRiskRouteCdServiceImpl extends ServiceImpl<MobileSourceRiskRouteCdMapper, MobileSourceRiskRouteCd> implements MobileSourceRiskRouteCdService {

    @Override
    public void add(JSONObject jsonObject) {
        List<MobileSourceRiskRouteCd> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.remove("attributes");
            feature.put("type", "Feature");
            JSONObject properties = feature.getJSONObject("properties");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray paths = geometry.getJSONArray("paths");
            JSONArray coords = (JSONArray) paths.get(0);
            geometry.remove("paths");
            geometry.put("coordinates", coords);
            geometry.put("type", "LineString");


            MobileSourceRiskRouteCd riskRouteCd = MobileSourceRiskRouteCd.builder()
                    .roadSectionName(properties.getString("LDMC"))
                    .riskLevel(properties.getString("HJFXDJ"))
                    .roadSectionLengh(properties.getInteger("LENGTH"))
                    .associatedRiver(properties.getString("GLST"))
                    .shortestDistanceFromRiver(properties.getInteger("LHZDJL"))
                    .latStart(properties.getBigDecimal("QDWD"))
                    .lonStart(properties.getBigDecimal("QDJD"))
                    .latEnd(properties.getBigDecimal("ZDWD"))
                    .lonEnd(properties.getBigDecimal("ZDJD"))
                    .areaBelong(properties.getString("SZXQ"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(riskRouteCd);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(riskRouteCd);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
    }
}
