package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.WaterSystem;
import com.ergo.datacenter.entity.WaterSystemCdNew;
import com.ergo.datacenter.mapper.WaterSystemCdNewMapper;
import com.ergo.datacenter.service.WaterSystemCdNewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@Service
public class WaterSystemCdNewServiceImpl extends ServiceImpl<WaterSystemCdNewMapper, WaterSystemCdNew> implements WaterSystemCdNewService {

    @Override
    public void add(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray rings = geometry.getJSONArray("rings");
            geometry.put("coordinates", rings.get(0));
            geometry.put("type", "LineString");
            geometry.remove("rings");
            WaterSystemCdNew waterSystem = WaterSystemCdNew.builder()
                    .name(attributes.getString("Name"))
                    .obj(feature.toJSONString())
                    .layer(attributes.getString("Layer"))
                    .build();
            try {
                baseMapper.insert(waterSystem);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(waterSystem + "插入失败");
                return;
            }
        });
    }
}
