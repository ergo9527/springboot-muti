package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.IndustryParkRange;
import com.ergo.datacenter.entity.WaterSystem;
import com.ergo.datacenter.mapper.IndustryParkRangeMapper;
import com.ergo.datacenter.service.IndustryParkRangeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@Service
public class IndustryParkRangeServiceImpl extends ServiceImpl<IndustryParkRangeMapper, IndustryParkRange> implements IndustryParkRangeService {

    @Override
    public void add(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray ring = geometry.getJSONArray("rings");
            geometry.put("coordinates", ring);
            geometry.put("type", "Polygon");
            geometry.remove("rings");
            IndustryParkRange industryParkRange = IndustryParkRange.builder()
                    .name(attributes.getString("NAME"))
                    .county(attributes.getString("County"))
                    .countyCode(attributes.getString("QHDM"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(industryParkRange);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(industryParkRange + "插入失败");
                return;
            }
        });
    }
}
