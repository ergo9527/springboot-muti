package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.DataAssetTables;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2022-03-15
 */
public interface DataAssetTablesService extends IService<DataAssetTables> {

}
