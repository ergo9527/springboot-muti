package com.ergo.datacenter.service.impl;

import com.ergo.datacenter.entity.MapConfigPolygon;
import com.ergo.datacenter.mapper.MapConfigPolygonMapper;
import com.ergo.datacenter.service.MapConfigPolygonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Service
public class MapConfigPolygonServiceImpl extends ServiceImpl<MapConfigPolygonMapper, MapConfigPolygon> implements MapConfigPolygonService {

}
