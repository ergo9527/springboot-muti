package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.EcologicalRedlines;
import com.ergo.datacenter.entity.WaterSystem;
import com.ergo.datacenter.mapper.WaterSystemMapper;
import com.ergo.datacenter.service.WaterSystemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@Service
public class WaterSystemServiceImpl extends ServiceImpl<WaterSystemMapper, WaterSystem> implements WaterSystemService {

    @Override
    public void addBatch(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray ring = geometry.getJSONArray("rings");
            geometry.put("coordinates", ring);
            geometry.put("type", "Polygon");
            geometry.remove("rings");
            WaterSystem waterSystem = WaterSystem.builder()
                        .name(attributes.getString("Name"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(waterSystem);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(waterSystem + "插入失败");
                return;
            }
        });
    }
}
