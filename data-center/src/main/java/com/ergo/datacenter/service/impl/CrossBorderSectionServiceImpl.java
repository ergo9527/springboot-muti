package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.CrossBorderSection;
import com.ergo.datacenter.mapper.CrossBorderSectionMapper;
import com.ergo.datacenter.service.CrossBorderSectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-12
 */
@Service
public class CrossBorderSectionServiceImpl extends ServiceImpl<CrossBorderSectionMapper, CrossBorderSection> implements CrossBorderSectionService {

    @Override
    public void insertBatch(JSONObject jsonObject) {

        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(a -> {
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) a;
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(map);
            JSONObject f = JSONObject.parseObject(jsonObject1.toString());

            JSONObject properties = f.getJSONObject("properties");
            JSONObject geometry = f.getJSONObject("geometry");
            JSONArray coordinates = geometry.getJSONArray("coordinates");
            CrossBorderSection section = CrossBorderSection.builder()
                    .sectionName(properties.getString("KJDMMC"))
                    .sectionLevel(properties.getString("KJDMDJ"))
                    .sensitiveReceptorType(properties.getString("HJMGSTLX"))
                    .areaBelong(properties.getString("SZXQ"))
                    .areaCode(properties.getString("XQDM"))
                    .sectionType(properties.getString("KJDMLX"))
                    .basinName(properties.getString("所属流域"))
                    .riverName(properties.getString("河段（河流）名称"))
                    .crossAreaLevel(properties.getString("垮区域级别"))
                    .crossAreaType(properties.getString("KYLX"))
                    .lat(coordinates.getBigDecimal(1))
                    .lon(coordinates.getBigDecimal(0))
                    .build();
            baseMapper.insert(section);
        });
    }
}
