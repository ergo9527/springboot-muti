package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkingWaterSourceReserveCd;
import com.ergo.datacenter.entity.ReserveCd;
import com.ergo.datacenter.mapper.DrinkingWaterSourceReserveCdMapper;
import com.ergo.datacenter.service.DrinkingWaterSourceReserveCdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Service
public class DrinkingWaterSourceReserveCdServiceImpl extends ServiceImpl<DrinkingWaterSourceReserveCdMapper, DrinkingWaterSourceReserveCd> implements DrinkingWaterSourceReserveCdService {

    @Override
    public void add(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray ring = geometry.getJSONArray("rings");
            geometry.put("coordinates", ring);
            geometry.put("type", "Polygon");
            geometry.remove("rings");
            JSONObject properties = feature.getJSONObject("properties");
            DrinkingWaterSourceReserveCd reserveCd = DrinkingWaterSourceReserveCd.builder()
                    .name(properties.getString("SYDMC"))
                    .level(properties.getString("BHQJB"))
                    .remark(properties.getString("BZ"))
                    .shapeArea(properties.getBigDecimal("Shape_Area"))
                    .shapeLength(properties.getBigDecimal("Shape_Leng"))
                    .obj(feature.toJSONString())
                    .build();
            System.out.println(reserveCd.getShapeArea()+"||"+reserveCd.getShapeLength());
            try {
                baseMapper.insert(reserveCd);
            } catch (Exception e) {
                System.out.println(reserveCd.getName() + "插入失败");
                return;
            }
        });
    }
}
