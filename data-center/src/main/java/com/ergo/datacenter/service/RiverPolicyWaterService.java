package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RiverPolicyWater;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
public interface RiverPolicyWaterService extends IService<RiverPolicyWater> {

    void addBatch(JSONObject jsonObject);
}
