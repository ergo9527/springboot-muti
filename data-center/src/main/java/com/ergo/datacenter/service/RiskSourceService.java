package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RiskSource;

/**
 * @Author : liuxin
 * @Date: 2021/7/30 16:27
 * @Description :
 */
public interface RiskSourceService {

    void insert(JSONObject jsonObject);
}
