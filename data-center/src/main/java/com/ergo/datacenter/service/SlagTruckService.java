package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 16:09
 * @Description :
 */
public interface SlagTruckService {
    void add(JSONObject jsonObject);
}
