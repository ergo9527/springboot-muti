package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.druid.sql.visitor.functions.Reverse;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.ReserveCd;
import com.ergo.datacenter.mapper.ReserveCdMapper;
import com.ergo.datacenter.service.ReserveCdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-26
 */
@Service
public class ReserveCdServiceImpl extends ServiceImpl<ReserveCdMapper, ReserveCd> implements ReserveCdService {

    @Override
    public void addBatch(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");

        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject attributes = feature.getJSONObject("attributes");
            feature.put("properties", attributes);
            feature.put("type", "Feature");
            feature.remove("attributes");
            JSONObject geometry = feature.getJSONObject("geometry");
            JSONArray ring = geometry.getJSONArray("rings");
            geometry.put("coordinates", ring);
            geometry.put("type", "Polygon");
            geometry.remove("rings");
            JSONObject properties = feature.getJSONObject("properties");
            ReserveCd reserveCd = ReserveCd.builder()
                    .name(properties.getString("保护区名称"))
                    .title(properties.getString("NAME"))
                    .gb(properties.getInteger("GB"))
                    .level(properties.getString("级别"))
                    .area(properties.getDouble("面积_1"))
                    .reserveZoning(properties.getString("保护区分区"))
                    .obj(feature.toJSONString())
                    .build();
            try {
                baseMapper.insert(reserveCd);
            } catch (Exception e) {
                System.out.println(reserveCd.getName() + "插入失败");
                return;
            }
        });
    }
}
