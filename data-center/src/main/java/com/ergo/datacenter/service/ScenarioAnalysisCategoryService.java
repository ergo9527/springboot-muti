package com.ergo.datacenter.service;

import com.ergo.datacenter.entity.ScenarioAnalysisCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ergo
 * @since 2021-10-08
 */
public interface ScenarioAnalysisCategoryService extends IService<ScenarioAnalysisCategory> {

}
