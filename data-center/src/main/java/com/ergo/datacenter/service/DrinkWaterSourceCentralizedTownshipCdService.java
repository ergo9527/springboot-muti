package com.ergo.datacenter.service;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkWaterSourceCentralizedTownshipCd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
public interface DrinkWaterSourceCentralizedTownshipCdService extends IService<DrinkWaterSourceCentralizedTownshipCd> {
    void add(JSONObject jsonObject);
}
