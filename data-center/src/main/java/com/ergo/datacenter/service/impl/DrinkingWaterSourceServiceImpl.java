package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.CrossBorderSection;
import com.ergo.datacenter.entity.DrinkingWaterSource;
import com.ergo.datacenter.mapper.DrinkingWaterSourceMapper;
import com.ergo.datacenter.service.DrinkingWaterSourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Service
public class DrinkingWaterSourceServiceImpl extends ServiceImpl<DrinkingWaterSourceMapper, DrinkingWaterSource> implements DrinkingWaterSourceService {

    @Override
    public void insertBatch(JSONObject jsonObject) {

        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(a -> {
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) a;
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(map);
            JSONObject f = JSONObject.parseObject(jsonObject1.toString());

            JSONObject properties = f.getJSONObject("properties");
            JSONObject geometry = f.getJSONObject("geometry");
            JSONArray coordinates = geometry.getJSONArray("coordinates");

            DrinkingWaterSource source = DrinkingWaterSource.builder()
                    .sydName(properties.getString("SYDMC"))
                    .sydLevel(properties.getString("SYDJB"))
                    .sensitiveReceptorType(properties.getString("HJMGSTLX"))
                    .areaBelong(properties.getString("SZQX"))
                    .areaCode(properties.getString("QXDM"))
                    .basinName(properties.getString("所属流域"))
                    .riverName(properties.getString("所属河流"))
                    .sydType(properties.getString("水源地级别"))
                    .lat(coordinates.getBigDecimal(1))
                    .lon(coordinates.getBigDecimal(0))
                    .build();
            baseMapper.insert(source);
        });
    }
}
