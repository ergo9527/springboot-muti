package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DataAnalysisCategory;
import com.ergo.datacenter.entity.EmissionEntInfo;
import com.ergo.datacenter.mapper.EmissionEntInfoMapper;
import com.ergo.datacenter.service.EmissionEntInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-11-23
 */
@Service
public class EmissionEntInfoServiceImpl extends ServiceImpl<EmissionEntInfoMapper, EmissionEntInfo> implements EmissionEntInfoService {

    @Override
    public void add(JSONObject jsonObject) {
        //请求地址
        String url = "http://admin.hzmingrui.cn/admin/listMapDevice";

        //设置headerMap参数，对应httpHeader参数，用于签名计算
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Auth", "qazwsxeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFeHAiOjE2Mzc2NTc2NDUsIklEIjoiYnZoYWo2NTZxcDFxaWF2N28zMGciLCJJYXQiOjE2Mzc2Mzk2NDUsIkluZm8iOnsiYWRkcmVzcyI6IiIsImJsX25hbWUiOiIiLCJibF9ubyI6IiIsImJ1c2luZXNzX2hvdXIiOiIiLCJjb250YWN0IjoiIiwiY3JlYXRlX2F0IjoxNjA4NjkwMDU1LCJjcmVhdG9yIjoiIiwiZGVzYyI6IiIsImlkIjoxMTk5LCJsb2dvIjoiIiwibWFpbnRhaW5lcl9pZCI6IiIsIm1vYmlsZSI6IiIsIm5hbWUiOiLlm5vlt53nnIHlnKjnur_msrnng5_nm5HnrqEiLCJvcGVyYXRvcl9pZCI6bnVsbCwib3JnIjoi5Zub5bed55yB5Zyo57q_5rK554Of55uR566hIiwicGlkIjoiIiwic3RhdHVzIjowLCJ0ZWxlcGhvbmUiOiIiLCJ0eXAiOjN9LCJQZXJtaXQiOlsic3RhdHM6YWNoaWV2ZW1lbnQiLCJzdGF0czpyZWR1Y3Rpb24iLCJzeXM6Y3Vpc2luZSIsInN0YXRzOmJvYXJkIiwiIiwiIiwiIiwiIiwiIiwiIiwiIiwiYXV0aDpvcmciLCJjdXN0b21lcjpsaXN0IiwibWFpbnRlbjpyZWNvcmQiLCJtYXAiLCIiLCJjdXN0b21lcjpsb2NhbGUiLCJzdGF0czppbnRpbWUiLCJzdXBlcnZpc2lvbiIsIm1haW50ZW46ZmFpbHVyZSIsImRldmljZTpsaXN0Iiwic3RhdHMiLCJzdGF0czpkZXRlY3RvciIsImRhdGFNaW51dGUiLCJjdXN0b21lciIsIiIsIiIsIm1haW50ZW4iLCJtZXNzYWdlIiwic3lzOnNldCIsIiIsIiIsImF1dGgiLCIiXSwiUm9sZVNpZ24iOiJDaGFyZ2VyIiwiUm9sZVR5cGUiOiIzIiwiUm9sZVdlaWdodHMiOjR9.O1fSBoJYA-SYznLnB-tStgTRCLMKftxm7jA3Tc4Q_lI");

        //发送http请求
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        JSONObject response = restTemplate.postForObject(url, httpEntity, JSONObject.class);
        JSONArray devices = response.getJSONObject("Data").getJSONArray("Device");
        devices.stream().forEach(d -> {
            cn.hutool.json.JSONObject deviceJSON = JSONUtil.parseObj(d);
            JSONObject device = JSONObject.parseObject(deviceJSON.toString());
            EmissionEntInfo entInfo = EmissionEntInfo.builder()
                    .id(device.getString("Id"))
                    .mn(device.getString("MN"))
                    .address(device.getString("Addr"))
                    .name(device.getString("LocaleName"))
                    .cuisineName(device.getString("CuisineName"))
                    .lat(new BigDecimal(device.getString("Lat")))
                    .lon(new BigDecimal(device.getString("Lng")))
                    .build();
            baseMapper.insert(entInfo);
        });
    }
}
