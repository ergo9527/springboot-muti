package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RouteAssessRisk;
import com.ergo.datacenter.entity.RouteAssessRiskExclusion;
import com.ergo.datacenter.mapper.RouteAssessRiskExclusionMapper;
import com.ergo.datacenter.service.RouteAssessRiskExclusionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
@Service
public class RouteAssessRiskExclusionServiceImpl extends ServiceImpl<RouteAssessRiskExclusionMapper, RouteAssessRiskExclusion> implements RouteAssessRiskExclusionService {
    @Override
    public void addBatch(JSONObject jsonObject) {
        List<RouteAssessRiskExclusion> err = new ArrayList<>();
        //遍历features
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(f -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(f);
            JSONObject feature = JSONObject.parseObject(jsonObject1.toString());
            JSONObject properties = feature.getJSONObject("properties");

            RouteAssessRiskExclusion routeAssessRisk = RouteAssessRiskExclusion.builder()
                    .name(properties.getString("LDMC"))
                    .remark(properties.getString("REMARK"))
                    .routeLength(properties.getInteger("LDCD"))
                    .relativeRiver(properties.getString("GLHD"))
                    .shortestDistanceFromRiver(properties.getInteger("ZDLHJL"))
                    .latStart(properties.getBigDecimal("QDWD"))
                    .lonStart(properties.getBigDecimal("QDJD"))
                    .latEnd(properties.getBigDecimal("ZDWD"))
                    .lonEnd(properties.getBigDecimal("ZDJD"))
                    .shapeLength(properties.getBigDecimal("Shape_Length"))
                    .obj(feature.toJSONString())
                    .fid(properties.getLong("FID_评估路线_风险路段_排除路段"))
                    .build();
            try {
                baseMapper.insert(routeAssessRisk);
            } catch (Exception e) {
                e.printStackTrace();
                err.add(routeAssessRisk);
                return;
            }
        });
        System.out.println("插入失败条数: " + err.size());
    }
}
