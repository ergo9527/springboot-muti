package com.ergo.datacenter.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.CrossBorderSection;
import com.ergo.datacenter.entity.IndustryParkSewageFacilities;
import com.ergo.datacenter.mapper.IndustryParkSewageFacilitiesMapper;
import com.ergo.datacenter.service.IndustryParkSewageFacilitiesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Service
public class IndustryParkSewageFacilitiesServiceImpl extends ServiceImpl<IndustryParkSewageFacilitiesMapper, IndustryParkSewageFacilities> implements IndustryParkSewageFacilitiesService {

    @Override
    public void insertBatch(JSONObject jsonObject) {
        JSONArray features = jsonObject.getJSONArray("features");
        features.stream().forEach(a -> {
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) a;
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(map);
            JSONObject f = JSONObject.parseObject(jsonObject1.toString());

            JSONObject properties = f.getJSONObject("properties");
            JSONObject geometry = f.getJSONObject("geometry");
            JSONArray coordinates = geometry.getJSONArray("coordinates");
            IndustryParkSewageFacilities facilities = IndustryParkSewageFacilities.builder()
                    .parkName(properties.getString("F2"))
                    .parkCode(properties.getString("四川省工业园区污水处理设施情况表"))
                    .subParkName(properties.getString("F3"))
                    .parkType(properties.getString("F4"))
                    .facilityName(properties.getString("F5"))
                    .facilityType(properties.getString("F6"))
                    .lonFacility(properties.getString("F7"))
                    .latFacility(properties.getString("F8"))
                    .codDayAvg(properties.getDouble("F9"))
                    .ammoniaNitrogenDayAvg(properties.getDouble("F10"))
                    .sewageOutletName(properties.getString("F11"))
                    .lonOutlet(properties.getString("F12"))
                    .latOutlet(properties.getString("F13"))
                    .lat(coordinates.getBigDecimal(1))
                    .lon(coordinates.getBigDecimal(0))
                    .build();
            baseMapper.insert(facilities);
        });
    }
}
