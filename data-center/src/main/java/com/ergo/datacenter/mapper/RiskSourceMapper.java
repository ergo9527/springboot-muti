package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.RiskSource;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/7/30 16:26
 * @Description :
 */
@Mapper
@Repository
public interface RiskSourceMapper extends BaseMapper<RiskSource> {
}
