package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.TableAssetInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/7/8 9:24
 * @Description :
 */
@Mapper
@Repository
public interface TableAssetInfoMapper extends BaseMapper<TableAssetInfo> {
}
