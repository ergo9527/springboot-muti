package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.SlagTruck;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 16:12
 * @Description :
 */
public interface SlagTruckMapper extends BaseMapper<SlagTruck> {
}
