package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.DataAssetMetaVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
@Mapper
@Repository
public interface DataAssetMetaVersionMapper extends BaseMapper<DataAssetMetaVersion> {

}
