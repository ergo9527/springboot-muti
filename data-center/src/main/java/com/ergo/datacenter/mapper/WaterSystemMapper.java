package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.WaterSystem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
public interface WaterSystemMapper extends BaseMapper<WaterSystem> {

}
