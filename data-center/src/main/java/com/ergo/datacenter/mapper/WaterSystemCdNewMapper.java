package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.WaterSystemCdNew;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@Mapper
@Repository
public interface WaterSystemCdNewMapper extends BaseMapper<WaterSystemCdNew> {

}
