package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.TInveSewageoutlet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-09-09
 */
@Mapper
@Repository
public interface TInveSewageoutletMapper extends BaseMapper<TInveSewageoutlet> {

}
