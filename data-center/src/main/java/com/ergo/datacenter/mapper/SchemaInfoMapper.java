package com.ergo.datacenter.mapper;


import com.ergo.datacenter.entity.ColumnAssetInfo;
import com.ergo.datacenter.entity.TableAssetInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/6/26 11:04
 * @Description :
 */
@Mapper
@Repository
public interface SchemaInfoMapper {

    List<TableAssetInfo> findAllTable();

    @Select("SELECT table_schema as db_name, table_name, column_name, column_comment, data_type, column_comment as head_name FROM information_schema.COLUMNS WHERE table_schema = 'glzt-data-center-warehouse' and table_name = #{tableName}")
    List<ColumnAssetInfo> findTableColumns(String tableName);

    TableAssetInfo findTableSchemaByName(String tableName);

    @Select("SELECT s.column_name,s.COLUMN_COMMENT as column_comment FROM information_schema.Columns s,data_asset_tables t WHERE t.`code`=#{code} and t.table_name=s.TABLE_NAME")
    List<Map<String, Object>> findColumnCommentMap(String code);
}
