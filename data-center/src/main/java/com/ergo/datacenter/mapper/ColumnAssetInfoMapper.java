package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.ColumnAssetInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/7/8 9:25
 * @Description :
 */
@Mapper
@Repository
public interface ColumnAssetInfoMapper extends BaseMapper<ColumnAssetInfo> {
}
