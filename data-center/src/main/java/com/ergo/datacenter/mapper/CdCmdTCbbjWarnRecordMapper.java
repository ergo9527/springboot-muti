package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.CdCmdTCbbjWarnRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-10-21
 */
public interface CdCmdTCbbjWarnRecordMapper extends BaseMapper<CdCmdTCbbjWarnRecord> {

}
