package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.NoiseHourData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2022-03-11
 */
public interface NoiseHourDataMapper extends BaseMapper<NoiseHourData> {

}
