package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.RiverPolicyPoint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
@Mapper
public interface RiverPolicyPointMapper extends BaseMapper<RiverPolicyPoint> {

}
