package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.IndustryParkRange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
public interface IndustryParkRangeMapper extends BaseMapper<IndustryParkRange> {

}
