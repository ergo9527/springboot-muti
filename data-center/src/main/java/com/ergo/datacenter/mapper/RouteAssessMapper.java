package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.RouteAssess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-18
 */
public interface RouteAssessMapper extends BaseMapper<RouteAssess> {

}
