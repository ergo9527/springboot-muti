package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.ReserveCd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-26
 */
public interface ReserveCdMapper extends BaseMapper<ReserveCd> {

}
