package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.DrinkWaterSourceIntakeReserveCd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import lombok.Builder;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Mapper
@Repository
public interface DrinkWaterSourceIntakeReserveCdMapper extends BaseMapper<DrinkWaterSourceIntakeReserveCd> {

}
