package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.WasteSolid;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-16
 */
public interface WasteSolidMapper extends BaseMapper<WasteSolid> {

}
