package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.ColumnAssetExtInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/7/8 9:31
 * @Description :
 */
@Mapper
@Repository
public interface ColumnAssetExtMapper extends BaseMapper<ColumnAssetExtInfo> {
}
