package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.StApi;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-09-07
 */
public interface StApiMapper extends BaseMapper<StApi> {

}
