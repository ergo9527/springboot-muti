package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.KeyAreas;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
public interface KeyAreasMapper extends BaseMapper<KeyAreas> {

}
