package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.DataAssetTables;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2022-03-15
 */
@Mapper
@Repository
public interface DataAssetTablesMapper extends BaseMapper<DataAssetTables> {

}
