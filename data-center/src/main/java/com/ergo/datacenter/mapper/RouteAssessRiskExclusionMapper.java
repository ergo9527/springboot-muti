package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.RouteAssessRiskExclusion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
public interface RouteAssessRiskExclusionMapper extends BaseMapper<RouteAssessRiskExclusion> {

}
