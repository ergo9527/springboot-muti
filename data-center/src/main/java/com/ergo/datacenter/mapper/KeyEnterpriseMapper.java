package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.KeyEnterprise;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 9:26
 * @Description :
 */
@Mapper
@Repository
public interface KeyEnterpriseMapper extends BaseMapper<KeyEnterprise> {
}
