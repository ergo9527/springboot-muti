package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.OutletVideoInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-11-05
 */
@Mapper
@Repository
public interface OutletVideoInfoMapper extends BaseMapper<OutletVideoInfo> {

}
