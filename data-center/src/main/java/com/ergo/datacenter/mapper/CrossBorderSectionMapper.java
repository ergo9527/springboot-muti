package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.CrossBorderSection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-08-12
 */
public interface CrossBorderSectionMapper extends BaseMapper<CrossBorderSection> {

}
