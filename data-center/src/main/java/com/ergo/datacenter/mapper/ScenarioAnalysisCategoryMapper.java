package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.ScenarioAnalysisCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-10-08
 */
public interface ScenarioAnalysisCategoryMapper extends BaseMapper<ScenarioAnalysisCategory> {

}
