package com.ergo.datacenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.datacenter.entity.DataAnalysisCategory;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 14:57
 * @Description :
 */
@Mapper
@Repository
public interface DataAnalysisCategoryMapper extends BaseMapper<DataAnalysisCategory> {
}
