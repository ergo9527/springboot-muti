package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.IotPerceptionCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
public interface IotPerceptionCategoryMapper extends BaseMapper<IotPerceptionCategory> {

}
