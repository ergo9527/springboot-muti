package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.DataAssetShareRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2022-03-24
 */
public interface DataAssetShareRecordMapper extends BaseMapper<DataAssetShareRecord> {

}
