package com.ergo.datacenter.mapper;

import com.ergo.datacenter.entity.ScenarioDynamicMiddle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ergo
 * @since 2021-10-09
 */
public interface ScenarioDynamicMiddleMapper extends BaseMapper<ScenarioDynamicMiddle> {

}
