package com.ergo.datacenter.utils;

import com.alibaba.fastjson.JSONObject;

import java.io.*;

/**
 * @author liuxin
 * @date 2021/10/12 11:28
 * @description
 */
public class ReadJSONFiledUtils {

    //读取json文件
    //读取json文件
    public static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {

        String s = readJsonFile("C:\\Users\\ergo\\Desktop\\20210929\\成都市大型油库.json");
        System.out.println(s);
        JSONObject jsonObject = JSONObject.parseObject(s);
        System.out.println(jsonObject);

    }
}
