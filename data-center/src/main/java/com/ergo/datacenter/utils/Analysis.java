package com.ergo.datacenter.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.BusinessException;
import com.ergo.datacenter.result.MessageCode;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Analysis {

    public Analysis() {
        throw new Error("工具类不允许实例化！");
    }

    //固定表头对应英文名
    public final static Map fieldNameMap = new HashMap() {{
        put("污染源名称", "pollutionName");
        put("污染源编码", "sourceCode");
        put("污染源地址", "pollutionAddr");
        put("所属区县", "areaBelong");
        put("纬度", "lat");
        put("经度", "lon");
        put("环保分类", "envirType");
        put("站点名称（重点区域）", "stationName");
        put("施工状态", "status");
        put("状态", "status");
    }};

    /**
     * 解析Excel
     */
    public static List<JSONObject> analysisExcel(MultipartFile file) {

        //存放json记录的集合
        List<JSONObject> records = new ArrayList<>();

        //存放扩展字段JSON
        JSONObject extJsonObject = new JSONObject();

        try {

            //获取文件输入流
            InputStream in = file.getInputStream();

            //判断文件版本
            Workbook workbook = null;
            if (judgeExcelEdition(file.getOriginalFilename())) {
                workbook = new XSSFWorkbook(in);
            } else {
                workbook = new HSSFWorkbook(in);
            }

            //获取sheet
            Sheet sheet = workbook.getSheetAt(0);

            int tableHeadIndex = 0;

            row:
            for (int i = tableHeadIndex + 1; i < sheet.getPhysicalNumberOfRows(); i++) {     //循环行

                //从表头的下一行开始读取数据
                Row sheetRow = sheet.getRow(i);
                int rowCells = sheetRow.getPhysicalNumberOfCells();
                //循环获取某一行的每一列单元格
                JSONObject recordJSON = new JSONObject();
                column:
                for (int j = 0; j < rowCells; j++) {     //循环列
                    //获取列名
                    String fieldName = sheet.getRow(tableHeadIndex).getCell(j).toString();
                    //获取列的值
                    String fieldValue = sheetRow.getCell(j) + "";
//                    if (fieldName.equals("污染源名称") && StringUtils.isEmpty(fieldValue)) { //如果污染源名称为空，此行视为无效数据
//                        break row;  //跳出行循环
//                    }
//                    //获取列英文对照名
//                    Object o = fieldNameMap.get(fieldName);
//                    if (o == null) {     //如果没有找到，说明是非固定字段
//                        extJsonObject.put(fieldName, fieldValue);
//                    } else {
//                        recordJSON.put(o.toString(), fieldValue);
//                    }
                    recordJSON.put(fieldName, fieldValue);
                }
                //将一条recordJson添加至集合
//                recordJSON.put("ext", extJsonObject.toJSONString());
                records.add(recordJSON);
            }

            workbook.close();

        } catch (IOException e) {
            throw new BusinessException(MessageCode.FILE_INPUTSTREAM_ERROR);
        }
        return records;
    }

//    /**
//     * 解析Excel表格，返回记录list集合
//     *
//     * @param file 上传的文件
//     * @return 二维集合，代表所有记录。一行包含所有列
//     */
//    public static List<JSONObject> analysis(MultipartFile file) throws CustomerException {
//
//
//        //存放json记录的集合
//        List<JSONObject> records = new ArrayList<>();
//
//        try {
//
//            //获取文件输入流
//            InputStream in = file.getInputStream();
//
//            //判断文件版本
//            Workbook workbook = null;
//
//            if (judgeExcelEdition(file.getOriginalFilename())) {
//                workbook = new XSSFWorkbook(in);
//            } else {
//                workbook = new HSSFWorkbook(in);
//            }
//
//            //获取sheet名
//            Sheet sheet = workbook.getSheetAt(0);
//
//            int tableHeadIndex = 0;
//
//            for (int i = tableHeadIndex + 1; i < sheet.getPhysicalNumberOfRows(); i++) {     //.getPhysicalNumberOfRows(): 获取当前sheet行数
//
//                //从表头的下一行开始读取数据
//                Row sheetRow = sheet.getRow(i);
//                int rowCells = sheetRow.getPhysicalNumberOfCells();
//                //循环获取某一行的每一列单元格
//                JSONObject recordJSON = new JSONObject();
//                for (int j = 0; j < rowCells; j++) {     //.getPhysicalNumberOfCells(): 获取当前row列数
//                    recordJSON.put(sheet.getRow(tableHeadIndex).getCell(j).toString(), "" + sheetRow.getCell(j));
//                }
//                //将一条recordJson添加至集合
//                records.add(recordJSON);
//            }
//
//            workbook.close();
//
//        } catch (IOException e) {
//            throw new CustomerException(MessageCode.FILE_INPUTSTREAM_ERROR);
//        }
//        return records;
//
//    }


    /**
     * 分析Excel所有Sheet
     *
     * @param file
     * @return
     */
    public static JSONObject analysisAll(MultipartFile file) throws BusinessException {

        JSONObject excelJSON = new JSONObject();
        JSONArray sheetArray = new JSONArray();

        //设置文件名
        excelJSON.put("filename", file.getOriginalFilename());

        try {

            //获取文件输入流
            InputStream in = file.getInputStream();

            //判断文件版本
            Workbook workbook = null;

            if (judgeExcelEdition(file.getOriginalFilename())) {
                workbook = new XSSFWorkbook(in);
            } else {
                workbook = new HSSFWorkbook(in);
            }


            //获取sheet数量
            int sheetCounts = workbook.getNumberOfSheets();
            //遍历所有sheet
            for (int s = 0; s < sheetCounts; s++) {

                JSONObject sheetJSON = new JSONObject();
                JSONArray dataArray = new JSONArray();
                //获取sheet名
                Sheet sheet = workbook.getSheetAt(s);
                sheetJSON.put("sheetname", sheet.getSheetName());

                //该sheet为空,跳往下一条循环
                if (sheet.getFirstRowNum() < 0) {
                    continue;
                }

                int tableHeadIndex;

                //判断当前sheet的第 0行0列是否为合并单元格
                if (isMergedRegion(sheet, 0, 0)) {
                    tableHeadIndex = 1;
                } else {
                    tableHeadIndex = 0;
                }


                for (int i = tableHeadIndex + 1; i < sheet.getPhysicalNumberOfRows(); i++) {     //.getPhysicalNumberOfRows(): 获取当前sheet行数

                    //从表头的下一行开始读取数据
                    Row sheetRow = sheet.getRow(i);
                    int rowCells = sheetRow.getPhysicalNumberOfCells();
                    //循环获取某一行的每一列单元格
                    JSONObject recordJSON = new JSONObject();
                    for (int j = 0; j < rowCells; j++) {     //.getPhysicalNumberOfCells(): 获取当前row列数
                        recordJSON.put(sheet.getRow(tableHeadIndex).getCell(j).toString(), "" + sheetRow.getCell(j));
                    }
                    //将一条record记录添加至一张sheet的dataArray
                    dataArray.add(recordJSON);
                }
                sheetJSON.put("datas", dataArray);
                //将sheetJSON添加至sheetArray
                sheetArray.add(sheetJSON);
                workbook.close();
            }
            excelJSON.put("sheets", sheetArray);
        } catch (IOException e) {
            throw new BusinessException(MessageCode.FILE_INPUTSTREAM_ERROR);
        }
        return excelJSON;
    }

    /**
     * 判断上传的excel文件版本（xls为2003，xlsx为2017）
     *
     * @param fileName 文件路径
     * @return excel 2007及以上版本返回true，excel 2007以下版本返回false
     */
    private static boolean judgeExcelEdition(String fileName) {
        if (fileName.matches("^.+\\.(?i)(xls)$")) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * 判断指定的单元格是否是合并单元格
     *
     * @param sheet
     * @param row    行下标
     * @param column 列下标
     * @return
     */
    private static boolean isMergedRegion(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 解析CSV文件
     *
     * @param file
     * @return List<JSONObject>
     */
    public static List<JSONObject> analysisCSV(File file) {

        try {

            List<JSONObject> records = new ArrayList<>();
            JSONObject record = new JSONObject();

            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
            String line = null;
            String[] items = null;

            int tableHeadIndex = 1;
            String[] header = null;
            while ((line = reader.readLine()) != null) {

                //第一行作为表头
                if (tableHeadIndex == 1) {
                    header = line.split(",");
                    System.out.println(header.length);
                    tableHeadIndex += 1;

                } else {     //其他行为数据行

                    String[] columns = line.split(",");
                    System.out.println(columns.length);

                    //构造一条记录
                    for (int i = 0; i < columns.length; i++) {

                        record.put(header[i], columns[i]);
                    }
                    //放入List集合
                    records.add(record);
                }
            }
            return records;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
