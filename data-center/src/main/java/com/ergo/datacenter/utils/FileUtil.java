package com.ergo.datacenter.utils;

import com.ergo.datacenter.result.BusinessException;
import com.ergo.datacenter.result.MessageCode;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FileUtil {

    /**
     * MultipartFile 转 File
     *
     * @param file
     * @throws Exception
     */
    public static File multipartFileToFile(MultipartFile file) throws BusinessException {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            try {
                ins = file.getInputStream();
                toFile = new File(file.getOriginalFilename());
                inputStreamToFile(ins, toFile);
                ins.close();
            }catch (IOException e){
                e.printStackTrace();
                throw new BusinessException(MessageCode.FILE_INPUTSTREAM_ERROR);
            }
        }
        return toFile;
    }

    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            int length = 4 *1024 *1024;
            byte[] buffer = new byte[length];
            while ((bytesRead = ins.read(buffer, 0, length)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除本地临时文件
     *
     * @param file
     */
    public static void delteTempFile(File file) {
        if (file != null) {
            File del = new File(file.toURI());
            del.delete();
        }
    }
}
