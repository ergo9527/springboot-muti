package com.ergo.datacenter.utils;

import java.util.*;

/**
 * @Description List集合中的Map根据多属性进行去重工具类
 * @Author Lesion
 * @DateTime 2021-08-17 17:00
 * @Version V1.0.0
 */
public class MapInListDistinctByMultiKeyUtils {

    /**
     * 该方法可根据指定字段对List<Map>中的数据去重
     *
     * @param originMapList 源Map数组
     * @param keys          依据的去重字段
     * @return
     */
    public static List<Map<String, Object>> deleteDuplicatedMapFromListByKeys(List<Map<String, Object>> originMapList, List keys) {
        Map tempMap = new HashMap();
        for (Map originMap : originMapList) {
            String objHashCode = "";
            for (Object key : keys) {
                String value = originMap.get(key) != null ? originMap.get(key).toString() : "";
                objHashCode += value.hashCode();
            }
            tempMap.put(objHashCode, originMap);
        }
        List valueList = new ArrayList<>(tempMap.values());
        return valueList;
    }

    /**
     * Demo
     *
     * @param args
     */
    public static void main(String[] args) {

        List<Map<String, Object>> list = new ArrayList<>();

        Map<String, Object> m1 = new HashMap<>();
        Map<String, Object> m2 = new HashMap<>();
        Map<String, Object> m3 = new HashMap<>();

        m1.put("id", 1L);
        m1.put("lat", 30.123456);
        m1.put("lon", 107.123456);

        m2.put("id", 2L);
        m2.put("lat", 30.123456);
        m2.put("lon", 107.123456);

        m3.put("id", 3L);
        m3.put("lat", 29.123456);
        m3.put("lon", 106.123456);

        list.add(m1);
        list.add(m2);
        list.add(m3);

        //添加要参与去重的属性至List
        List<String> keyList = new ArrayList<>();
        keyList.add("lat");
        keyList.add("lon");

        //去重
        List result = MapInListDistinctByMultiKeyUtils.deleteDuplicatedMapFromListByKeys(list, keyList);

        System.out.println("原始集合:");
        list.forEach(System.out::println);
        System.out.println("去重后的集合:");
        result.forEach(System.out::println);
    }
}