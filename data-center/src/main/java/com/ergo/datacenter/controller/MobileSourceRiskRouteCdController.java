package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.service.MobileSourceRiskRouteCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
@RestController
@ResponseBody
@RequestMapping("/mobile-source-risk-route-cd")
public class MobileSourceRiskRouteCdController {

    @Autowired
    private MobileSourceRiskRouteCdService service;

    @PostMapping("/add")
    public void add(@RequestBody JSONObject jsonObject) {
        service.add(jsonObject);
    }
}

