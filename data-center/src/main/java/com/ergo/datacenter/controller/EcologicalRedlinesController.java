package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.EcologicalRedlinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@RestController
@RequestMapping("/redline")
@ResponseBody
public class EcologicalRedlinesController {

    @Autowired
    private EcologicalRedlinesService redlinesService;

    @PostMapping("/addBatch")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        redlinesService.addBatch(jsonObject);
        return ResultMsg.ok("done");
    }
}

