package com.ergo.datacenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.Result;
import com.ergo.datacenter.service.SlagTruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 16:06
 * @Description :
 */
@ResponseBody
@RestController
@RequestMapping("/slagTruck")
public class SlagTruckController {

    @Autowired
    private SlagTruckService slagTruckService;

    @PostMapping("/add")
    public Result syncDataAsset(@RequestBody JSONObject jsonObject) {
        slagTruckService.add(jsonObject);
        return Result.ok();
    }
}
