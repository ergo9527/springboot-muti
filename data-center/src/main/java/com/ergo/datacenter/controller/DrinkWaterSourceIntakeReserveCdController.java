package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.DrinkWaterSourceIntakeReserveCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@RestController
@RequestMapping("/drink-water-source-intake-reserve-cd")
public class DrinkWaterSourceIntakeReserveCdController {

    @Autowired
    private DrinkWaterSourceIntakeReserveCdService sourceIntakeReserveCdService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        sourceIntakeReserveCdService.add(jsonObject);
        return ResultMsg.ok();
    }

}

