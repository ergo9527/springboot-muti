package com.ergo.datacenter.controller;


import com.ergo.datacenter.service.SkynetCameraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-12
 */
@RestController
@RequestMapping("/skynet-camera")
public class SkynetCameraController {

    @Autowired
    private SkynetCameraService skynetCameraService;

    @GetMapping
    void add(@RequestParam String sessionId) {
        skynetCameraService.addCamera(sessionId);
    }

}

