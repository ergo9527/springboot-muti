package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.RiverPolicyPointService;
import com.ergo.datacenter.service.RiverPolicyWaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@RestController
@ResponseBody
@RequestMapping("/river-policy-water")
public class RiverPolicyWaterController {

    @Autowired
    private RiverPolicyWaterService riverPolicyWaterService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        riverPolicyWaterService.addBatch(jsonObject);
        return ResultMsg.ok();
    }
}

