package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.KeyAreasService;
import com.ergo.datacenter.service.WasteSolidService;
import com.ergo.datacenter.service.impl.WasteSolidServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-16
 */
@RestController
@ResponseBody
@RequestMapping("/waste-solid")
public class WasteSolidController {

    @Autowired
    private WasteSolidService wasteSolidService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        wasteSolidService.addBatch(jsonObject);
        return ResultMsg.ok("done");
    }

    @GetMapping("/addConfig")
    public ResultMsg addConfig() {
        wasteSolidService.addConfig();
        return ResultMsg.ok("done");
    }

    @GetMapping("/changeObj")
    public ResultMsg changeObj() {
        wasteSolidService.changePolygonToLine();
        return ResultMsg.ok("done");
    }
}

