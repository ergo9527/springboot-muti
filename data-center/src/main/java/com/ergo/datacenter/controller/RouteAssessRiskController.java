package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.RouteAssessRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
@RestController
@ResponseBody
@RequestMapping("/route-assess-risk")
public class RouteAssessRiskController {

    @Autowired
    private RouteAssessRiskService riskService;

    @PostMapping("/addBatch")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        riskService.addBatch(jsonObject);
        return ResultMsg.ok();
    }

}

