package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DrinkingWaterSourceReserveCd;
import com.ergo.datacenter.result.Result;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.DrinkingWaterSourceReserveCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@RestController
@RequestMapping("/drinking-water-source-reserve-cd")
public class DrinkingWaterSourceReserveCdController {

    @Autowired
    private DrinkingWaterSourceReserveCdService sourceReserveCdService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        sourceReserveCdService.add(jsonObject);
        return ResultMsg.ok();
    }
}

