package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.service.DrinkWaterSourceCentralizedTownshipCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
@RestController
@RequestMapping("/drink-water-source-centralized-township-cd")
public class DrinkWaterSourceCentralizedTownshipCdController {

    @Autowired
    private DrinkWaterSourceCentralizedTownshipCdService service;

    @PostMapping("/add")
    public void add(@RequestBody JSONObject jsonObject) {
        service.add(jsonObject);
    }
}

