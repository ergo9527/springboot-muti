package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.EmissionEntInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-23
 */
@RestController
@ResponseBody
@RequestMapping("/emission-ent-info")
public class EmissionEntInfoController {

    @Autowired
    private EmissionEntInfoService emissionEntInfoService;

    @PostMapping
    public ResultMsg add(@RequestBody JSONObject data) {
        emissionEntInfoService.add(data);
        return ResultMsg.ok("done");
    }

}

