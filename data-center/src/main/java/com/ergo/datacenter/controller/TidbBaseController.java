package com.ergo.datacenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ergo.datacenter.result.Result;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.TidbBaseService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Author : liuxin
 * @Date: 2021/6/15 17:30
 * @Description :
 */
@ResponseBody
@RestController
@RequestMapping("/dataCenter/tidb")
public class TidbBaseController {

    @Autowired
    TidbBaseService tidbBaseService;

    /**
     * 返回分页
     *
     * @param page
     * @param sql
     * @return
     */
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "size", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sql", value = "sql", dataType = "String", paramType = "query"),
    })
    @GetMapping("/page")
    public Result page(@ApiIgnore Page page, @RequestParam("sql") String sql) {

        return Result.ok(JSONObject.parseArray(tidbBaseService.findByPage(page, sql)));
    }

    /**
     * 返回List
     *
     * @param sql
     * @return
     */
    @ApiOperation("返回List")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sql", value = "sql", dataType = "String", paramType = "query"),
    })
    @GetMapping("/list")
    public ResultMsg findList(@RequestParam("sql") String sql) {
//        return ResultMsg.ok(JSONObject.parseArray(tidbBaseService.findList(sql)));
        return tidbBaseService.findList(sql);
    }

    /**
     * 返回Map
     *
     * @param sql
     * @return
     */
    @ApiOperation("返回Map")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sql", value = "sql", dataType = "String", paramType = "query"),
    })
    @GetMapping("/map")
    public Result findMap(@RequestParam("sql") String sql) {
        return Result.ok(JSONObject.parseObject(tidbBaseService.findMap(sql)));
    }
}
