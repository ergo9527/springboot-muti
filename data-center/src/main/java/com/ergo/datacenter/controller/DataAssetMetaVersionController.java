package com.ergo.datacenter.controller;


import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.DataAssetMetaVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
@RestController
@RequestMapping("/assetMeta")
public class DataAssetMetaVersionController {

    @Autowired
    private DataAssetMetaVersionService metaVersionService;

    @GetMapping("/syncVersion")
    public ResultMsg syncAssetMetaVersion() {
        return ResultMsg.ok("");
    }
}

