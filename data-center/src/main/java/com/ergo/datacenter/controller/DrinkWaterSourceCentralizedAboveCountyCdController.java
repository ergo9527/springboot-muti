package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.DrinkWaterSourceCentralizedAboveCountyCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-10-11
 */
@RestController
@ResponseBody
@RequestMapping("/drink-water-source-centralized-above-county-cd")
public class DrinkWaterSourceCentralizedAboveCountyCdController {

    @Autowired
    private DrinkWaterSourceCentralizedAboveCountyCdService service;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        service.add(jsonObject);
        return ResultMsg.ok();
    }
}

