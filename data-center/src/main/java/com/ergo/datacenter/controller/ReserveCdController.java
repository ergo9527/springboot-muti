package com.ergo.datacenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.ReserveCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-26
 */
@RestController
@ResponseBody
@RequestMapping("/reserve")
public class ReserveCdController {

    @Autowired
    private ReserveCdService reserveCdService;

    @PostMapping("/addBatch")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        reserveCdService.addBatch(jsonObject);
        return ResultMsg.ok();
    }
}

