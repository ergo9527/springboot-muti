package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.KeyAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@RestController
@ResponseBody
@RequestMapping("/keyAreas")
public class KeyAreasController {

    @Autowired
    private KeyAreasService keyAreasService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        keyAreasService.add(jsonObject);
        return ResultMsg.ok("done");
    }
}

