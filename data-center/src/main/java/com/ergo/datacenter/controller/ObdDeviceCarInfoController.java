package com.ergo.datacenter.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-19
 */
@RestController
@RequestMapping("/obd-device-car-info")
public class ObdDeviceCarInfoController {

}

