package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RiverPolicyPoint;
import com.ergo.datacenter.service.RiverPolicyPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
@RestController
@ResponseBody
@RequestMapping("/river-policy-point")
public class RiverPolicyPointController {

    @Autowired
    private RiverPolicyPointService riverPolicyPointService;

    @PostMapping("/add")
    public void add(@RequestBody JSONObject jsonObject) {
        riverPolicyPointService.add(jsonObject);
    }

}

