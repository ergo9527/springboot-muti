package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.WaterSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@RestController
@ResponseBody
@RequestMapping("/waterSystem")
public class WaterSystemController {

    @Autowired
    private WaterSystemService waterSystemService;

    @PostMapping("/add")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        waterSystemService.addBatch(jsonObject);
        return ResultMsg.ok();
    }


}

