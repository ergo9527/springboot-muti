package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.RouteAssessRiskExclusionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
@RestController
@ResponseBody
@RequestMapping("/route-assess-risk-exclusion")
public class RouteAssessRiskExclusionController {

    @Autowired
    private RouteAssessRiskExclusionService exclusionService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        exclusionService.addBatch(jsonObject);
        return ResultMsg.ok();
    }

}

