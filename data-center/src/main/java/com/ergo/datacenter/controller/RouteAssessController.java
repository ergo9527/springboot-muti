package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.RouteAssessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-18
 */
@RestController
@ResponseBody
@RequestMapping("/routeAssess")
public class RouteAssessController {

    @Autowired
    private RouteAssessService routeAssessService;

    @PostMapping("/addBatch")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        routeAssessService.addBatch(jsonObject);
        return ResultMsg.ok("done");
    }

}

