package com.ergo.datacenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.KeyEnterpriseService;
import com.ergo.datacenter.utils.Analysis;
import com.ergo.datacenter.utils.FileUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 9:25
 * @Description :
 */
@ResponseBody
@RestController
@RequestMapping("/keyCompany")
public class KeyEnterpriseController {

    @Autowired
    private KeyEnterpriseService keyEnterpriseService;

    @ApiOperation("Excel上传、解析、保存数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "污染源Excel文件", dataType = "file")
    })
    @PostMapping(value = "/upload/excel")
    @ResponseBody
    public ResultMsg uploadCSV(MultipartFile file) {

        //解析Excel表格
        List<JSONObject> list = Analysis.analysisExcel(file);
        keyEnterpriseService.analysis(list);
        return ResultMsg.ok();
    }
}
