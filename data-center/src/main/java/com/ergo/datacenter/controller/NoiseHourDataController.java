package com.ergo.datacenter.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ergo
 * @since 2022-03-11
 */
@RestController
@RequestMapping("/noise-hour-data")
public class NoiseHourDataController {

}

