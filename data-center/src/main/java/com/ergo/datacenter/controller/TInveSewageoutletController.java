package com.ergo.datacenter.controller;


import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.TInveSewageoutletService;
import org.apache.xmlbeans.impl.xb.ltgfmt.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-09-09
 */
@RestController
@ResponseBody
@RequestMapping("/sewageOutlet")
public class TInveSewageoutletController {

    @Autowired
    private TInveSewageoutletService sewageOutletService;

    @GetMapping("/dataClean")
    public ResultMsg cleanData() {
        sewageOutletService.CleanData();
        return ResultMsg.ok();
    }

    @PostMapping("/getAll/{code}")
    public ResultMsg getAll(@PathVariable("code") String code, Map<String,Object> data) {

        return ResultMsg.ok(sewageOutletService.findAll());
    }

}

