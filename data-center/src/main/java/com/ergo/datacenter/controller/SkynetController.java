package com.ergo.datacenter.controller;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.MapConfigPolygon;
import com.ergo.datacenter.result.ResultMsg;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/8/30 14:38
 * @Description :
 */
@RestController
@ResponseBody
@RequestMapping("/skynet")
public class SkynetController {

    @PostMapping("/getIds")
    public ResultMsg getIds(@RequestBody JSONObject jsonObject) {
        JSONArray cdArray = jsonObject.getJSONArray("childOrgan");
        List<String> ids = new ArrayList<>();
        Map<String, String> idMap = new HashMap<>();
        cdArray.stream().forEach(j -> {
            cn.hutool.json.JSONObject hj = JSONUtil.parseObj(j);
            JSONObject countyJSON = JSONObject.parseObject(hj.toString());
            JSONArray stationArray = countyJSON.getJSONArray("childOrgan");
            stationArray.stream().forEach(s -> {
                cn.hutool.json.JSONObject hj2 = JSONUtil.parseObj(s);
                JSONObject stationJSON = JSONObject.parseObject(hj2.toString());
                ids.add(stationJSON.getString("id"));
                idMap.put(stationJSON.getString("name"), stationJSON.getString("id"));
            });
        });
        idMap.keySet().stream().forEach(k -> {
            System.out.println(k + " : " + idMap.get(k));
        });
//        ids.stream().forEach(i->{
//            System.out.println(i);
//        });
        return ResultMsg.ok();

    }
}
