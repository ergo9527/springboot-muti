package com.ergo.datacenter.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-10-21
 */
@RestController
@RequestMapping("/cd-cmd-tcbbj-warn-record")
public class CdCmdTCbbjWarnRecordController {

}

