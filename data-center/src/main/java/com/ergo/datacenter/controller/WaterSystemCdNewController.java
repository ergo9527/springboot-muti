package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.service.WaterSystemCdNewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@RestController
@ResponseBody
@RequestMapping("/water-system-cd-new")
public class WaterSystemCdNewController {

    @Autowired
    private WaterSystemCdNewService waterSystemCdNewService;

    @PostMapping("/add")
    public void add(@RequestBody JSONObject jsonObject) {
        waterSystemCdNewService.add(jsonObject);
    }

}

