package com.ergo.datacenter.controller;


import com.ergo.datacenter.result.ResultMsg;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2022-03-15
 */
@ResponseBody
@RestController
@RequestMapping("/asset")
public class DataAssetTablesController {

}

