package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.IndustryParkRangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@RestController
@ResponseBody
@RequestMapping("/industryParkRange")
public class IndustryParkRangeController {

    @Autowired
    private IndustryParkRangeService rangeService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        rangeService.add(jsonObject);
        return ResultMsg.ok();
    }
}

