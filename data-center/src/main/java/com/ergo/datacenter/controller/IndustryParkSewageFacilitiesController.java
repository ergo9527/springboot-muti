package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.IndustryParkSewageFacilitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@RestController
@ResponseBody
@RequestMapping("/sewageFacilities")
public class IndustryParkSewageFacilitiesController {

    @Autowired
    private IndustryParkSewageFacilitiesService facilitiesService;

    @PostMapping("/addBatch")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        facilitiesService.insertBatch(jsonObject);
        return ResultMsg.ok("done");
    }
}

