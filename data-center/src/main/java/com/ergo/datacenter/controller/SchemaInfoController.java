package com.ergo.datacenter.controller;

import com.ergo.datacenter.result.Result;
import com.ergo.datacenter.service.SchemaInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author : liuxin
 * @Date: 2021/6/26 10:20
 * @Description :
 */
@ResponseBody
@RestController
@RequestMapping("/schemaInfo")
public class SchemaInfoController {

    @Autowired
    private SchemaInfoService schemaInfoService;

    @ApiOperation("同步数据中台库数据资产")
    @GetMapping("/dataAsset/syncByNameList")
    public Result syncDataAsset(@RequestParam String code) {
        schemaInfoService.syncDataCenterAsset(code);
        return Result.ok();
    }
}
