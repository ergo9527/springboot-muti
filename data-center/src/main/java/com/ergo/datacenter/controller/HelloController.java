package com.ergo.datacenter.controller;

import com.ergo.datacenter.result.MessageCode;
import com.ergo.datacenter.result.ResultMsg;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/9/2 15:42
 * @Description :
 */
@RestController
@RequestMapping("/hello")
@ResponseBody
public class HelloController {

    @PostMapping("/sayHello")
    public ResultMsg sayHello(@RequestBody Map<String, Object> data) {
        System.out.println("hello");

        if (data.get("name") == null || data.get("dateString") == null) {
            return ResultMsg.fail(MessageCode.REQUIRED_PARAMETERS_MISSING);
        } else {

            String name = data.get("name").toString();
            String dateString = data.get("dateString").toString();
            return ResultMsg.ok("hello," + name + " ---" + dateString);
        }
    }
}
