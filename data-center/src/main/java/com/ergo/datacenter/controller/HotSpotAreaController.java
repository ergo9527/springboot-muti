package com.ergo.datacenter.controller;


import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.service.HotSpotAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@RestController
@ResponseBody
@RequestMapping("/hot-spot-area")
public class HotSpotAreaController {

    @Autowired
    private HotSpotAreaService hotSpotAreaService;

    @PostMapping("/add")
    public void add(@RequestBody JSONObject data) {
        hotSpotAreaService.add(data);
    }
}

