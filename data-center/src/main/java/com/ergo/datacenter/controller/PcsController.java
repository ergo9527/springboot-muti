package com.ergo.datacenter.controller;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.DataAnalysisCategory;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.DataAnalysisCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author : liuxin
 * @Date: 2021/11/12 11:34
 * @Description :
 */
@ResponseBody
@RestController
@RequestMapping("/pcs")
public class PcsController {

    @Autowired
    private DataAnalysisCategoryService catalogsService;

    @GetMapping
    public ResultMsg addPCS() {
        //请求地址
        String url = "http://10.1.231.87:8080/mobile/organList";
        //请求参数
        JSONObject map = new JSONObject();
        map.put("sessionId", "0000000000200000000000004484773");

        //设置headerMap参数，对应httpHeader参数，用于签名计算
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //发送http请求
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(map.toJSONString(), headers);
        JSONObject response = restTemplate.postForObject(url, httpEntity, JSONObject.class);
        JSONObject data = response.getJSONObject("data");
        System.out.println(data);
        JSONArray organs = data.getJSONArray("childOrgan");
        System.out.println(organs);
        JSONObject SkyNet = organs.getJSONObject(2);
        JSONArray skyNetChilds = SkyNet.getJSONArray("childOrgan");
        JSONObject cd = skyNetChilds.getJSONObject(0);
        JSONArray cdChilds = cd.getJSONArray("childOrgan");
        cdChilds.stream().forEach(c -> {
            cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(c);
            JSONObject county = JSONObject.parseObject(jsonObject1.toString());
            //新增data_analysis_category
            DataAnalysisCategory category = new DataAnalysisCategory();
            category.setNodeName(county.getString("name"));
            category.setParentId("1456185723257974789");
            category.setCode("");
            category.setStatus(1);
            catalogsService.addOne(category);

            JSONArray countyChilds = county.getJSONArray("childOrgan");
            countyChilds.stream().forEach(p -> {
                cn.hutool.json.JSONObject jsonObject2 = JSONUtil.parseObj(p);
                JSONObject pcs = JSONObject.parseObject(jsonObject2.toString());
                //新增data_analysis_category
                DataAnalysisCategory category2 = new DataAnalysisCategory();
                category2.setNodeName(pcs.getString("name"));
                category2.setParentId(category.getId());
                category2.setCode("");
                category2.setStatus(1);
                catalogsService.addOne(category2);
            });


        });
        return ResultMsg.ok("done");
    }
}
