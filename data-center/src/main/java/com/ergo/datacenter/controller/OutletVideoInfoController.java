package com.ergo.datacenter.controller;


import com.ergo.datacenter.entity.OutletVideoInfo;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.OutletVideoInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-11-05
 */
@ResponseBody
@RestController
@RequestMapping("/outlet-video-info")
public class OutletVideoInfoController {

    @Autowired
    private OutletVideoInfoService outletVideoInfoService;

    @GetMapping("/")
    public ResultMsg add() {
        outletVideoInfoService.add();
        return ResultMsg.ok("ok");
    }

}

