package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.CrossBorderSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-12
 */

@ResponseBody
@RestController
@RequestMapping("/section")
public class CrossBorderSectionController {

    @Autowired
    private CrossBorderSectionService borderSectionService;

    @PostMapping("/add")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        borderSectionService.insertBatch(jsonObject);
        return ResultMsg.ok("done");
    }
}

