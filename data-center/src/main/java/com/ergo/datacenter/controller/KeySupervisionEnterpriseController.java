package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.KeySupervisionEnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-09-27
 */
@RestController
@RequestMapping("/key-supervision-enterprise")
public class KeySupervisionEnterpriseController {

    @Autowired
    private KeySupervisionEnterpriseService keyService;

    @PostMapping("/add")
    public ResultMsg add(@RequestBody JSONObject jsonObject) {
        keyService.addBatch(jsonObject);
        return ResultMsg.ok();
    }
}

