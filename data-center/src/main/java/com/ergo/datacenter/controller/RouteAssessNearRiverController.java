package com.ergo.datacenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.RouteAssessNearRiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-18
 */
@RestController
@ResponseBody
@RequestMapping("/routeAssess/nearRiver")
public class RouteAssessNearRiverController {

    @Autowired
    private RouteAssessNearRiverService riverService;

    @PostMapping("/add")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        riverService.addBatch(jsonObject);
        return ResultMsg.ok();
    }
}

