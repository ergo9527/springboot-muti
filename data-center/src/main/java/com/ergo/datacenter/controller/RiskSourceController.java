package com.ergo.datacenter.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.entity.RiskSource;
import com.ergo.datacenter.result.Result;
import com.ergo.datacenter.service.RiskSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author : liuxin
 * @Date: 2021/7/30 16:23
 * @Description :
 */
@ResponseBody
@RestController
@RequestMapping("/riskSource")
public class RiskSourceController {

    @Autowired
    private RiskSourceService riskSourceService;

    @PostMapping("/add")
    public Result addBatch(@RequestBody JSONObject jsonObject) {
        riskSourceService.insert(jsonObject);
        return Result.ok("ok");
    }
}
