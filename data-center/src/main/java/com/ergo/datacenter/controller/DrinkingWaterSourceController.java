package com.ergo.datacenter.controller;


import com.alibaba.fastjson.JSONObject;
import com.ergo.datacenter.result.ResultMsg;
import com.ergo.datacenter.service.DrinkingWaterSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@RestController
@ResponseBody
@RequestMapping("/waterSource")
public class DrinkingWaterSourceController {

    @Autowired
    private DrinkingWaterSourceService sourceService;

    @PostMapping("/addBatch")
    public ResultMsg addBatch(@RequestBody JSONObject jsonObject) {
        sourceService.insertBatch(jsonObject);
        return ResultMsg.ok("done");
    }

}

