package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class WaterSystemCdNew implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 水系名称
     */
    private String name;

    private String layer;

    /**
     * 几何学数据
     */
    private String obj;


}
