package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author : liuxin
 * @Date: 2021/7/6 16:38
 * @Description :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("data_asset_columns_ext")
public class ColumnAssetExtInfo {

    @TableId
    private String columnId;

    private Integer weight;

    private Boolean isDisplay;

}
