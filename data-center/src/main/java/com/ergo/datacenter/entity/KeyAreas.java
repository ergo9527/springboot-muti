package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class KeyAreas implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 区域名字
     */
    private String name;

    /**
     * 面域坐标数据
     */
    private String polygonObj;

    /**
     * 状态，1：有效，0：无效
     */
    private String status;


}
