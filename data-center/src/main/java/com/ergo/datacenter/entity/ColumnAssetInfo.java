package com.ergo.datacenter.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author : liuxin
 * @Date: 2021/7/6 16:31
 * @Description :
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("data_asset_columns")
public class ColumnAssetInfo {

    @TableId(type = IdType.AUTO)
    private Long id;

    @JSONField(name = "key")
    private String columnName;

    private String tableName;

    @JsonProperty("column_comment")
    @JSONField(name = "column_comment")
    private String columnComment;

    private String dataType;

    private String tableId;

    private String headName;

    private int weight;

    private Boolean isDisplay;

    private Date createdAt;
}
