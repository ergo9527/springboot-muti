package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.sql.Blob;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class DrinkingWaterSourceReserveCd implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 水源地名称
     */
    private String name;

    /**
     * 保护区级别
     */
    private String level;

    /**
     * 备注
     */
    private String remark;

    /**
     * 面积
     */
    private BigDecimal shapeArea;

    /**
     * 长度
     */
    private BigDecimal shapeLength;

    /**
     * 几何坐标
     */
    private String obj;


}
