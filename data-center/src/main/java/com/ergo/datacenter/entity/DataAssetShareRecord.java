package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2022-03-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataAssetShareRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 申请单位
     */
    private String applyOrgan;

    /**
     * 申请系统
     */
    private String applySys;

    /**
     * 共享方式
     */
    private String shareType;

    /**
     * 备注
     */
    private String comment;

    /**
     * 数据资产code
     */
    private String dataAssetCode;

    /**
     * 申请时间
     */
    private LocalDateTime applyAt;


}
