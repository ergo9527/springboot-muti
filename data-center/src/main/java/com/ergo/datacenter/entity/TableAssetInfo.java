package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author : liuxin
 * @Date: 2021/7/6 16:20
 * @Description : data_asset_tables
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("data_asset_tables")
public class TableAssetInfo {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @TableField("database_name")
    private String databaseName;

    @TableField("table_name")
    private String tableName;

    @TableField("table_comment")
    private String tableComment;

    private String source;

    private String code;

    private String background;

    private String dataRange;

    private String dataParam;

    private String dataFrequency;

    private String publicType;

    private String createAt;

    private String updateAt;

    private String dataFormat;

    private String comment;

    private Long martCategoryId;

    private String releaseStatus;

    @TableField(exist = false)
    private Long totalSize;

}
