package com.ergo.datacenter.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-09-27
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class KeySupervisionEnterprise implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 序号
     */
    private Long index;

    /**
     * 企业名称
     */
    private String entName;

    /**
     * 所属区县
     */
    private String areaBelong;

    /**
     * 经度
     */
    private BigDecimal lon;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 危险品类型
     */
    private String dangerGoodsType;

    /**
     * 关联河流
     */
    private String relativeRiver;

    /**
     * 关联环境风险受体
     */
    private String relativeEnvirRiskAcceptor;

    /**
     * 主要环境风险类型
     */
    private String majorEnvirRiskType;

    /**
     * 流域水环境风险等级
     */
    private String waterRiskLevel;

    /**
     * 预案评估等级
     */
    private String reservePlanAssessLevel;

    /**
     * Q值
     */
    private Double valueQ;

    /**
     * 备注
     */
    private String remark;


}
