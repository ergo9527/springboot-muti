package com.ergo.datacenter.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TInveSewageoutlet implements Serializable {

    private static final long serialVersionUID = 1L;

    private String fSewageoutletid;

    private Integer fDatasources;

    private String fInpsewageoutletname;

    private String fInpsewageoutletcode;

    private String fSewageoutletsource;

    @TableField("F_SiteCode")
    private String fSitecode;

    private String fSite;

    private String fScreeningaddress;

    private String fInplongitude;

    private String fInplatitude;

    private Integer fState;

    private String fSewageoutletname;

    private String fSewageoutletcode;

    private Integer fDatastate;

    private String fAddress;

    private String fLongitude;

    private String fLatitude;

    private String fWgslongitude;

    private String fWgslatitude;

    private String fInvesriverid;

    private String fRiverid;

    private String fChiefriverid;

    private String fGroupid;

    private String fInriveroutletscalecode;

    private String fInrivermodecode;

    private String fDischargedmodecode;

    private String fSewageoutletsortcode;

    private String fDivisioncode;

    private String fDivisionname;

    private Integer fCollectionmode;

    private Integer fIssewageoutlet;

    private String fDischargedcycle;

    private Integer fIssetupnameplates;

    private Integer fFlowingwater;

    private String fWaterspecialsmell;

    private String fWaterspecialcolour;

    private Integer fIshavemonitorcondition;

    private Integer fUnusuallysamp;

    private String fNotes;

    private String fWaterpartition1code;

    private String fWaterpartition2code;

    private String fWaterpartition3code;

    private String fWaterfunctioncode;

    private String fWaterfunction1code;

    private String fWaterfunction2code;

    private LocalDateTime fSetdate;

    private Integer fIsenable;

    private Integer fIsdelete;

    private LocalDateTime fInvesubmittime;

    private String fAudituserid;

    private String fAudituser;

    private LocalDateTime fAudittime;

    private Integer fAuditresult;

    private String fAuditoption;

    private Integer fCheckstate;

    private String fCreateuserid;

    private String fCreateusername;

    private String fCreatedeptid;

    private String fCreatedeptname;

    private String fCreateunitid;

    private String fCreateuintname;

    private LocalDateTime fCreatetime;

    private String fUpdateuserid;

    private String fUpdateusername;

    private String fUpdatedeptid;

    private String fUpdatedeptname;

    private String fUpdateunitid;

    private String fUpdateunitname;

    private LocalDateTime fUpdatetime;

    private String fVerifyuserid;

    private String fVerifyuser;

    private LocalDateTime fVerifytime;

    private Integer fVerifyresult;

    private String fVerifyoption;

    private String fRivername;

    @TableField("F_WaterHaveQuestion")
    private Integer fWaterhavequestion;

    @TableField("F_WaterColHaveQuestion")
    private Integer fWatercolhavequestion;

    private String fChiefusername;

    private String fChiefuserunit;

    private String fChiefposition;

    private String fTelephone;

    private Integer fNature;

    private String fWatergrade;

    private String fWaterfunction2dataid;

    private String fWaterfunction1dataid;

    private Integer fPhisexceeding;

    private Integer fCodisexceeding;

    private Integer fAmmonianitisexceeding;

    private Integer fTotalphoisexceeding;

    private Integer fTotalnitisexceeding;

    private Integer fBod5isexceeding;

    private Integer fAnimaloilsconisexceeding;

    private Integer fIsblacksmellywater;

    private String fThisrivername;

    private String fWatercharacteristics;

    private String fSewagesurenvironment;

    private String fSewagesussource;

    private String fSewageoutletsortmxcode;

    @TableField("F_EmissionStandard")
    private String fEmissionstandard;

    @TableField("F_SewOutletShape")
    private String fSewoutletshape;

    @TableField("F_IsWaterAnomaly")
    private Integer fIswateranomaly;

    @TableField("F_IsWateQualityrAnomaly")
    private Integer fIswatequalityranomaly;

    @TableField("F_SewOutletQuestions")
    private String fSewoutletquestions;

    @TableField("F_IsMustProcedures")
    private Integer fIsmustprocedures;

    @TableField("F_IsHaveProcedures")
    private Integer fIshaveprocedures;

    @TableField("F_SampIsExceeding")
    private Integer fSampisexceeding;

    @TableField("F_ArtiSampExceeding")
    private Integer fArtisampexceeding;

    @TableField("F_ReviewState")
    private Integer fReviewstate;

    private String fRecord;

    private Integer fOperationsteps;

    @TableField("F_TmpSewageOutletCode")
    private String fTmpsewageoutletcode;

    private String fChiefusernametotal2;

    private String fChiefpositiontotal2;

    private String fTelephonetotal2;

    private String fChiefusername2;

    private String fChiefposition2;

    private String fTelephone2;

    private String fChiefusernametotal3;

    private String fChiefpositiontotal3;

    private String fTelephonetotal3;

    private String fChiefusername3;

    private String fChiefposition3;

    private String fTelephone3;

    private String fChiefusernametotal4;

    private String fChiefpositiontotal4;

    private String fTelephonetotal4;

    private String fChiefusername4;

    private String fChiefposition4;

    private String fTelephone4;

    private String fSewageoutlettypenote;

    private String fSewageoutlettypecode;

    private Integer fInwatergradetargetcode;

    private Integer fIsopentrace;

    private String fIncompletetracenotes;

    private String fIncompletetracereason;

    private Integer fIsreleasesamp;

    private String fWatercharacteristiccode;

    private String fSewageoutletcodeNew;

    private String fOldcreateunitid;

    private String fGlpkid;

    private String fStreetid;

    private String fCanceltypecode;

    private String fWhycancel;

    private Integer fIsmustartificial;

    private String fUndetectedtypecode;

    private String fUndetectednote;

    private String fMouthheadname;

    private String fMonthheadposition;

    private String fMonthheadtel;


}
