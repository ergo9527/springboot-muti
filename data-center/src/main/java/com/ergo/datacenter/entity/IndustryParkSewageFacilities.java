package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class IndustryParkSewageFacilities implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 工业园区名称
     */
    private String parkName;

    private String parkCode;

    /**
     * 分园名称
     */
    private String subParkName;

    /**
     * 工业园区级别
     */
    private String parkType;

    /**
     * 污水处理设施名称
     */
    private String facilityName;

    /**
     * 污水处理设施类型
     */
    private String facilityType;

    /**
     * 污水处理设施纬度
     */
    private String latFacility;

    /**
     * 污水处理设施经度
     */
    private String lonFacility;

    /**
     * 污水处理设施出水COD日均值(mg/L)
     */
    private Double codDayAvg;

    /**
     * 污水处理设施出水氨氮日均值(mg/L)
     */
    private Double ammoniaNitrogenDayAvg;

    /**
     * 入河排污口名称
     */
    private String sewageOutletName;

    /**
     * 入河排污口纬度
     */
    private String latOutlet;

    /**
     * 入河排污口经度
     */
    private String lonOutlet;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 经度
     */
    private BigDecimal lon;


}
