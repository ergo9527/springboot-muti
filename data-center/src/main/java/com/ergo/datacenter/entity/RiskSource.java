package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author : liuxin
 * @Date: 2021/7/30 16:25
 * @Description :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("risk_source")
public class RiskSource {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long objectId;

    private String name;

    private String county;

    private String city;

    private BigDecimal lat;

    private BigDecimal lon;
}
