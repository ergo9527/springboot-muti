package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 14:51
 * @Description :
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataAnalysisCategory implements Comparable<DataAnalysisCategory> {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    private String nodeName;

    private String parentId;

    private String code;

    private int status;

    @TableField(exist = false)
    private List<DataAnalysisCategory> children;

    @Override
    public int compareTo(DataAnalysisCategory o) {
        System.out.println(nodeName.compareTo(o.getNodeName()));
        return nodeName.compareTo(o.getNodeName());
    }
}
