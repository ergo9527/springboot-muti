package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class NationalNatureReserveCd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 保护区名称
     */
    private String name;

    /**
     * 所属区县
     */
    private String areaBelong;

    /**
     * 级别
     */
    private String level;

    /**
     * 保护区
     */
    private String protectArea;

    /**
     * 经度
     */
    private BigDecimal lon;

    /**
     * 纬度
     */
    private BigDecimal lat;


}
