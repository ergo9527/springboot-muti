package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author : liuxin
 * @Date: 2021/8/7 9:26
 * @Description :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("key_enterprise_basic")
public class KeyEnterprise {

    @TableId(type = IdType.AUTO)
    private String id;

    private String name;

    private String regist_number;

    private String credit_code;

    private String industry_name;

    private String industry_category;

    private String business_status;

    private String county;

    private String industry_category_detail;

    private String phone_legal_person;

    private String industry_type;

    private String addr_prod_business;

    private BigDecimal lat;

    private BigDecimal lon;

    private String recent_station_name;
}
