package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-09-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class StApi implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 省厅接口api后缀
     */
    private String api;

    /**
     * 省厅接口url
     */
    private String url;

    /**
     * 是否可用
     */
    @TableField("isAccessable")
    private String isaccessable;

    /**
     * 备注
     */
    private String remark;


}
