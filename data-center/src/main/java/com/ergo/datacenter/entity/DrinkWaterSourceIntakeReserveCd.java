package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-09-30
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class DrinkWaterSourceIntakeReserveCd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 所属区县
     */
    private String areaBelong;

    /**
     * 水源地
     */
    private String waterSource;

    /**
     * 取水口
     */
    private String waterIntake;

    /**
     * 服务城
     */
    private String serviceCity;

    /**
     * 取水_1
     */
    private String waterIntakePlace;

    /**
     * 经度
     */
    private BigDecimal lon;

    /**
     * 纬度
     */
    private BigDecimal lat;


}
