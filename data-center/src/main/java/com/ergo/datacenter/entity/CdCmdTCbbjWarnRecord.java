package com.ergo.datacenter.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CdCmdTCbbjWarnRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    private String warnRecordId;

    private String pointCode;

    private String pointName;

    private String warnContent;

    private String warnType;

    private String triggerType;

    private String warnPollute;

    private LocalDateTime warnTime;

    private LocalDateTime firstWarnTime;

    private LocalDateTime createTime;

    private String createUser;

    private LocalDateTime editTime;

    private String editUser;

    private String regionCode;

    private String regionName;

    private String pointLocal;


}
