package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author ergo
 * @since 2021-08-12
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CrossBorderSection implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 断面id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 跨境断面名称
     */
    private String sectionName;

    /**
     * 跨境断面等级
     */
    private String sectionLevel;

    /**
     * 环境敏感受体类型
     */
    private String sensitiveReceptorType;

    /**
     * 所在辖区
     */
    private String areaBelong;

    /**
     * 辖区代码
     */
    private String areaCode;

    /**
     * 跨境断面类型
     */
    private String sectionType;

    /**
     * 所属流域
     */
    private String basinName;

    /**
     * 河流(河段)名称
     */
    private String riverName;

    /**
     * 跨区域级别
     */
    private String crossAreaLevel;

    /**
     * 跨区域类型
     */
    private String crossAreaType;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 经度
     */
    private BigDecimal lon;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionLevel() {
        return sectionLevel;
    }

    public void setSectionLevel(String sectionLevel) {
        this.sectionLevel = sectionLevel;
    }

    public String getSensitiveReceptorType() {
        return sensitiveReceptorType;
    }

    public void setSensitiveReceptorType(String sensitiveReceptorType) {
        this.sensitiveReceptorType = sensitiveReceptorType;
    }

    public String getAreaBelong() {
        return areaBelong;
    }

    public void setAreaBelong(String areaBelong) {
        this.areaBelong = areaBelong;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public String getBasinName() {
        return basinName;
    }

    public void setBasinName(String basinName) {
        this.basinName = basinName;
    }

    public String getRiverName() {
        return riverName;
    }

    public void setRiverName(String riverName) {
        this.riverName = riverName;
    }

    public String getCrossAreaLevel() {
        return crossAreaLevel;
    }

    public void setCrossAreaLevel(String crossAreaLevel) {
        this.crossAreaLevel = crossAreaLevel;
    }

    public String getCrossAreaType() {
        return crossAreaType;
    }

    public void setCrossAreaType(String crossAreaType) {
        this.crossAreaType = crossAreaType;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "CrossBorderSection{" +
                "id=" + id +
                ", sectionName=" + sectionName +
                ", sectionLevel=" + sectionLevel +
                ", sensitiveReceptorType=" + sensitiveReceptorType +
                ", areaBelong=" + areaBelong +
                ", areaCode=" + areaCode +
                ", sectionType=" + sectionType +
                ", basinName=" + basinName +
                ", riverName=" + riverName +
                ", crossAreaLevel=" + crossAreaLevel +
                ", crossAreaType=" + crossAreaType +
                ", lat=" + lat +
                ", lon=" + lon +
                "}";
    }
}
