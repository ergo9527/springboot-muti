package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class IndustryParkRange implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 工业园区名称
     */
    private String name;

    /**
     * 区县
     */
    private String county;

    /**
     * 区县代码
     */
    private String countyCode;

    /**
     * 几何学数据
     */
    private String obj;


}
