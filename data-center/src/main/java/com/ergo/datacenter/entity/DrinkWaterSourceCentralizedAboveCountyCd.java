package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-10-11
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class DrinkWaterSourceCentralizedAboveCountyCd implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 水源地名称
     */
    private String sydName;

    /**
     * 水源地等级
     */
    private String sydLevel;

    /**
     * 环境敏感受体类型
     */
    private String sensitiveReceptorType;

    /**
     * 区县代码
     */
    private String areaCode;

    /**
     * 所属辖区
     */
    private String areaBelong;

    /**
     * 所属流域
     */
    private String basinName;

    /**
     * 所属河流
     */
    private String riverName;

    /**
     * 水源地级别
     */
    private String sydType;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 经度
     */
    private BigDecimal lon;


}
