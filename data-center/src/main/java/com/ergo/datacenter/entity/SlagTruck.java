package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author : liuxin
 * @Date: 2021/7/29 16:12
 * @Description :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("slag_truck_heat")
public class SlagTruck {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Object score;

    private BigDecimal lat;

    private BigDecimal lon;

    private String pollutant;

    private Integer year;

    private Integer month;
}
