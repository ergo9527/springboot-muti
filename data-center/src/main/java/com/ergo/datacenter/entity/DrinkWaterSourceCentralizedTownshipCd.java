package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class DrinkWaterSourceCentralizedTownshipCd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 水源地名称
     */
    private String name;

    /**
     * 所属区县
     */
    private String areaBelong;

    /**
     * 所属乡镇
     */
    private String township;

    /**
     * 所在街道
     */
    private String street;

    /**
     * 水源类型
     */
    private String waterSourceType;

    /**
     * 供水服
     */
    private String waterSupply;

    /**
     * 水源状态
     */
    private String status;

    /**
     * 供水_1
     */
    @TableField(value = "supply_1")
    private Long supply1;

    /**
     * 年实际
     */
    private BigDecimal annualActual;

    /**
     * 纬度
     */
    private BigDecimal lat;

    /**
     * 经度
     */
    private BigDecimal lon;


}
