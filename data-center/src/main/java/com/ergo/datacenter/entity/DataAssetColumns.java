package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataAssetColumns implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * 字段注释
     */
    private String columnComment;

    /**
     * 字段数据类型
     */
    private String dataType;

    /**
     * 所属表名
     */
    private String tableName;

    /**
     * 所属表id
     */
    private Long tableId;

    /**
     * 表头名
     */
    private String headName;


}
