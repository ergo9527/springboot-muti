package com.ergo.datacenter.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CataLogs {
    @ApiModelProperty(value = "目录id", required = true, dataType = "Integer")
    private int catalogsId;
    //todo
    @ApiModelProperty(value = "计划访问")
    private String plannedAccess;

    @ApiModelProperty(value = "实际访问")
    private String actualAccess;

    @ApiModelProperty(value = "访问状态")
    private String accessStatus;

    @ApiModelProperty(value = "目录名称")
    private String catalogsName;

    @ApiModelProperty(value = "目录编码")
    private String catalogsCode;

    @ApiModelProperty(value = "父目录编码")
    private String parentCode;

    @ApiModelProperty(value = "目录级别")
    private int catalogsLevel;

    @ApiModelProperty(value = "目录名字")
    private String name;

    @ApiModelProperty(value = "打开")
    private boolean open;

    @ApiModelProperty(value = "数量")
    private String number;

    @ApiModelProperty(value = "数据格式")
    private String dataFormat;

    @ApiModelProperty(value = "因素")
    private String enFactors;

    @ApiModelProperty(value = "铅单位")
    private String leadUnit;

    @ApiModelProperty(value = "评论")
    private String remarks;

    @ApiModelProperty(value = "参数")
    private String parameters;

    @ApiModelProperty(value = "解释")
    private String explains;

    @ApiModelProperty(value = "订单索引")
    private int orderIndex;

    @ApiModelProperty(value = "激活")
    private int active;

    @ApiModelProperty(value = "子目录")
    private List<CataLogs> children;

    @ApiModelProperty(value = "子号码标签")
    private int childNumberTag;

    @ApiModelProperty(value = "套装除了")
    private boolean setExcept;

    @ApiModelProperty(value = "套灰色")
    private boolean setGrey;

    @ApiModelProperty(value = "code")
    private String code;
}
