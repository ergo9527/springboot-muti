package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ScenarioDynamicMiddle implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 情景分析目录节点code
     */
    private String scenarioCategoryCode;

    /**
     * 动态查询code
     */
    private String dynamicCode;

    /**
     * 备注
     */
    private String remark;


}
