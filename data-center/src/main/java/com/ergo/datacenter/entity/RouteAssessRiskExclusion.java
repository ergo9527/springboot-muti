package com.ergo.datacenter.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-08-19
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class RouteAssessRiskExclusion implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FID_评估路线_风险路段_排除路段
     */
    private Long fid;

    /**
     * 路段名称
     */
    private String name;

    /**
     * 路段长度
     */
    private Integer routeLength;

    /**
     * 关联河段
     */
    private String relativeRiver;

    /**
     * 最短离河距
     */
    private Integer shortestDistanceFromRiver;

    /**
     * 起点纬度
     */
    private BigDecimal latStart;

    /**
     * 起点经度
     */
    private BigDecimal lonStart;

    /**
     * 终点纬度
     */
    private BigDecimal latEnd;

    /**
     * 终点经度
     */
    private BigDecimal lonEnd;

    /**
     * 备注
     */
    private String remark;

    /**
     * shape_length
     */
    private BigDecimal shapeLength;

    /**
     * feature
     */
    private String obj;


}
