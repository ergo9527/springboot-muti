package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-11-02
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class HotSpotArea implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String geometryObj;

    /**
     * 网格ID
     */
    private String gridId;

    /**
     * 区县
     */
    private String county;

    /**
     * 乡镇
     */
    private String town;

    private String wRxy;

    private String aRxy;

    private String rxy;


}
