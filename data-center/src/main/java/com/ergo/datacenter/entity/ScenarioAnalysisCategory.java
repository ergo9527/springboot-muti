package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-10-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ScenarioAnalysisCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 目录节点名
     */
    private String nodeName;

    /**
     * 父节点id
     */
    private Long parentId;

    /**
     * 动态查询code
     */
    private String code;

    /**
     * 1
     */
    private Integer status;


}
