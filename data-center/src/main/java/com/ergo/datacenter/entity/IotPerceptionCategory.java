package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IotPerceptionCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 目录节点名
     */
    private String nodeName;

    /**
     * 父节点id
     */
    private Long parentId;

    /**
     * 动态查询code
     */
    private String code;

    /**
     * 1-显示;0-不显示
     */
    private Integer status;

    /**
     * 显示顺序
     */
    private Integer showIndex;


}
