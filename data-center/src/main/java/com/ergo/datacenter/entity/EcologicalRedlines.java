package com.ergo.datacenter.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-08-31
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class EcologicalRedlines implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区县
     */
    private String district;

    /**
     * 保护地等级
     */
    private String protectedLevel;

    /**
     * 红线命名
     */
    private String redlineName;

    /**
     * 生态系统
     */
    private String ecosystem;

    /**
     * 主导功能
     */
    private String leadingFunction;

    /**
     * 保护地名称
     */
    private String protectedName;

    /**
     * 类型编码
     */
    private String typeCode;

    /**
     * 红线编码
     */
    private String redlineCode;

    /**
     * 行政区代码
     */
    private String areaCode;

    /**
     * 管控措施
     */
    private String controlMeasures;

    /**
     * 红线类型
     */
    private String redlineType;

    /**
     * 面积(km²)
     */
    private BigDecimal area;

    /**
     * 几何学数据
     */
    private String obj;


}
