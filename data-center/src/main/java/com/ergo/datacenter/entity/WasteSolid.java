package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-08-16
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@TableName("solid_waste")
public class WasteSolid implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 固废类
     */
    private String wasteType;

    /**
     * 参考影
     */
    private String referDate;

    /**
     * 所属区
     */
    private String areaBelong;

    /**
     * 面积
     */
    private BigDecimal areaCovered;

    /**
     * 编码
     */
    private String wasteCode;

    /**
     * 是否拆迁渣
     */
    private String isDemolitionSlag;

    /**
     * 中心纬度
     */
    private BigDecimal lat;

    /**
     * 中心经度
     */
    private BigDecimal lon;

    /**
     * 面域数据obj
     */
    private String polygonObj;

}
