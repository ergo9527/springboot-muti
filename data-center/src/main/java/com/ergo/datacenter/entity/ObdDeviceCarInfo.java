package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-11-19
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class ObdDeviceCarInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    private String customDeviceName;

    private String obdCode;

    private Integer carTypeId;

    private String carTypeName;

    private Long deviceId;

    private Integer emissionType;

    private String emissionTypeStr;

    private Boolean isDelete;
}
