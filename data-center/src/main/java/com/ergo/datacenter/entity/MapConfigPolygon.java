package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-08-13
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class MapConfigPolygon implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
//    @TableId(value = "id", type = IdType.AUTO)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 面域id
     */
    private Long polygonId;

    /**
     * mapConfig ID
     */
    private Long pid;

    /**
     * 面域颜色
     */
    private String color;

    /**
     * 面域透明度
     */
    private Double fillOpacity;

    /**
     * 创建人id
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改人ID
     */
    private Long updateBy;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}
