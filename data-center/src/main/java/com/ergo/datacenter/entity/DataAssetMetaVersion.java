package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2022-03-25
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class DataAssetMetaVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 数据资产表id
     */
    private Long assetId;

    /**
     * json:表字段以及字段类型键值对
     */
    private String columnMetaData;

    /**
     * 发布日期
     */
    private Date publishedAt;

    /**
     * 版本号
     */
    private String version;


}
