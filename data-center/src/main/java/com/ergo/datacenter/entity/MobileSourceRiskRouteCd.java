package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-10-12
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class MobileSourceRiskRouteCd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 环境风险等级
     */
    private String riskLevel;

    /**
     * 路段名称
     */
    private String roadSectionName;

    /**
     * 路段长度
     */
    private Integer roadSectionLengh;

    private Integer shortestDistanceFromRiver;

    /**
     * 关联水体
     */
    private String associatedRiver;

    /**
     * 环境敏感受体
     */
    private String envirSensitiveAcceptor;

    /**
     * 所在辖区
     */
    private String areaBelong;

    /**
     * 几何学坐标数据
     */
    private String obj;

    /**
     * 起点纬度
     */
    private BigDecimal latStart;

    /**
     * 起点经度
     */
    private BigDecimal lonStart;

    /**
     * 终点纬度
     */
    private BigDecimal latEnd;

    /**
     * 终点经度
     */
    private BigDecimal lonEnd;


}
