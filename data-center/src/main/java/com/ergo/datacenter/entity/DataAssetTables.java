package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2022-03-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataAssetTables implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据资产表主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 数据资产备注(资产本身的备注)
     */
    private String assertComment;

    /**
     * 表来源
     */
    private String source;

    /**
     * 所属数据库名
     */
    private String databaseName;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 数据资产编码(常用于与动态查询匹配)
     */
    private String code;

    /**
     * 来源背景
     */
    private String background;

    /**
     * 数据范围
     */
    private String dataRange;

    /**
     * 数据参数
     */
    private String dataParam;

    /**
     * 数据交换频次
     */
    private String dataFrequency;

    /**
     * 公开类型
     */
    private String publicType;

    /**
     * 起始时间
     */
    private String createAt;

    /**
     * 更新时间
     */
    private String updateAt;

    /**
     * 数据格式
     */
    private String dataFormat;

    /**
     * 数据集市目录id
     */
    private Long martCategoryId;

    /**
     * 数据资产发布状态: 0-未发布; 1-已发布;
     */
    private Integer releaseStatus;

    /**
     * 数据接入备注(数据接入的备注)
     */
    private String accessComent;

    /**
     * 数据生产单位
     */
    private String prodOrgan;

    /**
     * 生产单位联系人
     */
    private String prodContacts;

    /**
     * 数据来源单位
     */
    private String sourceOrgan;

    /**
     * 来源单位联系人
     */
    private String sourceContacts;

    /**
     * 来源系统
     */
    private String sourceSystem;

    /**
     * 运维公司
     */
    private String operationCompany;

    /**
     * 运维联系人
     */
    private String operationContacts;

    /**
     * 审批单位
     */
    private String approveOrgan;

    /**
     * 接入方式
     */
    private String accessType;

    /**
     * 接入网络
     */
    private String accessNetwork;

    /**
     * 接入时间
     */
    private String accessDate;


}
