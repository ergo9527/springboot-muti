package com.ergo.datacenter.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-10-28
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class RiverPolicyPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 点位名
     */
    private String name;

    /**
     * 点位类型
     */
    private String type;

    /**
     * 描述
     */
    private String description;

    private BigDecimal lat;

    private BigDecimal lon;

    private Integer status;


}
