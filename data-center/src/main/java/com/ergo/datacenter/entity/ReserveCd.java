package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-08-26
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class ReserveCd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * GB
     */
    private Integer gb;

    /**
     * name
     */
    private String title;

    /**
     * 保护区名称
     */
    private String name;

    /**
     * 保护区级别
     */
    private String level;

    /**
     * 保护区分区
     */
    private String reserveZoning;

    /**
     * 面积
     */
    private Double area;

    private String obj;
}
