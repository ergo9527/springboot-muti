package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2022-03-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class NoiseHourData implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String town;

    @TableField("stationName")
    private String stationName;

    private String deviceid;

    private String datatype;

    private LocalDateTime datetime;

    private LocalDateTime integraltime;

    private Double leq;

    private Double l5;

    private Double l10;

    private Double l50;

    private Double l90;

    private Double l95;

    private Double lmax;

    private Double lmin;

    private Double sd;

    private Double rate;

    private Double validrate;

    private Double ld;

    private Double ln;

    private Double ldn;

    private String weista;

    private String adjvalue;

    @TableField("deptId")
    private Long deptid;

    private String city;

    @TableField("stationNumber")
    private String stationnumber;

    @TableField("stationType")
    private Integer stationtype;

    @TableField("isEequpCheck")
    private String iseequpcheck;

    @TableField("isCalibrationCheck")
    private String iscalibrationcheck;

    @TableField("isOperation")
    private String isoperation;

    @TableField("isWind")
    private String iswind;

    @TableField("isRain")
    private String israin;

    @TableField("isFault")
    private String isfault;


}
