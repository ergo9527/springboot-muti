package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.*;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-11-05
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class OutletVideoInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 排口名
     */
    private String name;

    /**
     * 排口code
     */
    private String code;

    /**
     * 排口位置：1-水上；2-水下
     */
    private Integer position;

    /**
     * 排口状态：1-在线；2-离线
     */
    private Integer status;

    /**
     * 排口类型：1-污水排口；2-雨水排口
     */
    private Integer type;

    /**
     * 电话
     */
    private String phone;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lon;

    /**
     * 河流id：1锦江；2府河；3沱江
     */
    @TableField("riverLakeId")
    private Integer riverlakeid;

    /**
     * 责任主体
     */
    private String duty;

    /**
     * 监管主体
     */
    private String supervise;

    /**
     * 排口告警：0正常；1~15异常
     */
    private Integer warn;


}
