package com.ergo.datacenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ergo
 * @since 2021-11-12
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class SkynetCamera implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 设备名称
     */
    private String name;

    /**
     * 机构名称
     */
    private String orgnName;

    /**
     * 设备纬度
     */
    private Double lat;

    /**
     * 设备经度
     */
    private Double lon;


}
