package com.ergo.datacenter;

import java.util.*;

/**
 * @Author : liuxin
 * @Date: 2021/9/8 10:02
 * @Description : 数字推理
 */
public class GuessNumber {

    public static void main(String[] args) {

        //打印规则
        printRules();
        System.out.println("请输入您想挑战的随机数数量(建议4): ");
        Scanner sc = new Scanner(System.in);
        int digits = sc.nextInt();
        //生成随机数字符串
        String randomString = getRandomNumberString(digits);
        if (randomString.length() == digits) {
            System.out.println("随机数生成完毕,请开始你的表演: ");
        }
        //开始推断
        int count = 0;
        Boolean flag = false;
        long guessBegin = System.currentTimeMillis();
        while (!flag) {
            Scanner randomSC = new Scanner(System.in);
            String guessString = randomSC.nextLine();
            if (guessString == null || guessString == "") {
                System.out.println("答案为空,请重新输入");
                continue;
            } else if (guessString.length() != digits) {
                System.out.println("答案位数有误,请重新输入");
                continue;
            }
            //验证推断
            flag = checkAnswer(guessString, randomString);
            count++;
        }
        long guessEnd = System.currentTimeMillis();
        String rank;
        if (count <= 3) {
            rank = "牛逼,关了吧,没意思";
        } else if (count <= 5) {
            rank = "灵童转世了属于是";
        } else if (count <= 8) {
            rank = "聪明绝顶";
        } else if (count <= 10) {
            rank = "离聪明人只差一步,加油";
        } else if (count <= 15) {
            rank = "我奶奶算得都比你快";
        } else {
            rank = "你在键盘上撒把米,鸡都比你能玩儿";
        }
        System.out.println("恭喜完成挑战,共尝试" + count + "次,耗时" + (guessEnd - guessBegin) / 1000 + "秒");
        System.out.println(rank);
    }


    /**
     * 规则打印
     */
    public static void printRules() {
        System.out.println("♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢数字推理游戏♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢");
        System.out.println(" ♢                                                   ♢");
        System.out.println("  ♢                      规则:                      ♢");
        System.out.println("   ♢         由n位0~9随机不重复数字组成的排列,        ♢");
        System.out.println("   ♢ 通过键入形如`9527`的数字组合来验证您的猜想是否正确 ♢");
        System.out.println("  ♢                      图例:                      ♢");
        System.out.println(" ♢              ★：数字正确且位置正确                  ♢");
        System.out.println("♢               ☆：数字正确但位置不正确                  ♢");
        System.out.println("♢                                                      ♢");
        System.out.println("♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢♢");
        System.out.println("                       ☺游戏开始☺");
    }

    /**
     * 生成指定数量的0~9随机不重复数字
     *
     * @param digits 随机数数量
     * @return 随机不重复数字的字符串
     */
    public static String getRandomNumberString(int digits) {

        HashSet<Integer> set = new HashSet<>();
        Random random = new Random();
        for (set.size(); set.size() < digits; ) {
            int randomNum = random.nextInt(10);
            set.add(randomNum);
        }
        Integer[] randomArray = set.toArray(new Integer[set.size()]);
        StringBuilder sb = new StringBuilder();
        Arrays.stream(randomArray).forEach(r -> {
            sb.append(r.toString());
        });
        return sb.toString();
    }

    /**
     * @param guessString  猜测数字字符串
     * @param randomString 正确答案数字字符串
     * @return
     */
    public static Boolean checkAnswer(String guessString, String randomString) {

        int absolutelyRight = 0;
        int justNumbermRight = 0;

        for (int i = 0; i < guessString.length(); i++) {
            if (guessString.charAt(i) == randomString.charAt(i)) {    //推断字符串和答案字符串对应位相同
                absolutelyRight++;
            } else if (randomString.indexOf(guessString.charAt(i)) != -1) {     //答案字符串里有该位字符,但不在对应位置
                justNumbermRight++;
            } else {  //判断下一位
                continue;
            }
        }
        System.out.println("★ " + absolutelyRight + "  ☆" + justNumbermRight);
        if (absolutelyRight == 4) {
            return true;
        } else {
            return false;
        }
    }
}
