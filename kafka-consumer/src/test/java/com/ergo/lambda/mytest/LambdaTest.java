package com.ergo.lambda.mytest;

import com.alibaba.fastjson.JSONObject;
import com.ergo.entity.TruckThermodynamic;
import com.ergo.lambda.entity.User;
import com.ergo.service.TruckThermodynamicService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @Author : liuxin
 * @Date: 2021/6/23 11:46
 * @Description :
 */
@SpringBootTest
public class LambdaTest {

    @Autowired
    private TruckThermodynamicService truckThermodynamicService;

    @Test
    public void testFilter() {

        List<User> userList = new ArrayList<>();
        User u1 = User.builder().name("张三").age(17).gender(0).build();
        User u2 = User.builder().name("李四").age(20).gender(1).build();
        User u3 = User.builder().name("王五").age(40).gender(1).build();
        User u4 = User.builder().name("王五").age(40).gender(1).build();
        User u5 = User.builder().name("赵六").age(15).gender(0).build();
        userList.add(u1);
        userList.add(u2);
        userList.add(u3);
        userList.add(u4);
        userList.add(u5);

        //测试去重
        System.out.println("去重:");
        userList.stream().distinct().sorted().forEach(user -> {
            System.out.println(user);
        });

        //测试排序
        System.out.println("排序:");
        userList.stream().sorted().forEach(user -> {
            System.out.println(user);
        });

        System.out.println(userList.size());
    }

    @Test
    public void test02() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名类");
            }
        };
        runnable.run();
    }

    @Test
    public void test03() {
        Runnable runnable = () -> {
            System.out.println("lambda");
        };
        runnable.run();
    }


    @Test
    public void test4() {

        JSONObject jsonObject = new JSONObject();
        while (true) {
            jsonObject.put("clat", 30.12548936);
            jsonObject.put("clon", 104.56894284);
            jsonObject.put("truck_num", new Random().nextInt(500));
            jsonObject.put("published_at", new Date());
            jsonObject.put("dt", new Date());
            truckThermodynamicService.insertIntoMysql(jsonObject);
        }
    }

}
