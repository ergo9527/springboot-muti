package com.ergo.lambda.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

/**
 * @Author : liuxin
 * @Date: 2021/6/23 14:59
 * @Description :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Comparable {

    private String name;

    private Integer age;

    /**
     * 性别：0-女；1-男
     */
    private Integer gender;

    /**
     * 重写比较方法
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        if (o instanceof User) {
            User user = (User) o;
            if (this.age > user.age) {
                return 1;
            } else if (this.age < user.age) {
                return -1;
            } else {
                //如果有属性一样可以嵌套if语句
                return -this.name.compareTo(user.name);
            }
        }
        throw new RuntimeException("传入类型不一致");
    }
}
