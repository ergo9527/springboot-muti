package com.ergo.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ergo.entity.TruckThermodynamic;
import com.ergo.mapper.TruckThermodynamicMapper;
import com.ergo.service.TruckThermodynamicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/6/22 14:22
 * @Description : 运渣车热力图统计Service
 */
@Service
public class TruckThermodynamicServiceImpl implements TruckThermodynamicService {

    @Autowired
    private TruckThermodynamicMapper truckThermodynamicMapper;

    @Override
    public void insertIntoMysql(JSONObject jsonObject) {
        TruckThermodynamic truckThermodynamic = JSONObject.toJavaObject(jsonObject, TruckThermodynamic.class);
        truckThermodynamicMapper.insert(truckThermodynamic);
    }

    @Override
    public JSONObject findByYearAndMonth(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        JSONObject jsonObject = findByDateFormat(sdf.format(date), sdf);
        return jsonObject;
    }

    @Override
    public JSONObject findByDay(Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject jsonObject = findByDateFormat(sdf.format(date), sdf);
        return jsonObject;
    }

    @Override
    public JSONObject findByHour(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return findByDateFormat(sdf.format(date), sdf);
    }

    private JSONObject findByDateFormat(String dateString, SimpleDateFormat sdf) {

        JSONObject jsonObject = new JSONObject();
        JSONArray dataArray = new JSONArray();

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.likeRight("published_at", dateString);
        List<TruckThermodynamic> truckThermodynamics = truckThermodynamicMapper.selectList(queryWrapper);
        if (truckThermodynamics.size() == 0) {
            return null;
        }
        truckThermodynamics.stream().forEach(truckThermodynamic -> {
            JSONArray singleData = new JSONArray();
            System.out.println(truckThermodynamic);
            singleData.add(truckThermodynamic.getTruckNum());
            singleData.add(truckThermodynamic.getClat());
            singleData.add(truckThermodynamic.getClon());
            dataArray.add(singleData);
        });
        jsonObject.put("id", truckThermodynamics.get(0).getId());
        jsonObject.put("date", sdf.format(truckThermodynamics.get(0).getPublishedAt()));
        jsonObject.put("data", dataArray);
        return jsonObject;
    }
}
