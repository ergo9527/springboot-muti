package com.ergo.service;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

/**
 * @Author : liuxin
 * @Date: 2021/6/22 14:21
 * @Description : 运渣车热力图统计Service
 */
public interface TruckThermodynamicService {

    void insertIntoMysql(JSONObject jsonObject);

    JSONObject findByYearAndMonth(Date dateString);

    JSONObject findByDay(Date dateString);

    JSONObject findByHour(Date dateString);
}
