package com.ergo.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.result.Result;
import com.ergo.service.TruckThermodynamicService;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author : liuxin
 * @Date: 2021/6/22 15:14
 * @Description :
 */

@ResponseBody
@RestController
@Api(tags = "运渣车热力图")
@RequestMapping("/truckThermodynamic")
public class TruckThermodynamicController {

    @Autowired
    private TruckThermodynamicService truckThermodynamicService;

    @GetMapping("/findByYearAndMonth")
    @ApiOperation(value = "根据年月查询运渣车热力图统计数据")
    public Result findByYearAndMonth(@RequestParam @JsonFormat(pattern = "yyyy-MM") @DateTimeFormat(pattern = "yyyy-MM") Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        JSONObject jsonObject = truckThermodynamicService.findByYearAndMonth(date);
        return Result.ok(jsonObject);
    }

    @GetMapping("/findByDay")
    @ApiOperation(value = "根据年月日查询运渣车热力图统计数据")
    public Result findByDay(@RequestParam @JsonFormat(pattern = "yyyy-MM-dd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        JSONObject jsonObject = truckThermodynamicService.findByDay(date);
        return Result.ok(jsonObject);
    }

    @GetMapping("/findByHour")
    @ApiOperation(value = "根据小时查询运渣车热力图统计数据")
    public Result findByHour(@RequestParam @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date) {
        JSONObject jsonObject = truckThermodynamicService.findByHour(date);
        return Result.ok(jsonObject);
    }

}
