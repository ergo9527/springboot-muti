package com.ergo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author : liuxin
 * @Date: 2021/6/22 14:25
 * @Description : 运渣车热力图统计数据实体
 */
@Data
@TableName("truck_thermodynamic_statistics")
@AllArgsConstructor
@NoArgsConstructor
public class TruckThermodynamic {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 纬度-高德坐标系
     */
    private BigDecimal clat;

    /**
     * 经度-高德坐标系
     */
    private BigDecimal clon;

    /**
     * 范围内运渣车数量
     */
    @TableField("truck_num")
    private Integer truckNum;

    /**
     * 发布时间
     */
    @TableField("published_at")
    private Date publishedAt;

    /**
     * 发布日期
     */
    @JsonIgnore
    @TableField("published_date")
    private Date dt;
}
