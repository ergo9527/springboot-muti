package com.ergo.consumer;

import com.alibaba.fastjson.JSONObject;
import com.ergo.service.TruckThermodynamicService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;


/**
 * @Author wxm
 * @Date 2021/5/15
 */
@Slf4j
@Component(value = "slagTruckConsumer")
public class SlagTruckConsumer {

    @Autowired
    private TruckThermodynamicService truckThermodynamicService;

    public void consumerAndInsert() {

        Properties props = new Properties();
        //kafka相关信息
        props.put("bootstrap.servers", "http://192.168.4.229:9092");
        props.setProperty("auto.offset.reset", "earliest");
        //topic后面加consumer
        props.setProperty("group.id", "slag_truck_consumer");
        props.setProperty("session.timeout.ms", "30000");
        props.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        //指定topic信息
        consumer.subscribe(Collections.singletonList("truck_dev"));
        log.info("Kafak消费者启动...");
        while (true) {
            //接收消息，poll参数为连接超时时间
            ConsumerRecords<String, String> records = consumer.poll(6000);
            for (ConsumerRecord<String, String> record : records) {
                if (!record.value().isEmpty()) {
                    try {
                        //转换成JSONObject
                        JSONObject jsonObject = JSONObject.parseObject(record.value());
                        System.out.println(jsonObject);
                        truckThermodynamicService.insertIntoMysql(jsonObject);
                        consumer.commitAsync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
