package com.ergo;

import com.ergo.consumer.SlagTruckConsumer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import scala.App;

@MapperScan("com.ergo.mapper")
@SpringBootApplication
public class TruckThermodynamicApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TruckThermodynamicApplication.class, args);
        SlagTruckConsumer slagTruckConsumer = (SlagTruckConsumer) context.getBean("slagTruckConsumer");
        slagTruckConsumer.consumerAndInsert();
    }
}
