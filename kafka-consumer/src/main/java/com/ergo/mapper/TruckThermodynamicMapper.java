package com.ergo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ergo.entity.TruckThermodynamic;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/6/22 14:35
 * @Description : 运渣车热力图统计Mapper
 */
@Mapper
@Repository
public interface TruckThermodynamicMapper extends BaseMapper<TruckThermodynamic> {
}
