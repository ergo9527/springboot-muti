package com.ergo.result;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @program: glzt-parent
 * @description: 结果集
 * @author: wyf
 * @create: 2021/2/7
 **/
public class Result implements Serializable {
    @ApiModelProperty("业务返回码  0成功 1错误")
    private Integer code;//业务返回码  0：成功  1:错误
    @ApiModelProperty("错误信息")
    private String message;//消息
    @ApiModelProperty("数据")
    private Object data;
    @ApiModelProperty("是否成功")
    private boolean success = true;
    @ApiModelProperty("描述是否成功")
    private String msg = "操作成功";


    public Integer getCode() {
        return this.code;
    }

    public Result setCode(Integer code) {
        this.code = code;
        return this;
    }

    public Result() {
        this.code = 0;
        this.message = "执行成功";
    }

    public Result(Object data) {
        this.code = 0;
        this.message = "执行成功";
        this.data = data;
    }

    public Result(String msg, boolean success, Integer code) {
        this.msg = msg;
        this.success = success;
        this.code = code;
    }

   /* public Result(String msg, boolean success, Integer code,Map<String,String> map) {
        this.msg = msg;
        this.success = success;
        this.code = code;
//        this.map = map;
    }*/


    public static Result ok() {
        return new Result();
    }

    public static Result ok(Object data) {
        return new Result(data);
    }

    public static Result ok(String msg) {
        return new Result(msg, true, 0);
    }


    public static Result fail(Exception e) {
        return new Result("操作失败", false, 1);
    }

    public static Result fail(String msg) {
        return new Result(msg, false, 1);
    }

    public static Result fail() {
        return new Result("操作失败", false, 1);
    }

    public static Result fail(MessageCode messageCode) {
        return new Result(messageCode.getMessage(), false, messageCode.getCode());
    }

/*    public static Result fail(Integer messageCode,String msg) {
        return new Result(msg, false, messageCode,);
    }*/

    public static Result fail(Integer code, String msg) {
        return new Result(msg, false, code);
    }


    public static Result noLogin() {
        return new Result("未登录", false, 2);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setInfo(String msg) {
        this.msg = msg;
    }

    public void setInfo(String msg, boolean success) {
        this.msg = msg;
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

/*    public Map<String, String> getMap() {
        return map;
    }*/

//    public void setMap(Map<String, String> map) {
//        this.map = map;
//    }
}
