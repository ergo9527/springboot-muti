package com.ergo.threadpool.thread;

/**
 * @Author : liuxin
 * @Date: 2021/7/27 17:52
 * @Description :
 */
public class HelloThread implements Runnable {
    @Override
    public void run() {
        System.out.println("问好线程: "+Thread.currentThread().getName()+"向你说嗨!");
    }
}
