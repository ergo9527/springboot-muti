package com.ergo.threadpool.executor;

import java.util.concurrent.*;

/**
 * @Author : liuxin
 * @Date: 2021/7/27 18:35
 * @Description : 定长线程池
 */
public class FixedThreadPoolExecutor extends ThreadPoolExecutor implements ExecutorService {

    /**
     * 构造方法:
     * 核心线程数 = 最大线程数 = n
     * 特点: 只有核心线程，线程数量固定，执行完立即回收，任务队列为链表结构的有界队列
     *
     * @param nThreads 线程数
     */
    public FixedThreadPoolExecutor(int nThreads) {
        super(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    }

    /**
     * 创建定长线程池，不指定线程工厂
     *
     * @param nThreads 线程数
     * @return ExecutorService
     */
    public static ExecutorService newFixedThreadPool(int nThreads) {
        return new ThreadPoolExecutor(nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    /**
     * 创建定长线程池，指定线程工厂
     *
     * @param nThreads 线程数
     * @return ExecutorService
     */
    public static ExecutorService newFixedThreadPool(int nThreads, ThreadFactory threadFactory) {
        return new ThreadPoolExecutor(nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),
                threadFactory);
    }
}
