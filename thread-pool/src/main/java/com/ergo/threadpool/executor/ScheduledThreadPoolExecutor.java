package com.ergo.threadpool.executor;

import java.util.concurrent.*;

/**
 * @Author : liuxin
 * @Date: 2021/7/27 18:11
 * @Description : 定时线程池
 */
public class ScheduledThreadPoolExecutor extends ThreadPoolExecutor implements ExecutorService {


    private static final long DEFAULT_KEEPALIVE_MILLIS = 10L;

    /**
     * 构造方法
     *
     * @param corePoolSize 核心线程数量
     */
    public ScheduledThreadPoolExecutor(int corePoolSize) {
        super(corePoolSize, Integer.MAX_VALUE, DEFAULT_KEEPALIVE_MILLIS, TimeUnit.SECONDS,
                new DelayQueue());
    }

    public static ScheduledThreadPoolExecutor newScheduledThreadPool(int corePoolSize) {
        return new ScheduledThreadPoolExecutor(corePoolSize);
    }
}
