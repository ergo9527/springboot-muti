package com.ergo.threadpool;

import com.ergo.threadpool.executor.FixedThreadPoolExecutor;
import com.ergo.threadpool.thread.HelloThread;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.concurrent.*;

@SpringBootTest
class ThreadPoolApplicationTests {

    /**
     * 测试定长线程池
     */
    @Test
    void testFixedThreadPool() {
        ExecutorService executorService = FixedThreadPoolExecutor.newFixedThreadPool(3);
        executorService.execute(new HelloThread());
        executorService.execute(new HelloThread());
        executorService.execute(new HelloThread());
        executorService.execute(new HelloThread());
    }

    /**
     * 测试定时线程池
     */
    @Test
    void testScheduledThreadPool() {

        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(5);
        //创建线程
        HelloThread helloThread = new HelloThread();
        //延迟三秒执行任务
        scheduledThreadPoolExecutor.schedule(helloThread, 1, TimeUnit.SECONDS);
        scheduledThreadPoolExecutor.scheduleAtFixedRate(helloThread,10,1000,TimeUnit.MILLISECONDS);

        //设置线程池的状态为STOP，然后尝试停止所有正在执行或暂停任务的线程，并返回等待执行任务的列表
        List<Runnable> runnables = scheduledThreadPoolExecutor.shutdownNow();
    }

    @Test
    void testScheduledThreadPool2() {
        // 1. 创建 定时线程池对象 & 设置线程池线程数量固定为5
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);
        // 2. 创建好Runnable类线程对象 & 需执行的任务
        Runnable task = new Runnable() {
            public void run() {
                System.out.println("执行任务啦");
            }
        };
        // 3. 向线程池提交任务
        scheduledThreadPool.schedule(task, 1, TimeUnit.SECONDS); // 延迟1s后执行任务
        scheduledThreadPool.scheduleAtFixedRate(task, 10, 1000, TimeUnit.MILLISECONDS);// 延迟10ms后、每隔1000ms执行任务
    }
}
