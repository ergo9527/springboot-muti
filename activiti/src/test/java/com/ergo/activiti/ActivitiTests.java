package com.ergo.activiti;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author : liuxin
 * @Date: 2021/6/11 15:58
 * @Description :
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ActivitiTests {

    @Test
    public void creatTable() {
        ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("/process/activiti.cfg.xml")
                .buildProcessEngine();
    }




}
