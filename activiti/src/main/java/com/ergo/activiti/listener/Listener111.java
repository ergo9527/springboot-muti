package com.ergo.activiti.listener;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 * @Author : liuxin
 * @Date: 2021/6/25 17:29
 * @Description :
 */
@Component
@Slf4j
public class Listener111 implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("cao");
    }
}
