package com.ergo.activiti;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/6/15 10:52
 * @Description :
 */

@SpringBootTest
public class MyActivitiTest {

    /**
     * 0：创建Activiti相关表
     */
    @Test
    public void testCreatTable() {
        ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("/activiti.cfg.xml")
                .buildProcessEngine();
    }

    /**
     * 1：部署流程
     * 运行成功后，查看之前的数据库表，就会发现多了很多内容
     */
    @Test
    public void testDeployProcess() {
        //加载的那两个内容就是我们之前已经弄好的基础内容哦。
        //得到了流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        processEngine.getRepositoryService()
                .createDeployment()
                .addClasspathResource("processes/qingjia.bpmn")
                .addClasspathResource("processes/qingjia.png")
                .deploy();
    }

    /**
     * 2：启动流程实例
     */
    @Test
    public void testStartProcessInstance() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        processEngine.getRuntimeService()
                .startProcessInstanceById("qingjia:1:4");  //这个是查看数据库中act_re_procdef表
    }

    /**
     * 3：完成请假申请
     */
    @Test
    public void testCompleteTask() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        processEngine.getTaskService()
                .complete("2505"); //查看act_ru_task表
    }

    /**
     * 4：直系领导查询正在执行的任务
     */
    @Test
    public void testQueryTaskByLeader() {
        //下面代码中的项目经理，就是我们之前设计那个流程图中添加的直系领导内容
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        List<Task> tasks = processEngine.getTaskService()
                .createTaskQuery()
                .taskAssignee("项目经理")
                .list();
        for (Task task : tasks) {
            System.out.println(task.getName());
        }
    }

    /**
     * :5：直系领导完成审批
     */
    @Test
    public void testFinishTaskByLeader() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        engine.getTaskService()
                .complete("5002"); //查看act_ru_task数据表
    }

    /**
     * 6：人事查询正在执行的任务
     */
    @Test
    public void testQueryTaskByHR() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        List<Task> tasks = processEngine.getTaskService()
                .createTaskQuery()
                .taskAssignee("人事")
                .list();
        for (Task task : tasks) {
            System.out.println(task.getName());
        }
    }

    /**
     * :7：人事完成审批
     */
    @Test
    public void testFinishTaskByHR() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        engine.getTaskService()
                .complete("7502"); //查看act_ru_task数据表
    }

    /**
     * 根据名称查询流程部署
     */
    @Test
    public void testQueryDeploymentByName() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        List<Deployment> deployments = processEngine.getRepositoryService()
                .createDeploymentQuery()
                .orderByDeploymenTime()//按照部署时间排序
                .desc()//按照降序排序
//                .deploymentName("qingjia")
                .list();
        for (Deployment deployment : deployments) {
            System.out.println(deployment.getId());
        }
    }

    /**
     * 查询所有的流程定义
     */
    @Test
    public void testQueryAllPD() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        List<ProcessDefinition> pdList = processEngine.getRepositoryService()
                .createProcessDefinitionQuery()
                .orderByProcessDefinitionVersion()
                .desc()
                .list();
        for (ProcessDefinition pd : pdList) {
            System.out.println(pd.getName());
        }
    }

    /**
     * 根据deployment_id_和name查询表：act_ge_bytearray，并输出流程图/bpmn文件到指定路径
     */
    @Test
    public void testShowImage() throws Exception {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        InputStream inputStream = processEngine.getRepositoryService()
                /**
                 * deploymentID
                 * 文件的名称和路径
                 */
                .getResourceAsStream("1", "processes/qingjia.png");
        OutputStream outputStream3 = new FileOutputStream("e:/processimg.png");
        int b = -1;
        while ((b = inputStream.read()) != -1) {
            outputStream3.write(b);
        }
        inputStream.close();
        outputStream3.close();
    }

    /**
     * 根据pdid查看图片(在act_re_procdef数据表中)
     * @throws Exception
     */
//    @Test
//    public void testShowImage2() throws Exception{
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        InputStream inputStream = processEngine.getRepositoryService()
//                .("shenqing:1:804");
//        OutputStream outputStream = new FileOutputStream("e:/processimg.png");
//        int b = -1 ;
//        while ((b=inputStream.read())!=-1){
//            outputStream.write(b);
//        }
//        inputStream.close();
//        outputStream.close();
//    }

    /**
     * 查看bpmn文件(在act_re_procdef数据表中)
     */
    @Test
    public void testShowBpmn() throws Exception {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        InputStream inputStream = processEngine.getRepositoryService()
                .getProcessModel("qingjia:1:4");
        OutputStream outputStream = new FileOutputStream("e:/processimg.bpmn");
        int b = -1;
        while ((b = inputStream.read()) != -1) {
            outputStream.write(b);
        }
        inputStream.close();
        outputStream.close();
    }

    /**
     * 根据pdid得到processDefinitionEntity
     */
    @Test
    public void testProcessDefinitionEntity() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) processEngine.getRepositoryService()
                .getProcessDefinition("qingjia:1:4");
        System.out.println(processDefinitionEntity.toString());
    }

    /**
     * 根据pdid得到processDefinitionEntity中的activityimpl
     */
//    @Test
//    public void testGetActivityImpl() {
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) processEngine.getRepositoryService()
//                .getProcessDefinition("qingjia:1:4");
//
//        //ActivityImpl是一个对象,代表processDefinitionEntity中的一个节点
//        List<ActivityImpl> activityImpls = processDefinitionEntity.getActivities();
//        for (ActivityImpl activityImpl : activityImpls) {
//            System.out.println(activityImpl.getId());
//            System.out.print("hegiht:"+activityImpl.getHeight());
//            System.out.print("width:"+activityImpl.getWidth());
//            System.out.print(" x:"+activityImpl.getX());
//            System.out.println(" y:"+activityImpl.getY());
//
//    }

    /**
     * 当前用户-->当前用户正在执行的任务--->当前正在执行的任务的piid-->该任务所在的流程实例
     */
    @Test
    public void getPIByUser() {
        List<ProcessInstance> pis = ActivitiUtils.getPIByUser("员工A");
        pis.stream().forEach(pi ->{
            System.out.println(pi.getName());
        });
    }



}
