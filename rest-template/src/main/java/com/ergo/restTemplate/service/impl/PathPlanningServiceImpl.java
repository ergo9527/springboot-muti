package com.ergo.restTemplate.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ergo.restTemplate.service.PathPlanningService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/9/6 17:10
 * @Description :
 */
@Service
public class PathPlanningServiceImpl implements PathPlanningService {
    @Override
    public JSONObject findPath(Map<String, Object> map) {
        String url = "https://restapi.amap.com/v3/direction/transit/integrated?origin={origin}&destination={destination}&city={city}&output=JSON&key={key}";
        RestTemplate restTemplate = new RestTemplate();
        JSONObject result = restTemplate.getForObject(url, JSONObject.class, map);
        return result;
    }
}
