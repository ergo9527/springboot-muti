package com.ergo.restTemplate.service;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/9/2 15:27
 * @Description :
 */
public interface HelloWorldService {

    JSONObject postWithSign(Map<String, Object> queryMap);
}
