package com.ergo.restTemplate.service;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/9/6 17:09
 * @Description :
 */
public interface PathPlanningService {
    JSONObject findPath(Map<String,Object> map);
}
