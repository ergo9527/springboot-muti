package com.ergo.restTemplate.service.impl;

import com.ali.gateway.utils.SignUtils;
import com.alibaba.fastjson.JSONObject;
import com.ergo.restTemplate.result.BusinessException;
import com.ergo.restTemplate.result.MessageCode;
import com.ergo.restTemplate.service.HelloWorldService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/9/2 15:28
 * @Description :
 */
@Service
public class HelloWorldServiceImpl implements HelloWorldService {
    @Override
    public JSONObject postWithSign(Map<String, Object> queryMap) {

        //用户id
        String ak_id = "Evp00SbI61lq3ePQ";
        //用户秘钥
        String ak_secret = "ERIaC4eurPlG2iydVW0VY0RsRU69jWxS";
        //请求地址
        String url = "http://127.0.0.1:8087/hello/sayHello";
        //请求参数
        JSONObject jsonObj = new JSONObject(queryMap);
        String queryString = jsonObj.toString();

        //设置headerMap参数，对应httpHeader参数，用于签名计算
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("method", "POST");
        headerMap.put("accept", "application/json");
        headerMap.put("content-type", "application/json");
        headerMap.put("date", SignUtils.toGMTString(new Date()));
        headerMap.put("x-acs-apicaller-uid", ak_id);
        headerMap.put("X-Access-Id", ak_id);    //接口调用的用户id(传递的是accessKeyId)
        headerMap.put("X-Access-Secret", ak_secret);
        //生成签名
        try {
            String signature = SignUtils.postSign(url, queryString, ak_id, ak_secret, headerMap);
            headerMap.put("Api-Signature", signature);  //将签名放入请求头
        } catch (Exception e) {
            throw new BusinessException(MessageCode.HTTP_HEADER_BUILD_FAIL);
        }
        String responseString;
        //发送http请求
        try {
            responseString = SignUtils.sendHttpRequest(url, queryString, ak_id, ak_secret, headerMap);
        } catch (Exception e) {
            throw new BusinessException(MessageCode.HTTP_REQUEST_FAIL);
        }
        return JSONObject.parseObject(responseString);
    }
}
