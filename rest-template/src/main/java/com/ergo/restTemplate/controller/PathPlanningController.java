package com.ergo.restTemplate.controller;

import com.ergo.restTemplate.result.ResultMsg;
import com.ergo.restTemplate.service.PathPlanningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/9/6 17:09
 * @Description : 高德开放平台-路径规划接口
 */
@RestController
@ResponseBody
@RequestMapping("/pathPlanning")
public class PathPlanningController {

    @Autowired
    private PathPlanningService pathPlanningService;

    @PostMapping("/findPath")
    public ResultMsg findPath(@RequestBody Map<String, Object> map) {
        return ResultMsg.ok(pathPlanningService.findPath(map));
    }
}
