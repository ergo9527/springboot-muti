package com.ergo.restTemplate.controller;

import com.ergo.restTemplate.result.ResultMsg;
import com.ergo.restTemplate.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author: liuxin
 * @Date: 2021/9/2 15:19
 * @Description: 接口提供方：原为省厅接口（由于内网环境无法调用，故改为本地data-center项目提供服务）
 * 签名过程：使用访问秘钥、秘钥密码、url、请求参数以及请求头关键信息等生成签名，并放入请求头中
 * 验签过程：获取header中的签名，根据url、请求参数以及请求头关键信息等重新生成签名，比较与header中签名是否一致
 */
@RestController
@ResponseBody
@RequestMapping("/helloWorld")
public class HelloWorldController {

    @Autowired
    private HelloWorldService helloWorldService;

    @PostMapping("/sayHello")
    public ResultMsg buildSign(@RequestBody Map<String, Object> data) {
        return ResultMsg.ok(helloWorldService.postWithSign(data));
    }
}
