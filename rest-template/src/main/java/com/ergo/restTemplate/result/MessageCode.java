package com.ergo.restTemplate.result;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @program: glzt-parent
 * @description:
 * @author: wyf
 * @create: 2021/3/9
 **/
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum MessageCode {

    ERROR(50000, "对不起，系统开小差了!"),
    SERVICE_NAME_EMPTY(50001, "服务名不能为空"),
    STATION_TYPE_EMPTY(50002, "站点类型不能为空"),
    PAGE_INFO_ERROR(50003, "分页信息错误"),
    FILE_INPUTSTREAM_ERROR(50004, "文件输入流异常"),
    HTTP_HEADER_BUILD_FAIL(50005, "http请求头构建失败"),
    HTTP_REQUEST_FAIL(50006, "http请求失败");


    //错误编码
    private Integer code;

    //错误信息
    private String message;

}
