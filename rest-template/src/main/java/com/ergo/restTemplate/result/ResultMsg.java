package com.ergo.restTemplate.result;



/**
 * Created by Administrator on 2018/1/16.
 */
public class ResultMsg {

    private boolean success = true;

    private String msg = "操作成功";

    private Integer code = 0;

    private Object data;

    public Integer getCode() {
        return this.code;
    }

    public ResultMsg setCode(Integer code) {
        this.code = code;
        return this;
    }

    public ResultMsg() {
    }

    public ResultMsg(Object data) {
        this.data = data;
    }

    public ResultMsg(String msg, boolean success, Integer code) {
        this.msg = msg;
        this.success = success;
        this.code = code;
    }

    public static ResultMsg ok() {
        return new ResultMsg();
    }

    public static ResultMsg ok(Object data) {
        return new ResultMsg(data);
    }

    public static ResultMsg ok(String msg) {
        return new ResultMsg(msg, true, 0);
    }


    public static ResultMsg fail(Exception e) {
        return new ResultMsg(e.getMessage(), false, 500);
    }

    public static ResultMsg fail(String msg) {
        return new ResultMsg(msg, false, 1);
    }

    public static ResultMsg fail(MessageCode messageCode) {
        return new ResultMsg(messageCode.getMessage(), false, messageCode.getCode());
    }

    public static ResultMsg fail(Integer code, String msg) {
        return new ResultMsg(msg, false, code);
    }

    public static ResultMsg noLogin() {
        return new ResultMsg("未登录", false, 2);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setInfo(String msg) {
        this.msg = msg;
    }

    public void setInfo(String msg, boolean success) {
        this.msg = msg;
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}

