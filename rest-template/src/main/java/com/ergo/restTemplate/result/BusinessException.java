package com.ergo.restTemplate.result;

import java.io.Serializable;

/**
 * @author zt
 * @version 2021.4.16
 * @Classname TaskLogTraceController
 * @Description 全局业务异常处理
 * @Date 2021/4/21 11:10
 */
public class BusinessException extends RuntimeException implements Serializable {

    private MessageCode messageCode;

    public BusinessException(){
    }

    public BusinessException(MessageCode messageCode){
        //给父类传递异常信息
        super(messageCode.getMessage());
        this.messageCode=messageCode;
    }

    public MessageCode getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(MessageCode messageCode) {
        this.messageCode = messageCode;
    }
}