package com.ergo.socket;

import com.alibaba.fastjson.JSONObject;
import com.ergo.socket.server.VocTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;

@SpringBootTest
class SocketApplicationTests {

    @Test
    void contextLoads() {
        String vocDataString = "##2199QN=20210917144227016;ST=31;CN=2051;PW=123456;MN=610100021;Flag=5;" +
                "CP=&&DataTime=20210917095000;a99999-Avg=7.74,a99999-Flag=N;a24001-Avg=1.34,a24001-Flag=N;a24079-Avg=0,a24079-Flag=N;a24002-Avg=1.84,a24002-Flag=N;a24038-Avg=0,a24038-Flag=N;a24037-Avg=0.39,a24037-Flag=N;a24053-Avg=1.24,a24053-Flag=N;a24061-Avg=0,a24061-Flag=N;a24045-Avg=0,a24045-Flag=N;a24064-Avg=0,a24064-Flag=N;a24063-Avg=0,a24063-Flag=N;a24041-Avg=0.21,a24041-Flag=N;a24077-Avg=0,a24077-Flag=N;a24919-Avg=0,a24919-Flag=N;a24902-Avg=0,a24902-Flag=N;a24076-Avg=0,a24076-Flag=N;a24074-Avg=0.17,a24074-Flag=N;" +
                "a24903-Avg=0,a24903-Flag=N;a24901-Avg=0,a24901-Flag=N;a24039-Avg=0.27,a24039-Flag=N;a24906-Avg=0,a24906-Flag=N;a24904-Avg=0,a24904-Flag=N;a24011-Avg=0,a24011-Flag=N;a24907-Avg=0,a24907-Flag=N;a24042-Avg=0.42,a24042-Flag=N;a24905-Avg=0,a24905-Flag=N;a24909-Avg=0,a24909-Flag=N;a24908-Avg=0,a24908-Flag=N;a24910-Avg=0,a24910-Flag=N;a25002-Avg=1.85,a25002-Flag=N;a24012-Avg=0,a24012-Flag=N;a24036-Avg=0,a24036-Flag=N;a24043-Avg=0,a24043-Flag=N;a24084-Avg=0,a24084-Flag=N;a24911-Avg=0,a24911-Flag=N;a24912-Avg=0,a24912-Flag=N;a24913-Avg=0,a24913-Flag=N;a24044-Avg=0,a24044-Flag=N;a25038-Avg=0,a25038-Flag=N;a25034-Avg=0,a25034-Flag=N;a25033-Avg=0,a25033-Flag=N;a25902-Avg=0,a25902-Flag=N;a25014-Avg=0,a25014-Flag=N;a25021-Avg=0,a25021-Flag=N;a25901-Avg=0,a25901-Flag=N;a25019-Avg=0,a25019-Flag=N;a25020-Avg=0,a25020-Flag=N;a24068-Avg=0,a24068-Flag=N;a25903-Avg=0,a25903-Flag=N;a25904-Avg=0,a25904-Flag=N;a24914-Avg=0,a24914-Flag=N;a25003-Avg=0,a25003-Flag=N;a24070-Avg=0,a24070-Flag=N;a25006-Avg=0,a25006-Flag=N;a25004-Avg=0,a25004-Flag=N;a25008-Avg=0,a25008-Flag=N;a24915-Avg=0,a24915-Flag=N;a24003-Avg=0,a24003-Flag=N;a24016-Avg=0,a24016-Flag=N;a29017-Avg=0,a29017-Flag=N;a24017-Avg=0,a24017-Flag=N;a24049-Avg=0,a24049-Flag=N;a24050-Avg=0,a24050-Flag=N;a25010-Avg=0,a25010-Flag=N;a25012-Avg=0,a25012-Flag=N;a25013-Avg=0,a25013-Flag=N;a25011-Avg=0,a25011-Flag=N;a25015-Avg=0,a25015-Flag=N;a05024-Avg=18.457,a05024-Flag=N;a01001-Avg=22.9,a01\n" +
                "001-Flag=N;a01002-Avg=83,a01002-Flag=N;a01006-Avg=951.8,a01006-Flag=N;a01007-Avg=1.2,a01007-Flag=N;a01008-Avg=259,a01008-Flag=N;" +
                "a05002-Avg=2354,a05002-Flag=N;a24088-Avg=596,a24088-Flag=N&&4DC0";
        JSONObject jsonObject = new JSONObject();
        String qn = "";
        if (vocDataString.contains("QN=")) {
            int indexOfQN = vocDataString.indexOf("QN=");
            qn = vocDataString.substring(indexOfQN + 3, indexOfQN + 20);
        }
        jsonObject.put("qn", qn);
        String st = "";
        if (vocDataString.contains("ST=")) {
            int indexOfST = vocDataString.indexOf("ST=");
            st = vocDataString.substring(indexOfST + 3, indexOfST + 5);
        }
        jsonObject.put("st", st);
        String cn = "";
        if (vocDataString.contains("CN=")) {
            int indexOfCN = vocDataString.indexOf("CN=");
            cn = vocDataString.substring(indexOfCN + 3, indexOfCN + 7);
        }
        jsonObject.put("cn", cn);

        String afterReplace = vocDataString.replaceAll("\n", "").replaceAll("##", "").replaceAll("CP=&&", "").replaceAll("&&", "").replaceAll(",", ";");
        String afterSub = afterReplace.substring(4, afterReplace.length() - 4);
        String[] split = afterSub.split(";");

        JSONObject data = new JSONObject();
        for (int i = 0; i < split.length; i++) {
            if (split[i].contains("PW")) {
                jsonObject.put("pwd", split[i].substring(3));
            }
            if (split[i].contains("MN")) {
                jsonObject.put("mn", split[i].substring(3));
            }
            if (split[i].contains("Flag")) {
                jsonObject.put("flag", split[i].substring(5));
            }
            if (split[i].contains("DataTime")) {
                String dateTime = split[i].substring(9);
                Calendar calendar = Calendar.getInstance();
                int year = Integer.valueOf(dateTime.substring(0, 4));
                int month = Integer.valueOf(dateTime.substring(4, 6));
                int day = Integer.valueOf(dateTime.substring(6, 8));
                int hour = Integer.valueOf(dateTime.substring(8, 10));
                int minute = Integer.valueOf(dateTime.substring(10, 12));
                int second = Integer.valueOf(dateTime.substring(12, 14));
                calendar.set(year, month, day, hour, minute, second);
                jsonObject.put("dataTime", calendar.getTime());
                break;
            }
        }
        for (int i = 7; i < split.length; i++) {
            String[] array = split[i].split("=");
            if (array[0].contains("Flag")) {
                data.put(array[0], array[1]);
            } else {
                data.put(array[0], Double.valueOf(array[1]));
            }
        }
        jsonObject.put("data", data);
        VocTest vocTest = JSONObject.toJavaObject(jsonObject, VocTest.class);
        System.out.println(vocTest.toString());
    }

    @Test
    public void test01(){
        String s3 = "hello";
        String s4 = "hello";
        System.out.println(s3.equals(s4));
    }
}
