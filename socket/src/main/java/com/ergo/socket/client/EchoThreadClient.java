package com.ergo.socket.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class EchoThreadClient {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        try {
            Socket client = new Socket("127.0.0.1", 8081);//创建和服务器的连接
            BufferedReader buff;//负责从服务器端 读取回写的字符串
            BufferedReader input;//负责读取从控制台输入的字符串
            PrintStream output;//负责在屏幕输入回写的字符串

            buff = new BufferedReader(new InputStreamReader(client.getInputStream()));
            input = new BufferedReader(new InputStreamReader(System.in));
            output = new PrintStream(client.getOutputStream());

            boolean flag = true;
            while (flag) {
                System.out.println("请输入信息：");
                String msg = input.readLine();
                if (msg == null || msg.equalsIgnoreCase("bye")) {
                    flag = false;//结束
                } else {
                    output.println(msg);//输出给服务器
                    //获得服务器回写的字符串
                    String echo = buff.readLine();
                    System.out.println();
                    System.out.println(echo);
                    System.out.println("==========");
                }
            }

            buff.close();
            client.close();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

}