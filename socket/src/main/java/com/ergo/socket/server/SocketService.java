package com.ergo.socket.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
@Service("SocketService")
public class SocketService {

    //搭建服务器端
    public void runServer() {

        try {
            //创建ServerSocket,绑定端口号
            ServerSocket server = new ServerSocket(8097);
            //创建用于接收客户端消息的Socket对象
            Socket client = null;
            boolean flag = true;
            //调用accept()方法,监听
            while (flag) {
                client = server.accept();//这里是阻塞方法
                //生成一个客户端响应线程
                new Thread(new EchoThread(client)).start();
            }
            //关闭socket和server
            client.close();
            server.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}