package com.ergo.socket.server;

import com.alibaba.fastjson.JSONObject;
import com.ergo.socket.utils.VocDataUtils;

import java.io.*;
import java.net.Socket;

//服务器端响应线程类
public class EchoThread implements Runnable {


    private Socket client;//客户端对象


    public EchoThread(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        //负责接受客户端传来的字符串，把字符串在回写给客户端。就像回声一样传个对方
        try {
            //负责读取从客户端发送过来的字符串，因为读取的是字符串，所以应该使用字符流，BufferReader
            BufferedReader buff = new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintStream out = new PrintStream(client.getOutputStream());
            boolean flag = true;
            while (flag) {
                String msg = buff.readLine();
                if (msg == null) {
                    flag = false;//结束线程
                } else {
                    JSONObject jsonObject = VocDataUtils.paresToJSONObject(msg);
                    VocTest vocTest = JSONObject.toJavaObject(jsonObject, VocTest.class);
                    // 响应现场机
                    StringBuilder sb = new StringBuilder();

                    sb.append("QN=").append(vocTest.getQn()).append(";ST=91").append(";CN=9014").append(";PW=").append(vocTest.getPwd())
                            .append(";MN=").append(vocTest.getMn()).append(";Flag=4;CP=&&&&\r");
                    out.println(sb.toString());
                    System.out.println(sb.toString());
//                    out.flush();
                }
            }
            buff.close();
            client.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}