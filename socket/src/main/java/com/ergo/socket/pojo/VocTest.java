package com.ergo.socket.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author ergo
 * @since 2021-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class VocTest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 请求编码,唯一标识一次命令交互
     */
    private String qn;

    /**
     * 系统编码
     */
    private String st;

    /**
     * 命令编码
     */
    private String cn;

    /**
     * 访问密码
     */
    private String pwd;

    /**
     * 设备唯一标识
     */
    private String mn;

    /**
     * 拆分包及应答标志
     */
    private Integer flag;

    /**
     * 数据时间
     */
    private Date dataTime;

    /**
     * 监测因子数据
     */
    private String data;


}
