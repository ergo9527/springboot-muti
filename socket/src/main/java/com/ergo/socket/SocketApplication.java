package com.ergo.socket;


import com.ergo.socket.server.SocketService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableDiscoveryClient
public class SocketApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SocketApplication.class, args);
        SocketService socketService = (SocketService) context.getBean("SocketService");
        socketService.runServer();
    }
}
