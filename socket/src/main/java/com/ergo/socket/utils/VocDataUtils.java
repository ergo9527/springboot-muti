package com.ergo.socket.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.Calendar;

/**
 * @Author : liuxin
 * @Date: 2021/9/17 16:48
 * @Description :
 */
public class VocDataUtils {

    public static JSONObject paresToJSONObject(String vocDataString) {
        JSONObject jsonObject = new JSONObject();
        String qn = "";
        if (vocDataString.contains("QN=")) {
            int indexOfQN = vocDataString.indexOf("QN=");
            qn = vocDataString.substring(indexOfQN + 3, indexOfQN + 20);
        }
        jsonObject.put("qn", qn);
        String st = "";
        if (vocDataString.contains("ST=")) {
            int indexOfST = vocDataString.indexOf("ST=");
            st = vocDataString.substring(indexOfST + 3, indexOfST + 5);
        }
        jsonObject.put("st", st);
        String cn = "";
        if (vocDataString.contains("CN=")) {
            int indexOfCN = vocDataString.indexOf("CN=");
            cn = vocDataString.substring(indexOfCN + 3, indexOfCN + 7);
        }
        jsonObject.put("cn", cn);

        String afterReplace = vocDataString.replaceAll("\n", "").replaceAll("##", "").replaceAll("CP=&&", "").replaceAll("&&", "").replaceAll(",", ";");
        String afterSub = afterReplace.substring(4, afterReplace.length() - 4);
        String[] split = afterSub.split(";");

        JSONObject data = new JSONObject();
        for (int i = 0; i < split.length; i++) {
            if (split[i].contains("PW")) {
                jsonObject.put("pwd", split[i].substring(3));
            }
            if (split[i].contains("MN")) {
                jsonObject.put("mn", split[i].substring(3));
            }
            if (split[i].contains("Flag")) {
                jsonObject.put("flag", split[i].substring(5));
            }
            if (split[i].contains("DataTime")) {
                String dateTime = split[i].substring(9);
                Calendar calendar = Calendar.getInstance();
                int year = Integer.valueOf(dateTime.substring(0, 4));
                int month = Integer.valueOf(dateTime.substring(4, 6));
                int day = Integer.valueOf(dateTime.substring(6, 8));
                int hour = Integer.valueOf(dateTime.substring(8, 10));
                int minute = Integer.valueOf(dateTime.substring(10, 12));
                int second = Integer.valueOf(dateTime.substring(12, 14));
                calendar.set(year, month, day, hour, minute, second);
                jsonObject.put("dataTime", calendar.getTime());
                break;
            }
        }
        for (int i = 7; i < split.length; i++) {
            String[] array = split[i].split("=");
            if (array[0].contains("Flag")) {
                data.put(array[0], array[1]);
            } else {
                data.put(array[0], Double.valueOf(array[1]));
            }
        }
        jsonObject.put("data", data);
        return jsonObject;
    }
}
