package com.ergo.openfeign.controller;

import com.alibaba.fastjson.JSONObject;
import com.ergo.openfeign.result.ResultMsg;
import com.ergo.openfeign.service.TidbBaseFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/8/20 17:56
 * @Description :
 */
@RequestMapping("/demo")
@ResponseBody
@RestController
public class RPCController {

    @Autowired
    private TidbBaseFeignService feignService;

    @PostMapping("/list/{code}")
    public ResultMsg queryList(@PathVariable("code") String code, @RequestBody Map<String, Object> data) {
        return ResultMsg.ok(feignService.findList(code, data));
    }

    @PostMapping("/page/{code}")
    public ResultMsg queryPage(@PathVariable("code") String code, @RequestBody Map<String, Object> data) {
        return ResultMsg.ok(feignService.findPage(code, data));
    }

    @PostMapping("/object/{code}")
    public ResultMsg queryObject(@PathVariable("code") String code, @RequestBody Map<String, Object> data) {
        return ResultMsg.ok(feignService.findObject(code, data));
    }
}
