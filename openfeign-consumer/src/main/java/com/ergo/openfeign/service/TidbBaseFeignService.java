package com.ergo.openfeign.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author : liuxin
 * @Date: 2021/6/28 11:36
 * @Description : Feign远程调用TidbBaseController接口
 */
@Component
@FeignClient("glzt-data-center-dynamic-query")
public interface TidbBaseFeignService {

    @PostMapping(value = "/dynamicQuery/list/{code}")
    JSONObject findList(@PathVariable("code") String code, @RequestBody Map<String, Object> data);

    @PostMapping(value = "/dynamicQuery/page/{code}")
    JSONObject findPage(@PathVariable("code") String code, @RequestBody Map<String, Object> data);

    @PostMapping(value = "/dynamicQuery/object/{code}")
    JSONObject findObject(@PathVariable("code") String code, @RequestBody Map<String, Object> data);
}
