package com.ergo.openfeign.result;

/**
 * All rights Reserved, Designed By Lin Xiaoshen
 *
 * @author: Lin Xiaoshen
 * @date: 2021/7/6 4:51 下午
 * @copyright: @2019
 */
public class PageResultModel extends ResultModel {

    private Long pageSize;

    private Long total;

    public PageResultModel(int code, String msg, Object data, Long pageSize, Long total) {
        super(code, msg, data);
        this.pageSize = pageSize;
        this.total = total;
    }

    public static PageResultModel success(Object data, Long pageSize, Long total) {
        return new PageResultModel(SUCCESS, null, data, pageSize, total);
    }

    public static PageResultModel error(String msg) {
        return error(ERROR, msg);
    }

    public static PageResultModel error(Integer code, String msg) {
        return new PageResultModel(code, msg, null, null, null);
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
