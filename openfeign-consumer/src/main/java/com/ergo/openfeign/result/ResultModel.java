package com.ergo.openfeign.result;

import lombok.Data;

/**
 * All rights Reserved, Designed By Lin Xiaoshen
 *
 * @author: Lin Xiaoshen
 * @date: 2021/7/6 4:51 下午
 * @copyright: @2019
 */
@Data
public class ResultModel {

    protected static Integer SUCCESS = 1;

    protected static Integer ERROR = 0;

    public int code;

    public String msg = "";

    public Object data;

    public ResultModel() {
    }

    public ResultModel(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static ResultModel success(String msg) {
        return new ResultModel(SUCCESS, msg, null);
    }

    public static ResultModel success(String msg, Object data) {
        return new ResultModel(SUCCESS, msg, data);
    }

    public static ResultModel success(Object data) {
        return new ResultModel(SUCCESS, null, data);
    }

    public static ResultModel error(String msg) {
        return new ResultModel(ERROR, msg, null);
    }

    public static ResultModel error(String msg, Object data) {
        return new ResultModel(ERROR, msg, data);
    }

    //自定义的MessageCode的构造函数
    public static ResultModel fail(Exception e) {
        return new ResultModel(500, e.getMessage(), null);
    }

    public static ResultModel fail(String msg) {
        return new ResultModel(500, msg, null);
    }

    public static ResultModel fail(MessageCode messageCode) {
        return new ResultModel(messageCode.getCode(), messageCode.getMessage(), null);
    }

    public static ResultModel fail(Integer code, String msg) {
        return new ResultModel(code, msg, null);
    }

}
