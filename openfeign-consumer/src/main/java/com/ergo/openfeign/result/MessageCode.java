package com.ergo.openfeign.result;

/**
 * @program: glzt-parent
 * @description:
 * @author: wyf
 * @create: 2021/3/9
 **/
public enum MessageCode {

    ERROR(50000, "对不起，系统开小差了!"),
    SERVICE_NAME_EMPTY(50001,"服务名不能为空"),
    STATION_TYPE_EMPTY(50002,"站点类型不能为空"),
    PAGE_INFO_ERROR(50003,"分页信息错误"),
    FILE_INPUTSTREAM_ERROR(50004,"文件输入流异常");


    //错误编码
    private Integer code;

    //错误信息
    private String message;

    MessageCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
